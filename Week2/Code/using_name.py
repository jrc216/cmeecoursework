#!/usr/bin/python
# Filename: using_name.py
"""file is usable as a script and not just an imported module"""
if __name__== '__main__':
	print 'This program is being run by itself'
else:
	print 'I am being imported from another module'
