#run r scripts
#run the bash script to compile latex
import subprocess 
import os.path

subprocess.Popen("Rscript --verbose MiniProject.R > ../Results/Miniproject.Rout 2>  ../Results/Miniproject_errFile.Rout", shell=True).wait()

if os.path.isfile("../Results/lm_graphs.pdf"):
	print 'lm graphs printed successfully'
else: 
	print 'lm graphs failed'

if os.path.isfile("../Data/schoolfield_inputs.csv"):
	print 'schoolfield inputs produced successfully'
else:
	print 'schoolfield inputs failed'
	
if os.path.isfile("../Data/merged_data.csv"):
	print 'results produced successfully'
else:
	print 'results failed to be produced'

subprocess.Popen("Rscript --verbose Jo.R > ../Results/Jo.Rout 2>  ../Results/Jo_errFile.Rout", shell=True).wait()

if os.path.isfile("../Results/pie.pdf"):
	print 'pie chart printed successfully'
else: 
	print 'pie chart failed'

if os.path.isfile("../Results/NLS.pdf"):
	print 'NLS graphs produced successfully'
else:
	print 'NLS graphs failed to be produced'
	
if os.path.isfile("../Results/nls_stats.csv"):
	print 'nls results produced successfully'
else:
	print 'nls results failed'

subprocess.Popen("bash CompileLatex.sh Report", shell=True).wait() #or os.system
#subprocess.Popen("bash ../Code/wordcount.sh", shell=True).wait()
