# SIMULATING DRIVE-DRIFT-SELECTION BALANCE
# RANDOM MATING

# INPUT PARAMETERS
# q IS THE ALLELE FREQ FOR THE LETHAL ALLELE
# d IS THE HOMING RATE?
# N IS THE INITIAL POPULATION SIZE
# t IS THE TIME STEPS (IN GENERATIONS)


drive.drift.selection<-function(q=0.5, d=0.6, N=500, t=10, R0=2, M=500)
{
  
  # A FUNCTION TO CALCULATE GENOTYPIC FREQUENCY
  cal.geno.freq<-function(population)
  {
    temp<-apply(population, 2, sum)
    # IF THE COLUMN SUM IS 0 THEN 00 HOMOZYGOTE
    # IF THE COLUMN SUM IS 1 THEN HETEROZYGOTE
    # IF THE COLUMN SUM IS 2 THEN 11 HOMOZYGOTE
    return(c(sum(temp==0), sum(temp==1), sum(temp==2))/length(temp))
  }
  
  # BEVERTON-HOLT MODEL FOR POPULATION DYNAMICS
  # TWO PARAMETERS TO MODEL CARRYING CAPACITY
  bh<-function(N, R0, M)
  {return(R0*N/(1+N/M))}
  
  
  # FITNESS COST
  # THE FIRST GENOTYPE IS 00
  # 01 HETEROZYGOTES, ASSUMING NO LOSS/ADVANTAGE IN FITNESS
  # 11 IS LETHAL
  fitness<-c(1, 1, 0)
  
  # GAMETES PRODUCED BY EACH GENOTYPE 
  gamete.0<-c(1, 1-d, 0)
  gamete.1<-c(0, d, 1)
  
  
  # USE A LIST TO STORE ALL THE POPULATION OUTPUTS
  # THE LIST CONSISTS OF t+1 MATRICES TO REPRESENT ALL THE INDIVIDUALS IN EACH GENERATION
  # EACH COLUMN IN A MATRIX REPRESENTS AN INDIVIDUAL
  population<-list()
  length(population)<-(t+1)
  
  # TO STORE THE POPULATION SIZE
  population.size<-rep(NA, t+1)
  population.size[1]<-N
  
  # TO STORE THE ALLELE FREQ OF q
  allele.freq.q<-rep(NA, t+1)
  allele.freq.q[1]<- q
  
  # THE INITIAL POPULATION
  # ASSUME HW EQUILIBRIUM (ie. BINOMIAL SAMPLING)
  k<-ceiling(2*N*q)
  population[[1]]<-matrix(sample(c(rep(0, 2*N-k), rep(1, k))), nr=2)
  #population[[1]]<-matrix(sample(0:1, size=2*N, replace=TRUE, prob=c(1-q, q)), nr=2)
  
  # FOR EACH TIME STEP
  for (i in 1:t)
  {
    if(population.size[[i]] != population.size[[i+1]])
      {
    # CALCULATE THE GENOTYPIC FREQ, THEN THE FREQ AFTER SELECTION, 
    # AND FINALLY THE GAMETIC FREQ (CONSIDERING THE DRIVE)
    genotype.freq<-cal.geno.freq(population[[i]])
    freq.after.selection<-genotype.freq*fitness/sum(genotype.freq*fitness)
    gametic.freq<-c(sum(gamete.0*freq.after.selection), sum(gamete.1*freq.after.selection))
    
    # CALCULATE THE POPULATION SIZE FOR THE NEXT TIME STEP
    # USING THE BH MODEL
    # AND THE LETHAL 11 GENOTYPE IS ALSO CONSIDERED
      population.size[i+1]<-floor(bh(population.size[i]*(1-genotype.freq[3]), R0=R0, M=M))
      
      # REPRODUCTION ONLY IF POPULATION SIZE >= 1
      if (population.size[i+1] >= 1)
      {
        # REPRODUCTION. USE THE gametic.freq FOR SAMPLING
        population[[i+1]]<-matrix(
          sample(0:1, size=2*population.size[i+1], replace=TRUE, 
                 prob=gametic.freq), nr=2)
        
        # THE NEW ALLELE FREQ OF q
        allele.freq.q[i+1]<-sum(population[[i+1]]==1)/(2*population.size[i+1])
      }
      else 
      {
        print('Opps! Population collapsed')
        return(list(population=population, population.size=population.size, allele.freq.q=allele.freq.q))
      }
    }
  }
  return(list(population=population, population.size=population.size, allele.freq.q=allele.freq.q))
}

###########################################
# TRY IT! 
result<-drive.drift.selection(q=0.2, d=0.8, N=500, t=25, R0=2, M=500)
