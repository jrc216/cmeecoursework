#!/usr/bin/python
"""
Plot a snapshot of a food web graph/network.
Needs: Adjacency list of who eats whom (consumer name/id in 1st
column, resource name/id in 2nd column), and list of species
names/ids and properties such as biomass (node abundance), or average
body mass.
"""
import networkx as nx # creates an object where you have to use nx. and you just access which parts of the function you want
import scipy as sc
import matplotlib.pyplot as plt
#import matplotlib.animation as ani # for animation

def GenRdmAdjList(N = 2, C = 0.5):   #generates a network with some probaility of connectance between two nodes
	"""
	Generate random adjacency list given N nodes with connectance
    probability C
    """
	Ids = range(N)
	ALst=[]    #empty list
	for i in Ids: # for every node draw a link between it and everything else using a unifrom distribution
		if sc.random.uniform(0,1,1) < C: # adds a link if randomly uniform value is less that the connectance probability
			Lnk = sc.random.choice(Ids,2).tolist()
			if Lnk[0] != Lnk[1]: #avoid self loops
				ALst.append(Lnk)
	return ALst

##Assign body mass range
SizRan = ([-10,10]) # use log scale

##Assign number of species (MaxN) and connectance (C)
MaxN = 30    #assign a network to be a particular size and then populate it
C = 0.75      #how dense the netwrok is, more dense means more connections

##Generate adjacency list:
AdjL = sc.array(GenRdmAdjList(MaxN,C))

##Generate  species (node) data:
Sps = sc.unique(AdjL) # get species ids
Sizs = sc.random.uniform(SizRan[0],SizRan[1],MaxN) # Generate body sizes (log10 scale)

######The Plotting#####
plt.close('all')   #close all plotting (same as dev.off() in R)

##Plot using networkx:
## Calculate coordinates for circular configuration:
## (See networkx.layout for inbuilt functions to compute other types of node cords)

pos = nx.circular_layout(Sps) # circular layout of visualisation

G = nx.Graph()   # network x graph
G.add_nodes_from(Sps)
G.add_edges_from(tuple(AdjL))
NodSizs= 10**-32 + (Sizs-min(Sizs))/(max(Sizs)-min(Sizs)) #node sizes in proportion to body sizes
nx.draw(G, pos, node_size = NodSizs*1000) # bigger nodes are bigger animals
plt.show()

