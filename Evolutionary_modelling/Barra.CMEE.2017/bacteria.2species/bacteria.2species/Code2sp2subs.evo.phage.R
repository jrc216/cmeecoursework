##assign values to parameters
N0<-c(0.1,0.1)
S0<-c(5,1)
P0<-N0/2 # intial phage density
d<-0.01
kcat<-matrix(c(0.1,0.1,0.1,0.1),nrow=2)
Km<-matrix(c(0.5,0.5,0.5,0.5),nrow=2)
c.conv<-matrix(c(1,1,1,1),nrow=2)
mu<-c(0.1,0.1)
topt<-c(20, 27)
a<-c(0.1, 0.1) #conversion rate of bacteria to phage
b<- c(0.1,0.1) #feeding rate of phage
dp<-0.1 #death rate phage
#tmax<-c(25, 32)

##set up so can have different parameter for each species X substrate
##column = species, row = substrate
E<-matrix(c(1,0,0,1),nrow=2,ncol=2)

##choose time steps and total time
time.step<-0.1
#temp.step<-0.1
times<-seq(0, 500, time.step)
#temps<-seq(0,40,length.out = times)
temps<-seq(0,40, length.out = length(times))


##set initial values of state variables
S<-S0
N<-N0
P<-P0
#topt<-topt
#kcat<-kcat
##store results
results<-matrix(NA,nrow=length(times)+1,ncol=13)

##store time 0 values
results[1,1]<-0
results[1,2:3]<-S
results[1,4:5]<-N
results[1, 6:7]<-P
results[1,8:11]<-E
results[1, 12:13]<-topt
##loop through the times
for (i in (1:length(times))) {
  
  ##assign new values
  S[1] <- S[1]-(kcat[1,1]*E[1,1]*S[1]/(Km[1,1]+S[1]))*N[1]*time.step-(kcat[1,2]*E[1,2]*S[1]/(Km[1,2]+S[1]))*N[2]*time.step-d*S[1]*time.step+d*S0[1]*time.step
  S[2] <- S[2]-(kcat[2,1]*E[2,1]*S[2]/(Km[2,1]+S[2]))*N[1]*time.step-(kcat[2,2]*E[2,2]*S[2]/(Km[2,2]+S[2]))*N[2]*time.step-d*S[2]*time.step+d*S0[2]*time.step
  N[1] <- N[1]+(c.conv[1,1]*(kcat[1,1]*E[1,1]*S[1]/(Km[1,1]+S[1]))*N[1]*time.step)+(c.conv[2,1]*(kcat[2,1]*E[2,1]*S[2]/(Km[2,1]+S[2]))*N[1]*time.step)-d*N[1]*time.step - b[1]*P[1]*N[1]*time.step
  N[2] <- N[2]+(c.conv[1,2]*(kcat[1,2]*E[1,2]*S[1]/(Km[1,2]+S[1]))*N[2]*time.step)+(c.conv[2,2]*(kcat[2,2]*E[2,2]*S[2]/(Km[2,2]+S[2]))*N[2]*time.step)-d*N[2]*time.step - b[2]*P[2]*N[2]*time.step
  
  P[1] <- P[1]+a[1]*N[1]*P[1]*time.step-dp*P[1]*time.step-d*P[1]*time.step # lotka volterra
  P[2] <- P[2]+a[2]*N[2]*P[2]*time.step-dp*P[2]*time.step-d*P[2]*time.step 
  
  ##change in enzyme for substrate 1 in each species
  ##this is mutation parameter X the selection gradient, namely how (1/N)dN.dt changes as you change enzyme allocation
  	E[1,1]<- E[1,1]+mu[1]*((c.conv[1,1]*(kcat[1,1]*S[1]*D1/(Km[1,1]+S[1])))-(c.conv[2,1]*(kcat[2,1]*S[2]*D1/(Km[2,1]+S[2]))))*time.step
  	E[1,2]<- E[1,2]+mu[2]*((c.conv[1,2]*(kcat[1,2]*S[1]*D2/(Km[1,2]+S[1])))-(c.conv[2,2]*(kcat[2,2]*S[2]*D2/(Km[2,2]+S[2]))))*time.step
  
  
  #change in topt
  #	topt[1]<-topt[1]+mu[1]*((c.conv[1,1]*(kcat[1,1]*E[1,1]*S[1]*D1*(temps[i]-topt[1]))/(20*(Km[1,1]+S[1]))) + (c.conv[2,1]*kcat[2,1]*E[2,1]*S[2]*D1*(temps[i]-topt[1]))/(20*(Km[2,1]+S[2])))*time.step
  #	topt[2]<-topt[2]+mu[2]*((c.conv[1,2]*(kcat[1,2]*E[1,2]*S[1]*D2*(temps[i]-topt[2]))/(20*Km[1,2]+S[1])) + (c.conv[2,2]*kcat[2,2]*E[2,2]*S[2]*D2*(temps[i]-topt[2]))/(20*(Km[2,2]+S[2])))*time.step
  ##corresponding opposite change in enzymes for substrate 2
  E[2,1]<-1-E[1,1]
  E[2,2]<-1-E[1,2]
  
  ##update k
  # kcat[1,] <- exp((-(temps[i]-topt[1])^2)/40)
  #kcat[2,] <- exp((-(temps[i]-topt[2])^2)/40)
  ##enforce boundary condition that total enzyme production is 1
  if(min(E)<0) E[E<0]<-0
  if(max(E)>1) E[E>1]<-1
  
  results[i+1,1]<-times[i]
  results[i+1,2:3]<-S
  results[i+1,4:5]<-N
  results[i+1, 6:7]<-P
  results[i+1,8:11]<-E
  #results[i+1, 12:13]<-topt
  
}

##plot the results
##a) Densities

par(mfrow=c(1,2))

matplot(results[,1], results[,2:5], type="l", xlab="Time", 
        ylab="Density/Concentration",col=c("black","red","black","red"),
        lty=c(1,1,3,3))
legend(x=100,y=2,legend=c("S1","S2","N1","N2"), col=c("black","red","black","red"),lty=c(1,1,3,3))

matplot(results[,1], results[,4:7], type="l", xlab="Time", 
        ylab="Density/Concentration",col=c("black","red", "black", "red"),lty=c(1,1,3,3))
legend(x=100,y=4,legend=c("bacteria1.Subst1","bacteria2.Subst2","phage1.subst1","phage2.subst2"), col=c("black","red"),lty=c(1,1,3,3))
