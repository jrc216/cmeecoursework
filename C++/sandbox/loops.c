#include <stdio.h>
#include <limits.h>


int main (void)
{
	int i;
	int im_getting_bored = 50000;
	
	i = 0;
	while (i < 5)   // as long as expression = non zero then while loop will continue
	{				//starts at 0, "<" runs faster than "<="
		i = i + 1; // ++i
	}
	printf("after while looping i is: %i\n", i);
	
	i = 0;
	do      //different way of writing a while loop
	{	
		++i;
		//printf("value of i: %i\n, i);
		
		//continue; //skips anything that follows, so will run all 2 billion loops again, only effects innermost loop
		
		if (i == im_getting_bored)
		{
			printf("Okay I'm getting bored of this crap!\n");
			break;
	}
}
	while (i < 5);
	printf("after do-while looping i is: %i\n", i); // doesnt need brackets as after do
	
	//while (i < INT_MAX); //max number of loops!, done in seconds!
	//printf("after do-while looping i is: %i\n", i);
	
	for (i = 0; i <5 ; ++i);
	{
	printf("for loops are nice \n");	
	}
	return 0;
}
