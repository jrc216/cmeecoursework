#!/usr/bin/python
"""More examples of regular expressions"""

r'^abc[ab]+\s\t\d' 
#^ - Start at beginning of line
#abc - match 'abc'
#[ab] - match a or b
#+ - match a or b multiple times
#\s - match a space
#\t match a tab
#\d - match a numeric character

r'^\d{1,2}\/\d{1,2}\/\d{4}$'
#^ - start at beginning
#\d - match a numeric character
#{1,2} - match the previous character between 1 and 2 times
#\/ - looking for a forward slash
#\d - find a numeric character
#{1,2} - match the previous character between 1 and 2 times
#\/ - look for a forward slash
#\d - match a numeric character
#{4} - match the previous character 4 times
#$ - match end of line

r'\s*[a-zA-Z,\s]+\s*'
#\s - match a space
#* - match the previous charater zero or more times
#[a-zA-Z,\s] - match any lower or upper case letter followed by a space
#+ - match previous part one or more times
#\s* - match any number of spaces

import re
date = '19940927'
match=re.search(r'^(19|20)\d\d', date)
match=re.search(r'(0\d|1[012])', date)
match=re.search(r'[12]\d|3[01]', date)

match=re.search(r'^(19|20)\d\d(0[1-9]]|1[012])[012]\d|3[01]', date)
match.group()
match=re.search(r'^(19|20)+\d{2}}(0[1-9]]|1[012])(0[1-9]|[12]\d|3[01]', date)
(^(1(?=9))|^(2(?=0)))[0-9]{3}((1(?=0))|(1(?=1))])|(1(?=2))|0)[0-9]{1} ((3(?=0))|(3(?=1))|[0-2])[0-9]{1}])
#(1(?=0)) - 1 has to be followed by 0
#?* = followed by anything
#scipyarange()) = scipy.array(range())
