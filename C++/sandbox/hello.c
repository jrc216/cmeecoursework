#include <stdio.h> // standard io, include library standard io, includes definition of print f

int main (void) // take a parameter called void, which is nothing, function called main which returns an integer, look her first if reading a C executable
{
	printf("You're entering a world of pain!\n"); // whitespace v. flexible in c, print if is name of function, arg in brackets, DONT FORGET SEMICOLON!!
												//literal value not a variable
												// \n is an argument, adds a line after the string
	return 0; // exit code - return 0 to os not the int 0!
}
// use gcc to compile, use gcc hello.c -o hello - change name of file for when run it
// use ./hello to run
// OR
// gcc hello.c
//./a.out

//can comment by wrapphing text: 
/* this is a comment and will be ignored */
//this is another comment. it is a single-line one, 
//only works until end of line
