import csv
import sys
import pdb
import doctest

#Define function
def is_an_oak(name):
    """ Returns True if name is starts with 'quercus '
        >>> is_an_oak('quercus')
        True
    """
    
    return name.lower()==('quercus') # changed from ".startswith" to "==" and got rid of space at end as is a csv file!
    
print(is_an_oak.__doc__)

def main(argv): 
    f = open('../Data/TestOaksData.csv','rb')
    g = open('../Data/JustOaksData.csv','wb') # changed directory route
    taxa = csv.reader(f) #enables reading of file
    csvwrite = csv.writer(g) # enables writing to file
    oaks = set() # made oaks a set 
    for row in taxa:
        print row
        print "The genus is", row[0] # first row
        if is_an_oak(row[0]):
            print row[0]
            print 'FOUND AN OAK!'
            print " "
            csvwrite.writerow([row[0], row[1]]) #enables writing to a csv    
    
    return 0
    
if (__name__ == "__main__"): # enables main argument to be called from the command line
    status = main(sys.argv)

doctest.testmod() # doctest
