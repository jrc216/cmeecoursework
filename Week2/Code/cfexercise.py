#!/usr/bin/python
"""Some functions exemplifying the use of control statements"""
#docstings are considered part of the running code (normal comments are #stripped). Hence, you can access your docstrings at run time.

__author__='Joanna Clarke (jrc216@ic.ac.uk)'
__version__='0.0.1'

#imports
import sys # modules to interface our program with the operating system

# How many times will 'hello' be printed?
# 1)
for i in range(3, 17): #for i in range 3-17
	print 'hello' 

#2) 
for j in range(12):
	if j % 3 == 0: # % modulus - returns remainder after dividing, so if remainder of dividing by three is 0 then 'hello' will be printed
		print 'hello'
		
#3)
for j in range(15):
	if j % 5 == 3:
		print 'hello'
	elif j % 4 == 3:
		print 'hello'
		
#4)
z = 0
while z != 15:  #while z is not 15
	print 'hello'
	z = z + 3 # add three to Z in each new loop

#5)
z = 12
while z < 100:
	if z == 31: #z is exactly equal to 31
		for k in range(7): #k has a value of 7
			print 'hello'
	elif z == 18: #else if z is exactly = to 18
		print 'hello'
	z = z + 1 # add one to z in each loop
			
# what does fooXX do? - name of funtion that will run one numbers have been 
#inputted and main is run, so can be run as a script 
def foo1(x):
	return x ** 0.5 # x to the power of 0.5
	
def foo2(x, y): #input two numbers, x and y
	if x > y: #if x is greater than y
		return x 
	return y
	
def foo3(x, y, z): # input three values
	if x > y: # if x is greater than y
		tmp = y #y is equal to a temporary file
		y = x 
		x = tmp # x is a temporary file
	if y > z: # if y s greater tan z
		tmp = z #z becomes a temporary file
		y = tmp
	return [x, y, z]

def foo4(x):
	result = 1 # result starts at 1
	for i in range(1, x + 1): # for i in the range of 1 to x+1
		result = result * i # updates result to equal result x the iteration
	return result
	
#This is a recursive function, meaning that the function calls itself
#read about it at
#en.wikipedia.org/wiki/Recursion_(computer_science)
def foo5(x):
	if x == 1:
		return 1
	return x * foo5(x - 1) # your value of x multiplied by the result of foo
	                       # for if x was one less e.g when x=3 foo5 = 6, so when x=4
	                       #foo5 =4 x 6=24 
	
	
	
def main(argv): # main argumant to be run that recalls the funtions defined above
	# sys.exit ("dont want to do this right now!")
	print foo1(10) # inputs the value of ten for the first foo function to run on
	print foo1(5)
	print foo2(2,3)
	print foo2(3,2)
	print foo3(2,3,4)
	print foo3(4,3,2)
	print foo4(4)
	print foo4(6)
	print foo5(1)
	print foo5(4)
	return 0

if (__name__=="__main__"): #makes sure the "main" function is called from commandline
	status=main(sys.argv)
	sys.exit (status)

		
		
