CODE
GIS_prac.py - running the GIS3 practical using a python script which outputs the zonal statistics
Spatial_methods_practical.R - fitting statistical models to spatial data, using R, bird diversity across Africa
clifford.test.R - code to perform a clifford test, how much observations of two variables tend to covary

DATA
bio1_15.tif - temperature data west side of Europe
bio1_16.tif - temperature data east side of Europe
bio1_UK_BNG.tif - reprojection of temperature data UK using british national grid
bio12_15.tif - precipitation data west side of Europe
bio12_16.tif - precipitation data east side of europe
bio12_UK_BNG.tif - reprojection of precipitation data UK using british national grid
g250_06.tif - land cover classes of the EU
g250_06_UK_BNG.tif - clipped version of above, isolating the UK reprojectong using the british national grid
avian_richness.tif - raster of avian richness in africa
elev.tif - raster of elevation across Africa
mean_aet.tif - raster of average evapotransporation across Africa
mean_temp.tif - raster of mean annual temperature across Africa

RESULTS
zonalstats.csv - output of GIS_prac.py
correlogram - correlogram showing avian richness across africa
histograms - histograms of each raster ( species richness, evapottranspiration, temperature, elevation)
plots - plots of the four rasters above
richness_plots - plots of avian richness across africa
spplot - spplot of autocorrelation of richness taing into account neighbours
spplot_compare - 3 spplots of actual richness and of the simple linear model and the more complex model
spplot_residuals - spplots of the residual richness
