WEEK3

CODE
apply1.R - using apply: applying the same function to rows/columns of a matrix
apply2.R - using apply on a matrix
basic_io.R - A simple R script to illustrate R input and output.
boilerplate.R - showing how to create your own function involing 2 arguments
break.R - showing how you can break out of a while loop if there's an error, so that the loop will continue runnig after an error
browse.R - debugging in R using browser()
Case_study1.R - creating a function that makes a matrix
Case_study2.R - A function that creates an ellipse
Case_study3.R - creating a bar graph using ggplot which as multiple layers
Case_study4.R - creating a linear regression and plotting it using ggplot2
Cotrol.R - examples of control loops in R
DataWrang.R - examples of data wrangling using PoundHill dataset by transposing a matrix and using the melt function
get_treeHeight.R - Takes a csv file name from the command line and outputs the result to a file
next.R - using next to pass to next iteration of loop
PP_Lattice.R - creating lattice graphs using ecol data, and workin out means and medians of data using tapply 
PP_Regress.R - creating scatter plots of predator mass as a function of prey mass separarated by feeding interacion and coloured by predator lifestage, also calculations of the regression lines using lm
PP_regress_loc.R - same as previous file but separated by location instead of predator lifstage
Ricker_model.R - 
run_get_TreeHeight.sh - a bash script to run get_TreeHeight.R
sample.R - running a simulation that involves sampling from a population using the sample function
SQLinR.R - script to allow R to use SQLite which is used to allow R to acccess, update and manage SQLite databases
TAutoCorr.R - 
TreeHeight.R - calculating heights of trees 
try.R - using try, which catches any errors and keeps going
Vectorize1.R - sums all elements in a matrix, faster when not using loops
Vectorize.R - Runs the stochastic (with gaussian fluctuations) Ricker Eqn 


RESULTS
MyBars.pdf - output of Case_study3.R, a bar graph using ggplot2 which as multiple layers
My_lin_reg.R - output of Case_study4.R - plotting a linear regression 
Pred_Lattice.pdf - density lattice plots of predator mass for different feeding types
Prey_Lattice.pdf - density lattice plots of prey mass for different feeding types
SizeRatio_Lattice.pdf - density lattice plots of prey/predator mass for different feeding types
PP_Regress_Figure.pdf -  scatter plots of predator mass as a function of prey mass separarated by feeding interacion and coloured by predator lifestage
PP_Regress_Loc_Figure.pdf - same as above but coloured by location instead of predator lifestage
Girko.pdf - 
MyData.csv
MyFirst-ggplot2-Figure.pdf - scater plot of predator mass as a function of prey mass using ggplot2
PP_Results.csv - mean and median results for PP_regress.r
Pred_Prey_overlay.pdf
TreeHts.csv
trees_treeheight.csv
