WEEK 1
CODE
boilerplate.sh - sell script template
ConcatenateTwoFiles.sh - merge two files
Countlines.sh - count the number of lines in a file
csvtospace.sh - convert a csv file to a space separated file
tabtocsv.sh - convert a space/tab separated file to csv
MyExampleScript.sh - an example script
UnixPrac1.txt - Practical week 1
variables.sh - shows the use of variables

DATA
spawannxs.txt - List of Species of Marine and Coastal Flora 
Protected Under Article 11(1)(a)
fasta files - gene sequence files
temperature files 

CHAPTER 1 UNIX AND LINUX
sudo - super user do - installs programs
mkdir - makes a new directory
cd - change directory
rm - remove
rm -r - removes a directory
ls - list
mv - move file
pwd - print working directory
touch - create a file
sort -n - sort lines in numerical order
> redirect output of coomand to a file on disk
>> append output from a command to a file on a disk
echo - print 
cat - print contents of file
wc - count characters
wc -l - count lines
ls head -5 - list top 5 rows in file
grep - matches strings in a file
grep -i - case sensitive
grep -w - only full words
grep -A (or -B) 2 - show two lines after match (or before)
grep -n - shows line number of match
find . -name "FIlename" - finds a file
find (direcotry name) - shows all files in directory
find . -maxdepth 2 -name - limits fepth of search to only 2 subdirectories
find . -type d - only find directories


CHAPTER 2 SHELL SCRIPTING - GEANY
bash myscript.sh - runs shell script
#!/bin/bash - shebang, this is a bash script
tr -s "\b" " " - removes excess spaces
tr -d "a" - removes all a's
tr [:lower:] [:upper:] - set to uppercase
tr -d [:alpha:] | tr -s "\b" "," - gets rid of text and deletes all spaces and replaces with a comma
cat $1 | tr -s "\t" "," >> $1.csv - convert to csv
cat $1.csv | tr -s "," " " >> $1.txt - convert csv to txt
MYVAr=myvalue - explicitly assign a value to a variable 
read MYVAR - assiging a vlue to a variable by reading it in


VERSION CONTROL - GIT
git status - show current status
git add
git commit -m - commit changes with added message
git push - sned changes to remote repository
git pull - upload from remote repository
git clone - download repository from remote server
git reset --hard
git commit -am - return to previous state
git branch - testing out a branch
git chechout master
git merge - merge branch
git branch -d - delete branch
git branch -D - abandon branch
find . -mindepth 1 -maxdepth 1 -type d -print -exec git -C {} pull \; - git pulling on multiple r subdirectories (each a separate repository)

LATEX
pdflatex - (x2) converts to pdf
bibtex - adds bibliography
















