#!/bin/bash
# Author: Your name you.login@imperial.ac.uk
# Script: tabtocsv.sh
# Desc: substitute the tabs in the files with commas
#saves the output into a space file
# Arguments: 1-> space file
# Date: Oct 2015

echo "Creating a space separated file from a csv file"
cat $1.csv | tr -s "," " " >> $1.txt
echo "Done"
exit







