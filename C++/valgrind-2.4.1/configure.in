# Process this file with autoconf to produce a configure script.
AC_INIT(Valgrind, 2.4.1, valgrind-users@lists.sourceforge.net)
AC_CONFIG_SRCDIR(coregrind/vg_main.c)
AM_CONFIG_HEADER(config.h)
AM_INIT_AUTOMAKE

AM_MAINTAINER_MODE

# Checks for programs.
CFLAGS="-Wno-long-long"

AC_PROG_LN_S
AC_PROG_CC
AM_PROG_CC_C_O
AC_PROG_CPP
AC_PROG_CXX
AC_PROG_RANLIB

# Check for the compiler support
if test "${GCC}" != "yes" ; then
   AC_MSG_ERROR([Valgrind relies on GCC to be compiled])
fi

# figure out where perl lives
AC_PATH_PROG(PERL, perl)

# figure out where gdb lives
AC_PATH_PROG(GDB, gdb)
# autoheader tries to execute the 3rd string or something;  I get warnings when
# it's defined.  So just leave it undefined.  --njn 2002-Apr-18
AC_DEFINE_UNQUOTED(GDB_PATH, "$GDB", "")

# some older automake's don't have it so try something on our own
ifdef([AM_PROG_AS],[AM_PROG_AS],
[
AS="${CC}"
AC_SUBST(AS)

ASFLAGS=""
AC_SUBST(ASFLAGS)
])

# This variable will collect the individual suppression files
# depending on the results of autoconf

DEFAULT_SUPP=""


# We don't want gcc 2.7
AC_MSG_CHECKING([for a supported version of gcc])

gcc_version=`${CC} --version | head -n 1`

case "${gcc_version}" in
     gcc-2.7.*)
	AC_MSG_RESULT([no (${gcc_version})])
	AC_MSG_ERROR([please use a recent (>= gcc-2.95) version of gcc])
	;;

     *)
	AC_MSG_RESULT([ok (${gcc_version})])
	;;
esac


# Checks for the platform
AC_CANONICAL_HOST

AC_MSG_CHECKING([for a supported CPU])
AC_SUBST(VG_ARCH)
AC_SUBST(KICKSTART_BASE)
AC_SUBST(ARCH_CORE_AM_CFLAGS)
AC_SUBST(ARCH_TOOL_AM_CFLAGS)
AC_SUBST(ARCH_CORE_AM_CCASFLAGS)

case "${host_cpu}" in
     i?86) 
	AC_MSG_RESULT([ok (${host_cpu})])
        VG_ARCH="x86"
        KICKSTART_BASE="0xb0000000"
        ARCH_CORE_AM_CFLAGS="@PREFERRED_STACK_BOUNDARY@ -DELFSZ=32"
        ARCH_TOOL_AM_CFLAGS="@PREFERRED_STACK_BOUNDARY@"
        ARCH_CORE_AM_CCASFLAGS=""
        ;;

     powerpc*)
        AC_MSG_RESULT([no (${host_cpu})])
        VG_ARCH="ppc"
        KICKSTART_BASE="0x70000000"
        ARCH_CORE_AM_CFLAGS="-DELFSZ=32"
        ARCH_TOOL_AM_CFLAGS="-fpic"
        ARCH_CORE_AM_CCASFLAGS="-Wa,-maltivec"
        AC_MSG_ERROR([PowerPC not supported. Sorry])
        ;;

     *) 
	AC_MSG_RESULT([no (${host_cpu})])
	AC_MSG_ERROR([Unsupported host architecture. Sorry])
	;;
esac

AC_MSG_CHECKING([for a supported OS])
AC_SUBST(VG_OS)

case "${host_os}" in
     *linux*)
	AC_MSG_RESULT([ok (${host_os})])
        VG_OS="linux"

        # Ok, this is linux. Check the kernel version
        AC_MSG_CHECKING([for the kernel version])

        kernel=`uname -r`

        case "${kernel}" in
             2.6.*) 
        	    AC_MSG_RESULT([2.6 family (${kernel})])
        	    AC_DEFINE([KERNEL_2_6], 1, [Define to 1 if you're using Linux 2.6.x])
        	    ;;

             2.4.*) 
        	    AC_MSG_RESULT([2.4 family (${kernel})])
        	    AC_DEFINE([KERNEL_2_4], 1, [Define to 1 if you're using Linux 2.4.x])
        	    ;;

             *) 
        	    AC_MSG_RESULT([unsupported (${kernel})])
        	    AC_MSG_ERROR([Valgrind works on kernels 2.4, 2.6])
        	    ;;
        esac

        ;;

     *freebsd*)
        AC_MSG_RESULT([ok (${host_os})])
        VG_OS="freebsd"
        ;;

     *) 
	AC_MSG_RESULT([no (${host_os})])
	AC_MSG_ERROR([Valgrind is operating system specific. Sorry. Please consider doing a port.])
	;;
esac

AC_MSG_CHECKING([for a supported CPU/OS combination])
AC_SUBST(VG_PLATFORM)

VG_PLATFORM="$VG_ARCH-$VG_OS"

case $VG_PLATFORM in
    x86-linux)
        AC_MSG_RESULT([ok (${host_cpu}-${host_os})])
        ;;

    *)
        AC_MSG_RESULT([no (${host_cpu}-${host_os})])
        AC_MSG_ERROR([Valgrind is platform specific. Sorry. Please consider doin
g a port.])
        ;;
esac

AC_SUBST(DEFAULT_SUPP)

glibc=""

AC_EGREP_CPP([GLIBC_21], [
#include <features.h>
#ifdef __GNU_LIBRARY__
 #if (__GLIBC__ == 2 && __GLIBC_MINOR__ == 1)
  GLIBC_21
 #endif
#endif
],
glibc="2.1")

AC_EGREP_CPP([GLIBC_22], [
#include <features.h>
#ifdef __GNU_LIBRARY__
 #if (__GLIBC__ == 2 && __GLIBC_MINOR__ == 2)
  GLIBC_22
 #endif
#endif
],
glibc="2.2")

AC_EGREP_CPP([GLIBC_23], [
#include <features.h>
#ifdef __GNU_LIBRARY__
 #if (__GLIBC__ == 2 && __GLIBC_MINOR__ == 3)
  GLIBC_23
 #endif
#endif
],
glibc="2.3")

AC_EGREP_CPP([GLIBC_24], [
#include <features.h>
#ifdef __GNU_LIBRARY__
 #if (__GLIBC__ == 2 && __GLIBC_MINOR__ == 4)
  GLIBC_24
 #endif
#endif
],
glibc="2.4")

AC_MSG_CHECKING([the glibc version])

case "${glibc}" in
     2.1)
	AC_MSG_RESULT(2.1 family)
	AC_DEFINE([GLIBC_2_1], 1, [Define to 1 if you're using glibc 2.1.x])
	DEFAULT_SUPP="${DEFAULT_SUPP} glibc-2.1.supp"
	;;

     2.2)
	AC_MSG_RESULT(2.2 family)
	AC_DEFINE([GLIBC_2_2], 1, [Define to 1 if you're using glibc 2.2.x])
	DEFAULT_SUPP="${DEFAULT_SUPP} glibc-2.2.supp"
	;;

     2.3)
	AC_MSG_RESULT(2.3 family)
	AC_DEFINE([GLIBC_2_3], 1, [Define to 1 if you're using glibc 2.3.x])
	DEFAULT_SUPP="${DEFAULT_SUPP} glibc-2.3.supp"
	;;

     2.4)
	AC_MSG_RESULT(2.4 family)
	AC_DEFINE([GLIBC_2_4], 1, [Define to 1 if you're using glibc 2.4.x])
	DEFAULT_SUPP="${DEFAULT_SUPP} glibc-2.4.supp"
	;;

     *)
	AC_MSG_RESULT(unsupported version)
	AC_MSG_ERROR([Valgrind requires the glibc version 2.1, 2.2, 2.3 or 2.4])
	;;
esac

# We don't know how to detect the X client library version
# (detecting the server version is easy, bu no help).  So we
# just use a hack: always include the suppressions for both
# versions 3 and 4.
AC_PATH_X
if test "${no_x}" != 'yes' ; then
   DEFAULT_SUPP="${DEFAULT_SUPP} xfree-4.supp"
   DEFAULT_SUPP="${DEFAULT_SUPP} xfree-3.supp"
fi


# does this compiler support -mpreferred-stack-boundary=2 ?
AC_MSG_CHECKING([if gcc accepts -mpreferred-stack-boundary])

safe_CFLAGS=$CFLAGS
CFLAGS="-mpreferred-stack-boundary=2"

AC_TRY_COMPILE(, [
int main () { return 0 ; }
],
[
PREFERRED_STACK_BOUNDARY="-mpreferred-stack-boundary=2"
AC_MSG_RESULT([yes])
], [
PREFERRED_STACK_BOUNDARY=""
AC_MSG_RESULT([no])
])
CFLAGS=$safe_CFLAGS

AC_SUBST(PREFERRED_STACK_BOUNDARY)

# does this compiler support -Wno-pointer-sign ?
AC_MSG_CHECKING([if gcc accepts -Wno-pointer-sign ])

safe_CFLAGS=$CFLAGS
CFLAGS="-Wno-pointer-sign"

AC_TRY_COMPILE(, [
int main () { return 0 ; }
],
[
no_pointer_sign=yes
AC_MSG_RESULT([yes])
], [
no_pointer_sign=no
AC_MSG_RESULT([no])
])
CFLAGS=$safe_CFLAGS

USE_MMX_REGISTER=""
AC_MSG_CHECKING([if gcc accepts -mmmx ])

safe_CFLAGS=$CFLAGS
CFLAGS="-mmmx"

AC_TRY_COMPILE(, [
int main () { return 0 ; }
],
[
handles_mmmx=yes
AC_MSG_RESULT([yes])
], [
handles_mmmx=no
AC_MSG_RESULT([no])
])

if test "$handles_mmmx" = "yes"; then
  USE_MMX_REGISTER="-mmmx"
fi

AC_SUBST([USE_MMX_REGISTER])

USE_SSE_REGISTER=""
AC_MSG_CHECKING([if gcc accepts -msse ])

safe_CFLAGS=$CFLAGS
CFLAGS="-msse"

AC_TRY_COMPILE(, [
int main () { return 0 ; }
],
[
handles_msse=yes
AC_MSG_RESULT([yes])
], [
handles_msse=no
AC_MSG_RESULT([no])
])

if test "$handles_msse" = "yes"; then
  USE_SSE_REGISTER="-msse"
fi

AC_SUBST([USE_SSE_REGISTER])



if test x$no_pointer_sign = xyes; then
  CFLAGS="$CFLAGS -Wno-pointer-sign"
fi

# Check for TLS support in the compiler and linker
AC_CACHE_CHECK([for TLS support], vg_cv_tls,
	       [AC_ARG_ENABLE(tls, [  --enable-tls            platform supports TLS],
		[vg_cv_tls=$enableval],
               	[AC_RUN_IFELSE([AC_LANG_PROGRAM([[static __thread int foo;]],
                                                [[return foo;]])],
                               [vg_cv_tls=yes],
                               [vg_cv_tls=no])])])

if test "$vg_cv_tls" = yes; then
AC_DEFINE([HAVE_TLS], 1, [can use __thread to define thread-local variables])
fi

# Check for PIE support in the compiler and linker
AC_CACHE_CHECK([for PIE support], vg_cv_pie,
	       [AC_ARG_ENABLE(pie, [  --enable-pie            platform supports PIE linking],
		[vg_cv_pie=$enableval],
               	[vg_cv_pie=no] )])
if test "$vg_cv_pie" = yes; then
AC_DEFINE([HAVE_PIE], 1, [can create position-independent executables])
fi
AM_CONDITIONAL(USE_PIE, test "$vg_cv_pie" = "yes")


# Checks for header files.
AC_HEADER_STDC
AC_CHECK_HEADERS([sys/endian.h endian.h mqueue.h])

# Checks for typedefs, structures, and compiler characteristics.
AC_TYPE_UID_T
AC_TYPE_OFF_T
AC_TYPE_SIZE_T
AC_HEADER_TIME

# Checks for library functions.
AC_FUNC_MEMCMP
AC_FUNC_MMAP
AC_TYPE_SIGNAL

AC_CHECK_FUNCS([floor memchr memset mkdir strchr strdup strpbrk strrchr strstr semtimedop])

AC_OUTPUT(
   Makefile 
   valgrind.spec
   valgrind.pc
   docs/Makefile 
   tests/Makefile 
   tests/vg_regtest 
   tests/unused/Makefile 
   include/valgrind.h 
   include/Makefile 
   include/x86/Makefile 
   include/linux/Makefile 
   include/x86-linux/Makefile 
   auxprogs/Makefile
   coregrind/Makefile 
   coregrind/demangle/Makefile 
   coregrind/docs/Makefile
   coregrind/x86/Makefile
   coregrind/linux/Makefile
   coregrind/x86-linux/Makefile
   addrcheck/Makefile
   addrcheck/tests/Makefile
   addrcheck/tests/x86/Makefile
   addrcheck/docs/Makefile
   memcheck/Makefile
   memcheck/tests/Makefile
   memcheck/tests/x86/Makefile
   memcheck/docs/Makefile
   cachegrind/Makefile
   cachegrind/x86/Makefile
   cachegrind/tests/Makefile
   cachegrind/tests/x86/Makefile
   cachegrind/docs/Makefile
   cachegrind/cg_annotate
   helgrind/Makefile
   helgrind/tests/Makefile
   helgrind/tests/x86/Makefile
   helgrind/docs/Makefile
   massif/Makefile
   massif/hp2ps/Makefile
   massif/tests/Makefile
   massif/docs/Makefile
   corecheck/Makefile
   corecheck/tests/Makefile
   corecheck/docs/Makefile
   lackey/Makefile
   lackey/tests/Makefile
   lackey/docs/Makefile
   none/Makefile
   none/tests/Makefile
   none/tests/x86/Makefile
   none/docs/Makefile
) 

cat<<EOF

Using the following suppressions by default: 

      ${DEFAULT_SUPP}
EOF

cat<<EOF > default.supp
# This is a generated file, composed of the following suppression rules:
#
#      ${DEFAULT_SUPP}
#

EOF

for file in ${DEFAULT_SUPP} ; do
    cat ${srcdir}/$file >> default.supp
done
