#!/usr/bin/python

"""trying out subprocess"""

import subprocess

#run bash commands from a python script
subprocess.os.system("geany boilerplate.py") & # & to close terminal then run next command
subprocess.os.system("gedit ../Data/TestOaksData.csv") &
subprocess.os.system("python boilerplate.py")


#compile Latex document
subprocess.os.system("pdflatex yourlatexdoc.tex")

#does the same as above
subprocess.Popen("geany boilerplate.py", shell=True).wait()

#make your code OS (Linux, Windows, Mac) independent. The result would be appropriately different on Windows (with backslashes instead of forward
#slashes).
subprocess.os.path.join('directory', 'subdirectory', 'file')

#catch output
MyPath = subprocess.os.path.join('directory', 'subdirectory', 'file')
