/*-*- c -*- ----------------------------------------------------------*/
/*--- The only header your tool will ever need to #include...      ---*/
/*---                                                       tool.h ---*/
/*--------------------------------------------------------------------*/

/*
   This file is part of Valgrind, an extensible x86 protected-mode
   emulator for monitoring program execution on x86-Unixes.

   Copyright (C) 2000-2005 Julian Seward
      jseward@acm.org

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307, USA.

   The GNU General Public License is contained in the file COPYING.
*/

#ifndef __TOOL_H
#define __TOOL_H

#include <stdarg.h>       /* ANSI varargs stuff  */

#include "basic_types.h"
#include "tool_asm.h"           /* asm stuff */
#include "tool_arch.h"          /* arch-specific tool stuff */
#include "vki.h"

/*====================================================================*/
/*=== Build options and table sizes.                               ===*/
/*====================================================================*/

/* You should be able to change these options or sizes, recompile, and
   still have a working system. */

/* The maximum number of pthreads that we support.  This is
   deliberately not very high since our implementation of some of the
   scheduler algorithms is surely O(N) in the number of threads, since
   that's simple, at least.  And (in practice) we hope that most
   programs do not need many threads. */
#define VG_N_THREADS 100

/* Maximum number of pthread keys available.  Again, we start low until
   the need for a higher number presents itself. */
#define VG_N_THREAD_KEYS 50


/*====================================================================*/
/*=== Useful macros                                                ===*/
/*====================================================================*/

#define mycat_wrk(aaa,bbb) aaa##bbb
#define mycat(aaa,bbb) mycat_wrk(aaa,bbb)

/* No, really.  I _am_ that strange. */
#define OINK(nnn) VG_(message)(Vg_DebugMsg, "OINK %d",nnn)

/* Path to all our library/aux files */
extern const Char *VG_(libdir);


/*====================================================================*/
/*=== Core/tool interface version                                  ===*/
/*====================================================================*/

/* The major version number indicates binary-incompatible changes to the
   interface;  if the core and tool major versions don't match, Valgrind
   will abort.  The minor version indicates binary-compatible changes.

   (Update: as it happens, we're never using the minor version number, because
   there's no point in doing so.)
*/
#define VG_CORE_INTERFACE_MAJOR_VERSION   7
#define VG_CORE_INTERFACE_MINOR_VERSION   0

typedef struct _ToolInfo {
   Int	sizeof_ToolInfo;
   Int	interface_major_version;
   Int	interface_minor_version;

   /* Initialise tool.   Must do the following:
      - initialise the `details' struct, via the VG_(details_*)() functions
      - register any helpers called by generated code
      
      May do the following:
      - initialise the `needs' struct to indicate certain requirements, via
      the VG_(needs_*)() functions
      - initialize all the tool's entrypoints via the VG_(init_*)() functions
      - register any tool-specific profiling events
      - any other tool-specific initialisation
   */
   void        (*sk_pre_clo_init) ( void );

   /* Specifies how big the shadow segment should be as a ratio to the
      client address space.  0 for no shadow segment. */
   float	shadow_ratio;
} ToolInfo;

/* Every tool must include this macro somewhere, exactly once. */
#define VG_DETERMINE_INTERFACE_VERSION(pre_clo_init, shadow)		\
   const ToolInfo SK_(tool_info) = {					\
      .sizeof_ToolInfo         = sizeof(ToolInfo),			\
      .interface_major_version = VG_CORE_INTERFACE_MAJOR_VERSION,	\
      .interface_minor_version = VG_CORE_INTERFACE_MINOR_VERSION,	\
      .sk_pre_clo_init         = pre_clo_init,				\
      .shadow_ratio	       = shadow,				\
   };

/*====================================================================*/
/*=== Command-line options                                         ===*/
/*====================================================================*/

/* Use this for normal null-termination-style string comparison */
#define VG_STREQ(s1,s2) (s1 != NULL && s2 != NULL \
                         && VG_(strcmp)((s1),(s2))==0)

/* Use these for recognising tool command line options -- stops comparing
   once whitespace is reached. */
#define VG_CLO_STREQ(s1,s2)     (0==VG_(strcmp_ws)((s1),(s2)))
#define VG_CLO_STREQN(nn,s1,s2) (0==VG_(strncmp_ws)((s1),(s2),(nn)))

/* Higher-level command-line option recognisers;  use in if/else chains */

#define VG_BOOL_CLO(qq_option, qq_var) \
   if (VG_CLO_STREQ(arg, qq_option"=yes")) { (qq_var) = True; } \
   else if (VG_CLO_STREQ(arg, qq_option"=no"))  { (qq_var) = False; }

#define VG_STR_CLO(qq_option, qq_var) \
   if (VG_CLO_STREQN(VG_(strlen)(qq_option)+1, arg, qq_option"=")) { \
      (qq_var) = &arg[ VG_(strlen)(qq_option)+1 ]; \
   }

#define VG_NUM_CLO(qq_option, qq_var) \
   if (VG_CLO_STREQN(VG_(strlen)(qq_option)+1, arg, qq_option"=")) { \
      (qq_var) = (Int)VG_(atoll)( &arg[ VG_(strlen)(qq_option)+1 ] ); \
   }

/* Bounded integer arg */
#define VG_BNUM_CLO(qq_option, qq_var, qq_lo, qq_hi) \
   if (VG_CLO_STREQN(VG_(strlen)(qq_option)+1, arg, qq_option"=")) { \
      (qq_var) = (Int)VG_(atoll)( &arg[ VG_(strlen)(qq_option)+1 ] ); \
      if ((qq_var) < (qq_lo)) (qq_var) = (qq_lo); \
      if ((qq_var) > (qq_hi)) (qq_var) = (qq_hi); \
   }


/* Verbosity level: 0 = silent, 1 (default), > 1 = more verbose. */
extern Int   VG_(clo_verbosity);

/* Profile? */
extern Bool  VG_(clo_profile);

/* Call this if a recognised option was bad for some reason.
   Note: don't use it just because an option was unrecognised -- return 'False'
   from SKN_(process_cmd_line_option) to indicate that. */
extern void VG_(bad_option) ( Char* opt );

/* Client args */
extern Int    VG_(client_argc);
extern Char** VG_(client_argv);

/* Client environment.  Can be inspected with VG_(getenv)() */
extern Char** VG_(client_envp);


/*====================================================================*/
/*=== Printing messages for the user                               ===*/
/*====================================================================*/

/* Print a message prefixed by "??<pid>?? "; '?' depends on the VgMsgKind.
   Should be used for all user output. */

typedef
   enum { Vg_UserMsg,         /* '?' == '=' */
          Vg_DebugMsg,        /* '?' == '-' */
          Vg_DebugExtraMsg,   /* '?' == '+' */
          Vg_ClientMsg        /* '?' == '*' */
   }
   VgMsgKind;

/* Functions for building a message from multiple parts. */
extern int VG_(start_msg)  ( VgMsgKind kind );
extern int VG_(add_to_msg) ( const Char* format, ... );
/* Ends and prints the message.  Appends a newline. */
extern int VG_(end_msg)    ( void );

/* Send a single-part message.  Appends a newline. */
extern int VG_(message)    ( VgMsgKind kind, const Char* format, ... );
extern int VG_(vmessage)   ( VgMsgKind kind, const Char* format, va_list vargs );


/*====================================================================*/
/*=== Profiling                                                    ===*/
/*====================================================================*/

/* Nb: VGP_(register_profile_event)() relies on VgpUnc being the first one */
#define VGP_CORE_LIST \
   /* These ones depend on the core */                \
   VGP_PAIR(VgpUnc,         "unclassified"),          \
   VGP_PAIR(VgpStartup,     "startup"),               \
   VGP_PAIR(VgpRun,         "running"),               \
   VGP_PAIR(VgpSched,       "scheduler"),             \
   VGP_PAIR(VgpMalloc,      "low-lev malloc/free"),   \
   VGP_PAIR(VgpCliMalloc,   "client  malloc/free"),   \
   VGP_PAIR(VgpTranslate,   "translate-main"),        \
   VGP_PAIR(VgpToUCode,     "to-ucode"),              \
   VGP_PAIR(VgpFromUcode,   "from-ucode"),            \
   VGP_PAIR(VgpImprove,     "improve"),               \
   VGP_PAIR(VgpESPUpdate,   "ESP-update"),            \
   VGP_PAIR(VgpRegAlloc,    "reg-alloc"),             \
   VGP_PAIR(VgpLiveness,    "liveness-analysis"),     \
   VGP_PAIR(VgpDoLRU,       "do-lru"),                \
   VGP_PAIR(VgpSlowFindT,   "slow-search-transtab"),  \
   VGP_PAIR(VgpExeContext,  "exe-context"),           \
   VGP_PAIR(VgpReadSyms,    "read-syms"),             \
   VGP_PAIR(VgpSearchSyms,  "search-syms"),           \
   VGP_PAIR(VgpAddToT,      "add-to-transtab"),       \
   VGP_PAIR(VgpCoreSysWrap, "core-syscall-wrapper"),  \
   VGP_PAIR(VgpDemangle,    "demangle"),              \
   VGP_PAIR(VgpCoreCheapSanity,     "core-cheap-sanity"),     \
   VGP_PAIR(VgpCoreExpensiveSanity, "core-expensive-sanity"), \
   /* These ones depend on the tool */                \
   VGP_PAIR(VgpPreCloInit,  "pre-clo-init"),          \
   VGP_PAIR(VgpPostCloInit, "post-clo-init"),         \
   VGP_PAIR(VgpInstrument,  "instrument"),            \
   VGP_PAIR(VgpSkinSysWrap, "tool-syscall-wrapper"),  \
   VGP_PAIR(VgpSkinCheapSanity,     "tool-cheap-sanity"),     \
   VGP_PAIR(VgpSkinExpensiveSanity, "tool-expensive-sanity"), \
   VGP_PAIR(VgpFini,        "fini")

#define VGP_PAIR(n,name) n
typedef enum { VGP_CORE_LIST } VgpCoreCC;
#undef  VGP_PAIR

/* When registering tool profiling events, ensure that the 'n' value is in
 * the range (VgpFini+1..) */
extern void VGP_(register_profile_event) ( Int n, Char* name );

extern void VGP_(pushcc) ( UInt cc );
extern void VGP_(popcc)  ( UInt cc );

/* Define them only if they haven't already been defined by vg_profile.c */
#ifndef VGP_PUSHCC
#  define VGP_PUSHCC(x)
#endif
#ifndef VGP_POPCC
#  define VGP_POPCC(x)
#endif


/*====================================================================*/
/*=== Useful stuff to call from generated code                     ===*/
/*====================================================================*/

/* ------------------------------------------------------------------ */
/* General stuff */

/* 64-bit counter for the number of basic blocks done. */
extern ULong VG_(bbs_done);

/* Check if an address is 4-byte aligned */
#define IS_ALIGNED4_ADDR(aaa_p) (0 == (((UInt)(aaa_p)) & 3))
#define IS_ALIGNED8_ADDR(aaa_p) (0 == (((UInt)(aaa_p)) & 7))


/* ------------------------------------------------------------------ */
/* Thread-related stuff */

/* Special magic value for an invalid ThreadId.  It corresponds to
   LinuxThreads using zero as the initial value for
   pthread_mutex_t.__m_owner and pthread_cond_t.__c_waiting. */
#define VG_INVALID_THREADID ((ThreadId)(0))

/* ThreadIds are simply indices into the VG_(threads)[] array. */
typedef
   UInt
   ThreadId;

/* Get the simulated %esp */
extern Addr VG_(get_thread_stack_pointer)(ThreadId tid);

/* Get the TID of the thread which currently has the CPU */
extern ThreadId VG_(get_running_tid)        ( void );

static inline ThreadId VG_(get_VCPU_tid)(void) { return VG_(get_running_tid)(); }

static inline ThreadId VG_(get_current_or_recent_tid)(void) { return VG_(get_running_tid)(); }

static inline Addr VG_(get_stack_pointer)(void)
{
   return VG_(get_thread_stack_pointer)(VG_(get_running_tid)());
}


/* Searches through all thread's stacks to see if any match.  Returns
   VG_INVALID_THREADID if none match. */
extern ThreadId VG_(first_matching_thread_stack)
                        ( Bool (*p) ( Addr stack_min, Addr stack_max, void* d ),
                          void* d );


/*====================================================================*/
/*=== Valgrind's version of libc                                   ===*/
/*====================================================================*/

/* Valgrind doesn't use libc at all, for good reasons (trust us).  So here
   are its own versions of C library functions, but with VG_ prefixes.  Note
   that the types of some are slightly different to the real ones.  Some
   additional useful functions are provided too; descriptions of how they
   work are given below. */

#if !defined(NULL)
#  define NULL ((void*)0)
#endif


/* ------------------------------------------------------------------ */
/* stdio.h
 *
 * Note that they all output to the file descriptor given by the
 * --log-fd/--log-file/--log-socket argument, which defaults to 2 (stderr).
 * Hence no need for VG_(fprintf)().
 */
extern UInt VG_(printf)  ( const char *format, ... );
/* too noisy ...  __attribute__ ((format (printf, 1, 2))) ; */
extern UInt VG_(sprintf) ( Char* buf, Char *format, ... );
extern UInt VG_(vprintf) ( void(*send)(Char, void *),
                           const Char *format, va_list vargs, void *send_arg );

extern Int  VG_(rename) ( Char* old_name, Char* new_name );

/* ------------------------------------------------------------------ */
/* stdlib.h */

extern void* VG_(malloc)         ( SizeT nbytes );
extern void  VG_(free)           ( void* p );
extern void* VG_(calloc)         ( SizeT n, SizeT bytes_per_elem );
extern void* VG_(realloc)        ( void* p, SizeT size );
extern void* VG_(malloc_aligned) ( SizeT align_bytes, SizeT nbytes );

extern void  VG_(print_malloc_stats) ( void );

/* terminate everything */
extern void  VG_(exit)( Int status )
             __attribute__ ((__noreturn__));

/* terminate the calling thread - probably not what you want */
extern void  VG_(exit_single)( Int status )
             __attribute__ ((__noreturn__));

/* Prints a panic message (a constant string), appends newline and bug
   reporting info, aborts. */
__attribute__ ((__noreturn__))
extern void  VG_(skin_panic) ( Char* str );

/* Looks up VG_(client_envp) */
extern Char* VG_(getenv) ( Char* name );

/* Get client resource limit*/
extern Int VG_(getrlimit) ( Int resource, struct vki_rlimit *rlim );

/* Set client resource limit*/
extern Int VG_(setrlimit) ( Int resource, const struct vki_rlimit *rlim );

/* Crude stand-in for the glibc system() call. */
extern Int   VG_(system) ( Char* cmd );

extern Long  VG_(atoll)  ( Char* str );

/* Like atoll(), but converts a number of base 16 */
extern Long  VG_(atoll16) ( Char* str );

/* Like atoll(), but converts a number of base 36 */
extern Long  VG_(atoll36) ( Char* str );

/* Like qsort(), but does shell-sort.  The size==1/2/4 cases are specialised. */
extern void VG_(ssort)( void* base, SizeT nmemb, SizeT size,
                        Int (*compar)(void*, void*) );


/* ------------------------------------------------------------------ */
/* ctype.h */
extern Bool VG_(isspace) ( Char c );
extern Bool VG_(isdigit) ( Char c );
extern Char VG_(toupper) ( Char c );


/* ------------------------------------------------------------------ */
/* string.h */
extern Int   VG_(strlen)         ( const Char* str );
extern Char* VG_(strcat)         ( Char* dest, const Char* src );
extern Char* VG_(strncat)        ( Char* dest, const Char* src, Int n );
extern Char* VG_(strpbrk)        ( const Char* s, const Char* accept );
extern Char* VG_(strcpy)         ( Char* dest, const Char* src );
extern Char* VG_(strncpy)        ( Char* dest, const Char* src, Int ndest );
extern Int   VG_(strcmp)         ( const Char* s1, const Char* s2 );
extern Int   VG_(strncmp)        ( const Char* s1, const Char* s2, Int nmax );
extern Char* VG_(strstr)         ( const Char* haystack, Char* needle );
extern Char* VG_(strchr)         ( const Char* s, Char c );
extern Char* VG_(strrchr)        ( const Char* s, Char c );
extern Char* VG_(strdup)         ( const Char* s);
extern void* VG_(memcpy)         ( void *d, const void *s, Int sz );
extern void* VG_(memset)         ( void *s, Int c, Int sz );
extern Int   VG_(memcmp)         ( const void* s1, const void* s2, Int n );

/* Like strcmp() and strncmp(), but stop comparing at any whitespace. */
extern Int   VG_(strcmp_ws)      ( const Char* s1, const Char* s2 );
extern Int   VG_(strncmp_ws)     ( const Char* s1, const Char* s2, Int nmax );

/* Like strncpy(), but if 'src' is longer than 'ndest' inserts a '\0' as the
   last character. */
extern void  VG_(strncpy_safely) ( Char* dest, const Char* src, Int ndest );

/* Mini-regexp function.  Searches for 'pat' in 'str'.  Supports
 * meta-symbols '*' and '?'.  '\' escapes meta-symbols. */
extern Bool  VG_(string_match)   ( const Char* pat, const Char* str );


/* ------------------------------------------------------------------ */
/* math.h */
/* Returns the base-2 logarithm of x. */
extern Int VG_(log2) ( Int x );


/* ------------------------------------------------------------------ */
/* unistd.h, fcntl.h, sys/stat.h */
extern Int  VG_(getdents)( UInt fd, struct vki_dirent *dirp, UInt count );
extern Int  VG_(readlink)( Char* path, Char* buf, UInt bufsize );
extern Int  VG_(getpid)  ( void );
extern Int  VG_(getppid) ( void );
extern Int  VG_(getpgrp) ( void );
extern Int  VG_(gettid)	 ( void );
extern Int  VG_(setpgid) ( Int pid, Int pgrp );

extern Int  VG_(open)   ( const Char* pathname, Int flags, Int mode );
extern Int  VG_(read)   ( Int fd, void* buf, Int count);
extern Int  VG_(write)  ( Int fd, const void* buf, Int count);
extern OffT VG_(lseek)  ( Int fd, OffT offset, Int whence);
extern void VG_(close)  ( Int fd );

extern Int  VG_(pipe)   ( Int fd[2] );

/* Nb: VG_(rename)() declared in stdio.h section above */
extern Int  VG_(unlink) ( Char* file_name );
extern Int  VG_(stat)   ( Char* file_name, struct vki_stat* buf );
extern Int  VG_(fstat)  ( Int   fd,        struct vki_stat* buf );
extern Int  VG_(dup2)   ( Int oldfd, Int newfd );

extern Char* VG_(getcwd) ( Char* buf, SizeT size );

/* Easier to use than VG_(getcwd)() -- does the buffer fiddling itself.
   String put into 'cwd' is VG_(malloc)'d, and should be VG_(free)'d.
   Returns False if it fails.  Will fail if the pathname is > 65535 bytes. */
extern Bool VG_(getcwd_alloc) ( Char** cwd );

/* ------------------------------------------------------------------ */
/* assert.h */
/* Asserts permanently enabled -- no turning off with NDEBUG.  Hurrah! */
#define VG__STRING(__str)  #__str

#define sk_assert(expr)                                               \
  ((void) ((expr) ? 0 :						      \
	   (VG_(skin_assert_fail) (VG__STRING(expr),	              \
			           __FILE__, __LINE__,                \
                                   __PRETTY_FUNCTION__), 0)))

__attribute__ ((__noreturn__))
extern void VG_(skin_assert_fail) ( const Char* expr, const Char* file,
                                    Int line, const Char* fn );


/* ------------------------------------------------------------------ */
/* Get memory by anonymous mmap. */
extern void* VG_(get_memory_from_mmap) ( SizeT nBytes, Char* who );

extern Bool VG_(is_client_addr) (Addr a);
extern Addr VG_(get_client_base)(void);
extern Addr VG_(get_client_end) (void);
extern Addr VG_(get_client_size)(void);

extern Bool VG_(is_shadow_addr) (Addr a);
extern Addr VG_(get_shadow_base)(void);
extern Addr VG_(get_shadow_end) (void);
extern Addr VG_(get_shadow_size)(void);

extern void *VG_(shadow_alloc)(UInt size);

extern Bool VG_(is_addressable)(Addr p, SizeT sz, UInt prot);

extern Addr VG_(client_alloc)(Addr base, SizeT len, UInt prot, UInt flags);
extern void VG_(client_free)(Addr addr);

extern Bool VG_(is_valgrind_addr)(Addr a);

/* Register an interest in apparently internal faults; used code which
   wanders around dangerous memory (ie, leakcheck).  The catcher is
   not expected to return. */
extern void VG_(set_fault_catcher)(void (*catcher)(Int sig, Addr addr));

/* initialize shadow pages in the range [p, p+sz) This calls
   init_shadow_page for each one.  It should be a lot more efficient
   for bulk-initializing shadow pages than faulting on each one. 
*/
extern void VG_(init_shadow_range)(Addr p, UInt sz, Bool call_init);

/* Calls into the core used by leak-checking */

/* Calls "add_rootrange" with each range of memory which looks like a
   plausible source of root pointers. */
extern void VG_(find_root_memory)(void (*add_rootrange)(Addr addr, SizeT sz));

/* Calls "mark_addr" with register values (which may or may not be pointers) */
extern void VG_(mark_from_registers)(void (*mark_addr)(Addr addr));

/* ------------------------------------------------------------------ */
/* signal.h.

   Note that these use the vk_ (kernel) structure
   definitions, which are different in places from those that glibc
   defines.  Since we're operating right at the kernel interface, glibc's view
   of the world is entirely irrelevant. */

/* --- Signal set ops --- */
extern Int  VG_(sigfillset)  ( vki_sigset_t* set );
extern Int  VG_(sigemptyset) ( vki_sigset_t* set );

extern Bool VG_(isfullsigset)  ( const vki_sigset_t* set );
extern Bool VG_(isemptysigset) ( const vki_sigset_t* set );

extern Int  VG_(sigaddset)   ( vki_sigset_t* set, Int signum );
extern Int  VG_(sigdelset)   ( vki_sigset_t* set, Int signum );
extern Int  VG_(sigismember) ( const vki_sigset_t* set, Int signum );

extern void VG_(sigaddset_from_set) ( vki_sigset_t* dst, vki_sigset_t* src );
extern void VG_(sigdelset_from_set) ( vki_sigset_t* dst, vki_sigset_t* src );

/* --- Mess with the kernel's sig state --- */
extern Int VG_(sigprocmask) ( Int how, const vki_sigset_t* set,
                              vki_sigset_t* oldset );
extern Int VG_(sigaction)   ( Int signum,
                              const struct vki_sigaction* act,
                              struct vki_sigaction* oldact );

extern Int VG_(sigtimedwait)( const vki_sigset_t *, vki_siginfo_t *, 
			      const struct vki_timespec * );

extern Int VG_(signal)      ( Int signum, void (*sighandler)(Int) );
extern Int VG_(sigaltstack) ( const vki_stack_t* ss, vki_stack_t* oss );

extern Int VG_(kill)        ( Int pid, Int signo );
extern Int VG_(tkill)       ( Int pid, Int signo );
extern Int VG_(sigpending)  ( vki_sigset_t* set );

extern Int VG_(waitpid)	    ( Int pid, Int *status, Int options );

/* ------------------------------------------------------------------ */
/* socket.h. */

extern Int VG_(getsockname) ( Int sd, struct vki_sockaddr *name, Int *namelen);
extern Int VG_(getpeername) ( Int sd, struct vki_sockaddr *name, Int *namelen);
extern Int VG_(getsockopt) ( Int sd, Int level, Int optname, void *optval,
                             Int *optlen);

/* ------------------------------------------------------------------ */
/* other, randomly useful functions */
extern UInt VG_(read_millisecond_timer) ( void );

extern Bool VG_(has_cpuid) ( void );

extern void VG_(cpuid) ( UInt eax,
                         UInt *eax_ret, UInt *ebx_ret,
                         UInt *ecx_ret, UInt *edx_ret );

/*====================================================================*/
/*=== UCode definition                                             ===*/
/*====================================================================*/

/* Tags which describe what operands are.  Must fit into 4 bits, which
   they clearly do. */
typedef
enum { TempReg  =0, /* virtual temp-reg */
       ArchReg  =1, /* simulated integer reg */
       ArchRegS =2, /* simulated segment reg */
       RealReg  =3, /* real machine's real reg */
       SpillNo  =4, /* spill slot location */
       Literal  =5, /* literal; .lit32 field has actual value */
       Lit16    =6, /* literal; .val[123] field has actual value */
       NoValue  =7  /* operand not in use */
     }
     Tag;

/* Invalid register numbers (can't be negative) */
#define INVALID_TEMPREG 999999999
#define INVALID_REALREG 999999999

/* Microinstruction opcodes. */
typedef
   enum {
      NOP,         /* Null op */

      LOCK,	   /* Indicate the existence of a LOCK prefix (functionally NOP) */

      /* Moving values around */
      GET,  PUT,   /* simulated register <--> TempReg */
      GETF, PUTF,  /* simulated %eflags  <--> TempReg */
      LOAD, STORE, /* memory  <--> TempReg            */
      MOV,         /* TempReg <--> TempReg            */
      CMOV,        /* Used for cmpxchg and cmov       */

      /* Arithmetic/logical ops */
      MUL, UMUL,                	  /* Multiply */
      ADD, ADC, SUB, SBB,                 /* Add/subtract (w/wo carry)     */
      AND, OR,  XOR, NOT,                 /* Boolean ops                   */
      SHL, SHR, SAR, ROL, ROR, RCL, RCR,  /* Shift/rotate (w/wo carry)     */
      NEG,                                /* Negate                        */
      INC, DEC,                           /* Increment/decrement           */
      BSWAP,                              /* Big-endian <--> little-endian */
      CC2VAL,                             /* Condition code --> 0 or 1     */
      WIDEN,                              /* Signed or unsigned widening   */

      /* Conditional or unconditional jump  */
      JMP,

      /* FPU ops */
      FPU,           /* Doesn't touch memory */
      FPU_R, FPU_W,  /* Reads/writes memory  */

      /* ------------ MMX ops ------------ */
      /* In this and the SSE encoding, bytes at higher addresses are
	 held in bits [7:0] in these 16-bit words.  I guess this means
	 it is a big-endian encoding. */

      /* 1 byte, no memrefs, no iregdefs, copy exactly to the
	 output.  Held in val1[7:0]. */
      MMX1,

      /* 2 bytes, no memrefs, no iregdefs, copy exactly to the
	 output.  Held in val1[15:0]. */
      MMX2,

      /* 3 bytes, no memrefs, no iregdefs, copy exactly to the
         output.  Held in val1[15:0] and val2[7:0]. */
      MMX3,

      /* 2 bytes, reads/writes mem.  Insns of the form
         bbbbbbbb:mod mmxreg r/m.
         Held in val1[15:0], and mod and rm are to be replaced
         at codegen time by a reference to the Temp/RealReg holding
         the address.  Arg2 holds this Temp/Real Reg.
         Transfer is always at size 8.
      */
      MMX2_MemRd,
      MMX2_MemWr,

      /* 3 bytes, reads/writes mem.  Insns of the form
         bbbbbbbb:mod mmxreg r/m:bbbbbbbb
         Held in val1[15:0] and val2[7:0], and mod and rm are to be
         replaced at codegen time by a reference to the Temp/RealReg
         holding the address.  Arg2 holds this Temp/Real Reg.
         Transfer is always at size 8.
      */
      MMX2a1_MemRd,

      /* 2 bytes, reads/writes an integer ("E") register.  Insns of the form
         bbbbbbbb:11 mmxreg ireg.
         Held in val1[15:0], and ireg is to be replaced
         at codegen time by a reference to the relevant RealReg.
         Transfer is always at size 4.  Arg2 holds this Temp/Real Reg.
      */
      MMX2_ERegRd,
      MMX2_ERegWr,

      /* ------------ SSE/SSE2 ops ------------ */
      /* In the following:

         a digit N indicates the next N bytes are to be copied exactly
         to the output.

         'a' indicates a mod-xmmreg-rm byte, where the mod-rm part is
         to be replaced at codegen time to a Temp/RealReg holding the
         address.

         'e' indicates a byte of the form '11 xmmreg ireg', where ireg
         is read or written, and is to be replaced at codegen time by
         a reference to the relevant RealReg.  'e' because it's the E
         reg in Intel encoding parlance.

         'g' indicates a byte of the form '11 ireg xmmreg', where ireg
         is read or written, and is to be replaced at codegen time by
         a reference to the relevant RealReg.  'g' because it's called
         G in Intel parlance. */

      /* 3 bytes, no memrefs, no iregdefs, copy exactly to the
         output.  Held in val1[15:0] and val2[7:0]. */
      SSE3,

      /* 3 bytes, reads/writes mem.  Insns of the form
         bbbbbbbb:bbbbbbbb:mod mmxreg r/m.
         Held in val1[15:0] and val2[7:0], and mod and rm are to be
         replaced at codegen time by a reference to the Temp/RealReg
         holding the address.  Arg3 holds this Temp/Real Reg.
         Transfer is usually, but not always, at size 16.  */
      SSE2a_MemRd,
      SSE2a_MemWr,

      /* 4 bytes, writes an integer register.  Insns of the form
         bbbbbbbb:bbbbbbbb:11 ireg bbb.
         Held in val1[15:0] and val2[7:0], and ireg is to be replaced
         at codegen time by a reference to the relevant RealReg.
         Transfer is always at size 4.  Arg3 holds this Temp/Real Reg.
      */
      SSE2g_RegWr,

      /* 5 bytes, writes an integer register.  Insns of the form
         bbbbbbbb:bbbbbbbb:11 ireg bbb :bbbbbbbb. Held in
         val1[15:0] and val2[7:0] and lit32[7:0], and ireg is to be
         replaced at codegen time by a reference to the relevant
         RealReg.  Transfer is always at size 4.  Arg3 holds this
         Temp/Real Reg.
      */
      SSE2g1_RegWr,

      /* 5 bytes, reads an integer register.  Insns of the form
         bbbbbbbb:bbbbbbbb:11 bbb ireg :bbbbbbbb. Held in
         val1[15:0] and val2[7:0] and lit32[7:0], and ireg is to be
         replaced at codegen time by a reference to the relevant
         RealReg.  Transfer is always at size 4.  Arg3 holds this
         Temp/Real Reg.
      */
      SSE2e1_RegRd,

      /* 4 bytes, no memrefs, no iregdefs, copy exactly to the
         output.  Held in val1[15:0] and val2[15:0]. */
      SSE4,

      /* 4 bytes, reads/writes mem.  Insns of the form
         bbbbbbbb:bbbbbbbb:bbbbbbbb:mod mmxreg r/m.
         Held in val1[15:0] and val2[15:0], and mod and rm are to be
         replaced at codegen time by a reference to the Temp/RealReg
         holding the address.  Arg3 holds this Temp/Real Reg.
         Transfer is at stated size.  */
      SSE3a_MemRd,
      SSE3a_MemWr,

      /* 4 bytes, reads/writes mem.  Insns of the form
         bbbbbbbb:bbbbbbbb:mod mmxreg r/m:bbbbbbbb
         Held in val1[15:0] and val2[15:0], and mod and rm are to be
         replaced at codegen time by a reference to the Temp/RealReg
         holding the address.  Arg3 holds this Temp/Real Reg.
         Transfer is at stated size.  */
      SSE2a1_MemRd,

      /* 4 bytes, writes an integer register.  Insns of the form
         bbbbbbbb:bbbbbbbb:bbbbbbbb:11 ireg bbb.
         Held in val1[15:0] and val2[15:0], and ireg is to be replaced
         at codegen time by a reference to the relevant RealReg.
         Transfer is always at size 4.  Arg3 holds this Temp/Real Reg.
      */
      SSE3g_RegWr,

      /* 5 bytes, writes an integer register.  Insns of the form
         bbbbbbbb:bbbbbbbb:bbbbbbbb: 11 ireg bbb :bbbbbbbb. Held in
         val1[15:0] and val2[15:0] and lit32[7:0], and ireg is to be
         replaced at codegen time by a reference to the relevant
         RealReg.  Transfer is always at size 4.  Arg3 holds this
         Temp/Real Reg.
      */
      SSE3g1_RegWr,

      /* 4 bytes, reads an integer register.  Insns of the form
         bbbbbbbb:bbbbbbbb:bbbbbbbb:11 bbb ireg.
         Held in val1[15:0] and val2[15:0], and ireg is to be replaced
         at codegen time by a reference to the relevant RealReg.
         Transfer is always at size 4.  Arg3 holds this Temp/Real Reg.
      */
      SSE3e_RegRd,
      SSE3e_RegWr, /* variant that writes Ereg, not reads it */

      /* 5 bytes, reads an integer register.  Insns of the form
         bbbbbbbb:bbbbbbbb:bbbbbbbb: 11 bbb ireg :bbbbbbbb. Held in
         val1[15:0] and val2[15:0] and lit32[7:0], and ireg is to be
         replaced at codegen time by a reference to the relevant
         RealReg.  Transfer is always at size 4.  Arg3 holds this
         Temp/Real Reg.
      */
      SSE3e1_RegRd,

      /* 4 bytes, reads memory, writes an integer register, but is
         nevertheless an SSE insn.  The insn is of the form
         bbbbbbbb:bbbbbbbb:bbbbbbbb:mod ireg rm where mod indicates
         memory (ie is not 11b) and ireg is the int reg written.  The
         first 4 bytes are held in lit32[31:0] since there is
         insufficient space elsewhere.  mod and rm are to be replaced
         at codegen time by a reference to the Temp/RealReg holding
         the address.  Arg1 holds this Temp/RealReg.  ireg is to be
         replaced at codegen time by a reference to the relevant
         RealReg in which the answer is to be written.  Arg2 holds
         this Temp/RealReg.  Transfer to the destination reg is always
         at size 4.  However the memory read can be at sizes 4 or 8
         and so this is what the sz field holds.  Note that the 4th
         byte of the instruction (the modrm byte) is redundant, but we
         store it anyway so as to be consistent with all other SSE
         uinstrs.
      */
      SSE3ag_MemRd_RegWr,

      /* 5 bytes, no memrefs, no iregdefs, copy exactly to the
         output.  Held in val1[15:0], val2[15:0] and val3[7:0]. */
      SSE5,

      /* 5 bytes, reads/writes mem.  Insns of the form
         bbbbbbbb:bbbbbbbb:bbbbbbbb:mod mmxreg r/m:bbbbbbbb
         Held in val1[15:0], val2[15:0], lit32[7:0].
         mod and rm are to be replaced at codegen time by a reference
         to the Temp/RealReg holding the address.  Arg3 holds this
         Temp/Real Reg.  Transfer is always at size 16.  */
      SSE3a1_MemRd,

      /* ------------------------ */

      /* Not strictly needed, but improve address calculation translations. */
      LEA1,  /* reg2 := const + reg1 */
      LEA2,  /* reg3 := const + reg1 + reg2 * 1,2,4 or 8 */

      /* Hack for x86 REP insns.  Jump to literal if TempReg/RealReg
         is zero. */
      JIFZ,

      /* Advance the simulated %eip by some small (< 128) number. */
      INCEIP,

      /* Dealing with segment registers */
      GETSEG, PUTSEG,   /* simulated segment register <--> TempReg */
      USESEG,           /* (LDT/GDT index, virtual addr) --> linear addr */

      /* Not for translating x86 calls -- only to call helpers */
      CALLM_S, CALLM_E, /* Mark start/end of CALLM push/pop sequence */
      PUSH, POP, CLEAR, /* Add/remove/zap args for helpers           */
      CALLM,            /* Call assembly-code helper                 */

      /* Not for translating x86 calls -- only to call C helper functions of
         up to three arguments (or two if the functions has a return value).
         Arguments and return value must be word-sized.  More arguments can
         be faked with global variables (eg. use VG_(lit_to_globvar)()).

         Seven possibilities: 'arg[123]' show where args go, 'ret' shows
         where return value goes (if present).

         CCALL(-,    -,    -   )    void f(void)
         CCALL(arg1, -,    -   )    void f(UInt arg1)
         CCALL(arg1, arg2, -   )    void f(UInt arg1, UInt arg2)
         CCALL(arg1, arg2, arg3)    void f(UInt arg1, UInt arg2, UInt arg3)
         CCALL(-,    -,    ret )    UInt f(UInt)
         CCALL(arg1, -,    ret )    UInt f(UInt arg1)
         CCALL(arg1, arg2, ret )    UInt f(UInt arg1, UInt arg2) */
      CCALL,

      /* This opcode makes it easy for tools that extend UCode to do this to
         avoid opcode overlap:

           enum { EU_OP1 = DUMMY_FINAL_UOPCODE + 1, ... }

         WARNING: Do not add new opcodes after this one!  They can be added
         before, though. */
      DUMMY_FINAL_UOPCODE
   }
   Opcode;


/* Condition codes, using the Intel encoding.  CondAlways is an extra. */
typedef
   enum {
      CondO      = 0,  /* overflow           */
      CondNO     = 1,  /* no overflow        */
      CondB      = 2,  /* below              */
      CondNB     = 3,  /* not below          */
      CondZ      = 4,  /* zero               */
      CondNZ     = 5,  /* not zero           */
      CondBE     = 6,  /* below or equal     */
      CondNBE    = 7,  /* not below or equal */
      CondS      = 8,  /* negative           */
      CondNS     = 9,  /* not negative       */
      CondP      = 10, /* parity even        */
      CondNP     = 11, /* not parity even    */
      CondL      = 12, /* jump less          */
      CondNL     = 13, /* not less           */
      CondLE     = 14, /* less or equal      */
      CondNLE    = 15, /* not less or equal  */
      CondAlways = 16  /* Jump always        */
   }
   Condcode;


/* Descriptions of additional properties of *unconditional* jumps. */
typedef
   enum {
     JmpBoring=0,   /* boring unconditional jump */
     JmpCall=1,     /* jump due to an x86 call insn */
     JmpRet=2,      /* jump due to an x86 ret insn */
     JmpSyscall=3,  /* do a system call, then jump */
     JmpClientReq=4,/* do a client request, then jump */
     JmpYield=5     /* do a yield, then jump */
   }
   JmpKind;


/* Flags.  User-level code can only read/write O(verflow), S(ign),
   Z(ero), A(ux-carry), C(arry), P(arity), and may also write
   D(irection).  That's a total of 7 flags.  A FlagSet is a bitset,
   thusly:
      76543210
       DOSZACP
   and bit 7 must always be zero since it is unused.

   Note: these Flag? values are **not** the positions in the actual
   %eflags register.  */

typedef UChar FlagSet;

#define FlagD (1<<6)
#define FlagO (1<<5)
#define FlagS (1<<4)
#define FlagZ (1<<3)
#define FlagA (1<<2)
#define FlagC (1<<1)
#define FlagP (1<<0)

#define FlagsOSZACP (FlagO | FlagS | FlagZ | FlagA | FlagC | FlagP)
#define FlagsOSZAP  (FlagO | FlagS | FlagZ | FlagA |         FlagP)
#define FlagsOSZCP  (FlagO | FlagS | FlagZ |         FlagC | FlagP)
#define FlagsOSACP  (FlagO | FlagS |         FlagA | FlagC | FlagP)
#define FlagsSZACP  (        FlagS | FlagZ | FlagA | FlagC | FlagP)
#define FlagsSZAP   (        FlagS | FlagZ | FlagA |         FlagP)
#define FlagsSZP    (        FlagS | FlagZ |                 FlagP)
#define FlagsZCP    (                FlagZ         | FlagC | FlagP)
#define FlagsOC     (FlagO |                         FlagC        )
#define FlagsAC     (                        FlagA | FlagC        )

#define FlagsALL    (FlagsOSZACP | FlagD)
#define FlagsEmpty  (FlagSet)0


/* flag positions in eflags */
#define EFlagC  (1 <<  0)       /* carry */
#define EFlagP  (1 <<  2)       /* parity */
#define EFlagA	(1 <<  4)	/* aux carry */
#define EFlagZ  (1 <<  6)       /* zero */
#define EFlagS  (1 <<  7)       /* sign */
#define EFlagD  (1 << 10)	/* direction */
#define EFlagO  (1 << 11)       /* overflow */
#define EFlagID (1 << 21)	/* changable if CPUID exists */

/* Liveness of general purpose registers, useful for code generation.
   Reg rank order 0..N-1 corresponds to bits 0..N-1, ie. first
   reg's liveness in bit 0, last reg's in bit N-1.  Note that
   these rankings don't match the Intel register ordering. */
typedef UInt RRegSet;

#define ALL_RREGS_DEAD      0                           /* 0000...00b */
#define ALL_RREGS_LIVE      ((1 << VG_MAX_REALREGS)-1)  /* 0011...11b */
#define UNIT_RREGSET(rank)  (1 << (rank))

#define IS_RREG_LIVE(rank,rregs_live) (rregs_live & UNIT_RREGSET(rank))
#define SET_RREG_LIVENESS(rank,rregs_live,b)       \
   do { RRegSet unit = UNIT_RREGSET(rank);         \
        if (b) rregs_live |= unit;                 \
        else   rregs_live &= ~unit;                \
   } while(0)


/* A Micro (u)-instruction. */
typedef
   struct {
      /* word 1 */
      UInt    lit32;      /* 32-bit literal */

      /* word 2 */
      UShort  val1;       /* first operand */
      UShort  val2;       /* second operand */

      /* word 3 */
      UShort  val3;       /* third operand */
      UChar   opcode;     /* opcode */
      UShort  size;       /* data transfer size */

      /* word 4 */
      FlagSet flags_r;    /* :: FlagSet */
      FlagSet flags_w;    /* :: FlagSet */
      UChar   tag1:4;     /* first  operand tag */
      UChar   tag2:4;     /* second operand tag */
      UChar   tag3:4;     /* third  operand tag */
      UChar   extra4b:4;  /* Spare field, used by WIDEN for src
                             -size, and by LEA2 for scale (1,2,4 or 8),
                             and by JMPs for original x86 instr size */

      /* word 5 */
      UChar   cond;            /* condition, for jumps */
      Bool    signed_widen:1;  /* signed or unsigned WIDEN ? */
      JmpKind jmpkind:3;       /* additional properties of unconditional JMP */

      /* Additional properties for UInstrs that call C functions:
           - CCALL
           - PUT (when %ESP is the target)
           - possibly tool-specific UInstrs
      */
      UChar   argc:2;          /* Number of args, max 3 */
      UChar   regparms_n:2;    /* Number of args passed in registers */
      Bool    has_ret_val:1;   /* Function has return value? */

      /* RealReg liveness;  only sensical after reg alloc and liveness
         analysis done.  This info is a little bit arch-specific --
         VG_MAX_REALREGS can vary on different architectures.  Note that
         to use this information requires converting between register ranks
         and the Intel register numbers, using VG_(realreg_to_rank)()
         and/or VG_(rank_to_realreg)() */
      RRegSet regs_live_after:VG_MAX_REALREGS;
   }
   UInstr;


typedef
   struct _UCodeBlock
   UCodeBlock;

extern Int     VG_(get_num_instrs) (UCodeBlock* cb);
extern Int     VG_(get_num_temps)  (UCodeBlock* cb);

extern UInstr* VG_(get_instr)      (UCodeBlock* cb, Int i);
extern UInstr* VG_(get_last_instr) (UCodeBlock* cb);


/*====================================================================*/
/*=== Instrumenting UCode                                          ===*/
/*====================================================================*/

/* Maximum number of registers read or written by a single UInstruction. */
#define VG_MAX_REGS_USED   3

/* Find what this instruction does to its regs, useful for
   analysis/optimisation passes.  `tag' indicates whether we're considering
   TempRegs (pre-reg-alloc) or RealRegs (post-reg-alloc).  `regs' is filled
   with the affected register numbers, `isWrites' parallels it and indicates
   if the reg is read or written.  If a reg is read and written, it will
   appear twice in `regs'.  `regs' and `isWrites' must be able to fit
   VG_MAX_REGS_USED elements. */
extern Int VG_(get_reg_usage) ( UInstr* u, Tag tag, Int* regs, Bool* isWrites );



/* ------------------------------------------------------------------ */
/* Virtual register allocation */

/* Get a new virtual register */
extern Int VG_(get_new_temp)   ( UCodeBlock* cb );

/* Get a new virtual shadow register */
extern Int VG_(get_new_shadow) ( UCodeBlock* cb );

/* Get a virtual register's corresponding virtual shadow register */
#define SHADOW(tempreg)  ((tempreg)+1)


/* ------------------------------------------------------------------ */
/* Low-level UInstr builders */
extern void VG_(new_NOP)     ( UInstr* u );
extern void VG_(new_UInstr0) ( UCodeBlock* cb, Opcode opcode, Int sz );
extern void VG_(new_UInstr1) ( UCodeBlock* cb, Opcode opcode, Int sz,
                               Tag tag1, UInt val1 );
extern void VG_(new_UInstr2) ( UCodeBlock* cb, Opcode opcode, Int sz,
                              Tag tag1, UInt val1,
                              Tag tag2, UInt val2 );
extern void VG_(new_UInstr3) ( UCodeBlock* cb, Opcode opcode, Int sz,
                              Tag tag1, UInt val1,
                              Tag tag2, UInt val2,
                              Tag tag3, UInt val3 );

/* Set read/write/undefined flags.  Undefined flags are treaten as written,
   but it's worth keeping them logically distinct. */
extern void VG_(set_flag_fields)  ( UCodeBlock* cb, FlagSet fr, FlagSet fw,
                                    FlagSet fu);
extern void VG_(set_lit_field)    ( UCodeBlock* cb, UInt lit32 );
extern void VG_(set_ccall_fields) ( UCodeBlock* cb, Addr fn, UChar argc,
                                    UChar regparms_n, Bool has_ret_val );
extern void VG_(set_cond_field)   ( UCodeBlock* cb, Condcode code );
extern void VG_(set_widen_fields) ( UCodeBlock* cb, UInt szs, Bool is_signed );

extern void VG_(copy_UInstr) ( UCodeBlock* cb, UInstr* instr );

extern Bool VG_(any_flag_use)( UInstr* u );

/* Macro versions of the above;  just shorter to type. */
#define uInstr0   VG_(new_UInstr0)
#define uInstr1   VG_(new_UInstr1)
#define uInstr2   VG_(new_UInstr2)
#define uInstr3   VG_(new_UInstr3)
#define uLiteral  VG_(set_lit_field)
#define uCCall    VG_(set_ccall_fields)
#define uCond     VG_(set_cond_field)
#define uWiden    VG_(set_widen_fields)
#define uFlagsRWU VG_(set_flag_fields)
#define newTemp   VG_(get_new_temp)
#define newShadow VG_(get_new_shadow)

/* Refer to `the last instruction stuffed in' (can be lvalue). */
#define LAST_UINSTR(cb) (cb)->instrs[(cb)->used-1]


/* ------------------------------------------------------------------ */
/* Higher-level UInstr sequence builders */

extern void VG_(lit_to_reg)    ( UCodeBlock* cb, UInt lit, UInt t );
extern UInt VG_(lit_to_newreg) ( UCodeBlock* cb, UInt lit         );

#define CB_F   UCodeBlock* cb, Addr f
#define EV     extern void
#define RPn    UInt regparms_n

/* Various CCALL builders, of the form "ccall_<args>_<retval>".  'R'
   represents a TempReg, 'L' represents a literal, '0' represents nothing
   (ie. no args, or no return value). */

EV VG_(ccall_0_0)   ( CB_F                                    );

EV VG_(ccall_R_0)   ( CB_F, UInt R1,                      RPn );
EV VG_(ccall_L_0)   ( CB_F, UInt L1,                      RPn );
EV VG_(ccall_R_R)   ( CB_F, UInt R1, UInt R_ret,          RPn );
EV VG_(ccall_L_R)   ( CB_F, UInt L1, UInt R_ret,          RPn );

EV VG_(ccall_RR_0)  ( CB_F, UInt R1, UInt R2,             RPn );
EV VG_(ccall_RL_0)  ( CB_F, UInt R1, UInt RL,             RPn );
EV VG_(ccall_LR_0)  ( CB_F, UInt L1, UInt R2,             RPn );
EV VG_(ccall_LL_0)  ( CB_F, UInt L1, UInt L2,             RPn );
EV VG_(ccall_RR_R)  ( CB_F, UInt R1, UInt R2, UInt R_ret, RPn );
EV VG_(ccall_RL_R)  ( CB_F, UInt R1, UInt L2, UInt R_ret, RPn );
EV VG_(ccall_LR_R)  ( CB_F, UInt L1, UInt R2, UInt R_ret, RPn );
EV VG_(ccall_LL_R)  ( CB_F, UInt L1, UInt L2, UInt R_ret, RPn );

EV VG_(ccall_RRR_0) ( CB_F, UInt R1, UInt R2, UInt R3,    RPn );
EV VG_(ccall_RLL_0) ( CB_F, UInt R1, UInt L2, UInt L3,    RPn );
EV VG_(ccall_LRR_0) ( CB_F, UInt L1, UInt R2, UInt R3,    RPn );
EV VG_(ccall_LLR_0) ( CB_F, UInt L1, UInt L2, UInt R3,    RPn );
EV VG_(ccall_LLL_0) ( CB_F, UInt L1, UInt L2, UInt L3,    RPn );

#undef CB_F
#undef EV
#undef RPn

/* One way around the 3-arg C function limit is to pass args via global
 * variables... ugly, but it works. */
void VG_(reg_to_globvar)(UCodeBlock* cb, UInt t,   UInt* globvar_ptr);
void VG_(lit_to_globvar)(UCodeBlock* cb, UInt lit, UInt* globvar_ptr);


/* Old, deprecated versions of some of the helpers (DO NOT USE) */
extern void VG_(call_helper_0_0) ( UCodeBlock* cb, Addr f);
extern void VG_(call_helper_1_0) ( UCodeBlock* cb, Addr f, UInt arg1,
                                   UInt regparms_n);
extern void VG_(call_helper_2_0) ( UCodeBlock* cb, Addr f, UInt arg1, UInt arg2,
                                   UInt regparms_n);
extern void VG_(set_global_var) ( UCodeBlock* cb, Addr globvar_ptr, UInt val);
extern void VG_(set_global_var_tempreg) ( UCodeBlock* cb, Addr globvar_ptr,
                                          UInt t_val);

/* ------------------------------------------------------------------ */
/* Allocating/freeing basic blocks of UCode */
extern UCodeBlock* VG_(setup_UCodeBlock) ( UCodeBlock* cb );
extern void        VG_(free_UCodeBlock)  ( UCodeBlock* cb );

/* ------------------------------------------------------------------ */
/* UCode pretty/ugly printing.  Probably only useful to call from a tool
   if VG_(needs).extended_UCode == True. */

/* When True, all generated code is/should be printed. */
extern Bool  VG_(print_codegen);

/* Pretty/ugly printing functions */
extern void  VG_(pp_UCodeBlock)  ( UCodeBlock* cb, Char* title );
extern void  VG_(pp_UInstr)      ( Int instrNo, UInstr* u );
extern void  VG_(pp_UInstr_regs) ( Int instrNo, UInstr* u );
extern void  VG_(up_UInstr)      ( Int instrNo, UInstr* u );
extern Char* VG_(name_UOpcode)   ( Bool upper, Opcode opc );
extern Char* VG_(name_UCondcode) ( Condcode cond );
extern void  VG_(pp_UOperand)    ( UInstr* u, Int operandNo,
                                   Int sz, Bool parens );

/* ------------------------------------------------------------------ */
/* Accessing archregs and their shadows */
extern UInt VG_(get_thread_archreg)     ( ThreadId tid, UInt archreg );

extern void VG_(set_thread_shadow_eflags) ( ThreadId tid, UInt val );

extern UInt VG_(get_thread_shadow_archreg) ( ThreadId tid, UInt archreg );
extern void VG_(set_thread_shadow_archreg) ( ThreadId tid, UInt archreg,
                                             UInt val );

/*====================================================================*/
/*=== Generating x86 code from UCode                               ===*/
/*====================================================================*/

/* All this only necessary for tools with VG_(needs).extends_UCode == True. */

/* This is the Intel register encoding -- integer regs. */
#define R_EAX 0
#define R_ECX 1
#define R_EDX 2
#define R_EBX 3
#define R_ESP 4
#define R_EBP 5
#define R_ESI 6
#define R_EDI 7

#define R_AL (0+R_EAX)
#define R_CL (0+R_ECX)
#define R_DL (0+R_EDX)
#define R_BL (0+R_EBX)
#define R_AH (4+R_EAX)
#define R_CH (4+R_ECX)
#define R_DH (4+R_EDX)
#define R_BH (4+R_EBX)

/* This is the Intel register encoding -- segment regs. */
#define R_ES 0
#define R_CS 1
#define R_SS 2
#define R_DS 3
#define R_FS 4
#define R_GS 5

/* For pretty printing x86 code */
extern const Char* VG_(name_of_mmx_gran) ( UChar gran );
extern const Char* VG_(name_of_mmx_reg)  ( Int mmxreg );
extern const Char* VG_(name_of_seg_reg)  ( Int sreg );
extern const Char* VG_(name_of_int_reg)  ( Int size, Int reg );
extern       Char  VG_(name_of_int_size) ( Int size );

/* Shorter macros for convenience */
#define nameIReg    VG_(name_of_int_reg)
#define nameISize   VG_(name_of_int_size)
#define nameSReg    VG_(name_of_seg_reg)
#define nameMMXReg  VG_(name_of_mmx_reg)
#define nameMMXGran VG_(name_of_mmx_gran)
#define nameXMMReg  VG_(name_of_xmm_reg)

/* Randomly useful things */
extern UInt  VG_(extend_s_8to32) ( UInt x );

/* Code emitters */
extern void VG_(emitB)    ( UInt b );
extern void VG_(emitW)    ( UInt w );
extern void VG_(emitL)    ( UInt l );
extern void VG_(new_emit) ( Bool upd_cc, FlagSet uses_flags, FlagSet sets_flags );

/* Finding offsets */
extern Int  VG_(shadow_reg_offset)   ( Int arch );
extern Int  VG_(shadow_flags_offset) ( void );

/* Convert reg ranks <-> Intel register ordering, for using register
   liveness information. */
extern Int VG_(realreg_to_rank) ( Int realreg );
extern Int VG_(rank_to_realreg) ( Int rank    );

/* Call a subroutine.  Does no argument passing, stack manipulations, etc. */
extern void VG_(synth_call) ( Addr target, Bool upd_cc, 
			      FlagSet use_flags, FlagSet set_flags );

/* For calling C functions -- saves caller save regs, pushes args, calls,
   clears the stack, restores caller save regs.  `fn' must be registered in
   the baseBlock first.  Acceptable tags are RealReg and Literal.  Optimises
   things, eg. by not preserving non-live caller-save registers.

   WARNING:  a UInstr should *not* be translated with synth_ccall() followed
   by some other x86 assembly code;  this will invalidate the results of
   vg_realreg_liveness_analysis() and everything will fall over.  */
extern void VG_(synth_ccall) ( Addr fn, Int argc, Int regparms_n, UInt argv[],
                               Tag tagv[], Int ret_reg,
                               RRegSet regs_live_before,
                               RRegSet regs_live_after );

/* Addressing modes */
extern void VG_(emit_amode_offregmem_reg)( Int off, Int regmem, Int reg );
extern void VG_(emit_amode_ereg_greg)    ( Int e_reg, Int g_reg );

/* v-size (4, or 2 with OSO) insn emitters */
extern void VG_(emit_movv_offregmem_reg) ( Int sz, Int off, Int areg, Int reg );
extern void VG_(emit_movv_reg_offregmem) ( Int sz, Int reg, Int off, Int areg );
extern void VG_(emit_movv_reg_reg)       ( Int sz, Int reg1, Int reg2 );
extern void VG_(emit_nonshiftopv_lit_reg)( Bool upd_cc, Int sz, Opcode opc, UInt lit,
                                           Int reg );
extern void VG_(emit_shiftopv_lit_reg)   ( Bool upd_cc, Int sz, Opcode opc, UInt lit,
                                           Int reg );
extern void VG_(emit_nonshiftopv_reg_reg)( Bool upd_cc, Int sz, Opcode opc,
                                           Int reg1, Int reg2 );
extern void VG_(emit_movv_lit_reg)       ( Int sz, UInt lit, Int reg );
extern void VG_(emit_unaryopv_reg)       ( Bool upd_cc, Int sz, Opcode opc, Int reg );
extern void VG_(emit_pushv_reg)          ( Int sz, Int reg );
extern void VG_(emit_popv_reg)           ( Int sz, Int reg );

extern void VG_(emit_pushl_lit32)        ( UInt int32 );
extern void VG_(emit_pushl_lit8)         ( Int lit8 );
extern void VG_(emit_cmpl_zero_reg)      ( Bool upd_cc, Int reg );
extern void VG_(emit_swapl_reg_EAX)      ( Int reg );
extern void VG_(emit_movv_lit_offregmem) ( Int sz, UInt lit, Int off,
                                           Int memreg );

/* b-size (1 byte) instruction emitters */
extern void VG_(emit_movb_lit_offregmem) ( UInt lit, Int off, Int memreg );
extern void VG_(emit_movb_reg_offregmem) ( Int reg, Int off, Int areg );
extern void VG_(emit_unaryopb_reg)       ( Bool upd_cc, Opcode opc, Int reg );
extern void VG_(emit_testb_lit_reg)      ( Bool upd_cc, UInt lit, Int reg );

/* zero-extended load emitters */
extern void VG_(emit_movzbl_offregmem_reg) ( Bool bounds, Int off, Int regmem, Int reg );
extern void VG_(emit_movzwl_offregmem_reg) ( Bool bounds, Int off, Int areg, Int reg );
extern void VG_(emit_movzwl_regmem_reg)    ( Bool bounds, Int reg1, Int reg2 );

/* misc instruction emitters */
extern void VG_(emit_call_reg)         ( Int reg );
extern void VG_(emit_add_lit_to_esp)   ( Int lit );
extern void VG_(emit_pushal)           ( void );
extern void VG_(emit_popal)            ( void );
extern void VG_(emit_AMD_prefetch_reg) ( Int reg );

/* jump emitters */
extern void VG_(init_target)	       ( Int *tgt );

extern void VG_(target_back)	       ( Int *tgt );
extern void VG_(target_forward)	       ( Int *tgt );
extern void VG_(emit_target_delta)     ( Int *tgt );

typedef enum {
   JP_NONE,			/* no prediction */
   JP_TAKEN,			/* predict taken */
   JP_NOT_TAKEN 		/* predict not taken */
} JumpPred;

extern void VG_(emit_jcondshort_delta) ( Bool simd_cc, Condcode cond, Int delta, JumpPred );
extern void VG_(emit_jcondshort_target)( Bool simd_cc, Condcode cond, Int *tgt, JumpPred );


/*====================================================================*/
/*=== Execution contexts                                           ===*/
/*====================================================================*/

/* Generic resolution type used in a few different ways, such as deciding
   how closely to compare two errors for equality. */
typedef
   enum { Vg_LowRes, Vg_MedRes, Vg_HighRes }
   VgRes;

typedef
   struct _ExeContext
   ExeContext;

/* Compare two ExeContexts.  Number of callers considered depends on `res':
     Vg_LowRes:  2
     Vg_MedRes:  4
     Vg_HighRes: all */
extern Bool VG_(eq_ExeContext) ( VgRes res,
                                 ExeContext* e1, ExeContext* e2 );

/* Print an ExeContext. */
extern void VG_(pp_ExeContext) ( ExeContext* );

/* Take a snapshot of the client's stack.  Search our collection of
   ExeContexts to see if we already have it, and if not, allocate a
   new one.  Either way, return a pointer to the context.  Context size
   controlled by --num-callers option.

   If called from generated code, use VG_(get_VCPU_tid)() to get the
   current ThreadId.  If called from non-generated code, the current
   ThreadId should be passed in by the core.
*/
extern ExeContext* VG_(get_ExeContext) ( ThreadId tid );

/* Get the nth IP from the ExeContext.  0 is the IP of the top function, 1
   is its caller, etc.  Returns 0 if there isn't one, or if n is greater
   than VG_(clo_backtrace_size), set by the --num-callers option. */
extern Addr VG_(get_EIP_from_ExeContext) ( ExeContext* e, UInt n );

/* Just grab the client's IP, as a much smaller and cheaper
   indication of where they are.  Use is basically same as for
   VG_(get_ExeContext)() above.
*/
extern Addr VG_(get_EIP)( ThreadId tid );

/* For tools needing more control over stack traces:  walks the stack to get
   instruction pointers from the top stack frames for thread 'tid'.  Maximum of
   'n_ips' addresses put into 'ips';  0 is the top of the stack, 1 is its
   caller, etc. */
extern UInt VG_(stack_snapshot) ( ThreadId tid, Addr* ips, UInt n_ips );

/* Does the same thing as VG_(pp_ExeContext)(), just with slightly
   different input. */
extern void VG_(mini_stack_dump) ( Addr ips[], UInt n_ips );


/*====================================================================*/
/*=== Error reporting                                              ===*/
/*====================================================================*/

/* ------------------------------------------------------------------ */
/* Suppressions describe errors which we want to suppress, ie, not
   show the user, usually because it is caused by a problem in a library
   which we can't fix, replace or work around.  Suppressions are read from
   a file at startup time.  This gives flexibility so that new
   suppressions can be added to the file as and when needed.
*/

typedef
   Int         /* Do not make this unsigned! */
   SuppKind;

/* The tool-relevant parts of a suppression are:
     kind:   what kind of suppression; must be in the range (0..)
     string: use is optional.  NULL by default.
     extra:  use is optional.  NULL by default.  void* so it's extensible.
*/
typedef
   struct _Supp
   Supp;

/* Useful in SK_(error_matches_suppression)() */
SuppKind VG_(get_supp_kind)   ( Supp* su );
Char*    VG_(get_supp_string) ( Supp* su );
void*    VG_(get_supp_extra)  ( Supp* su );

/* Must be used in VG_(recognised_suppression)() */
void VG_(set_supp_kind)   ( Supp* su, SuppKind suppkind );
/* May be used in VG_(read_extra_suppression_info)() */
void VG_(set_supp_string) ( Supp* su, Char* string );
void VG_(set_supp_extra)  ( Supp* su, void* extra );


/* ------------------------------------------------------------------ */
/* Error records contain enough info to generate an error report.  The idea
   is that (typically) the same few points in the program generate thousands
   of errors, and we don't want to spew out a fresh error message for each
   one.  Instead, we use these structures to common up duplicates.
*/

typedef
   Int         /* Do not make this unsigned! */
   ErrorKind;

/* The tool-relevant parts of an Error are:
     kind:   what kind of error; must be in the range (0..)
     addr:   use is optional.  0 by default.
     string: use is optional.  NULL by default.
     extra:  use is optional.  NULL by default.  void* so it's extensible.
*/
typedef
   struct _Error
   Error;

/* Useful in SK_(error_matches_suppression)(), SK_(pp_SkinError)(), etc */
ExeContext* VG_(get_error_where)   ( Error* err );
SuppKind    VG_(get_error_kind)    ( Error* err );
Addr        VG_(get_error_address) ( Error* err );
Char*       VG_(get_error_string)  ( Error* err );
void*       VG_(get_error_extra)   ( Error* err );

/* Call this when an error occurs.  It will be recorded if it hasn't been
   seen before.  If it has, the existing error record will have its count
   incremented.

   'tid' can be found as for VG_(get_ExeContext)().  The `extra' field can
   be stack-allocated;  it will be copied by the core if needed (but it
   won't be copied if it's NULL).

   If no 'a', 's' or 'extra' of interest needs to be recorded, just use
   NULL for them.  */
extern void VG_(maybe_record_error) ( ThreadId tid, ErrorKind ekind,
                                      Addr a, Char* s, void* extra );

/* Similar to VG_(maybe_record_error)(), except this one doesn't record the
   error -- useful for errors that can only happen once.  The errors can be
   suppressed, though.  Return value is True if it was suppressed.
   `print_error' dictates whether to print the error, which is a bit of a
   hack that's useful sometimes if you just want to know if the error would
   be suppressed without possibly printing it.  `count_error' dictates
   whether to add the error in the error total count (another mild hack). */
extern Bool VG_(unique_error) ( ThreadId tid, ErrorKind ekind,
                                Addr a, Char* s, void* extra,
                                ExeContext* where, Bool print_error,
                                Bool allow_GDB_attach, Bool count_error );

/* Gets a non-blank, non-comment line of at most nBuf chars from fd.
   Skips leading spaces on the line.  Returns True if EOF was hit instead.
   Useful for reading in extra tool-specific suppression lines.  */
extern Bool VG_(get_line) ( Int fd, Char* buf, Int nBuf );


/*====================================================================*/
/*=== Obtaining debug information                                  ===*/
/*====================================================================*/

/* Get the file/function/line number of the instruction at address
   'a'.  For these four, if debug info for the address is found, it
   copies the info into the buffer/UInt and returns True.  If not, it
   returns False and nothing is copied.  VG_(get_fnname) always
   demangles C++ function names.  VG_(get_fnname_w_offset) is the
   same, except it appends "+N" to symbol names to indicate offsets.  */
extern Bool VG_(get_filename) ( Addr a, Char* filename, Int n_filename );
extern Bool VG_(get_fnname)   ( Addr a, Char* fnname,   Int n_fnname   );
extern Bool VG_(get_linenum)  ( Addr a, UInt* linenum );
extern Bool VG_(get_fnname_w_offset)
                              ( Addr a, Char* fnname,   Int n_fnname   );

/* This one is more efficient if getting both filename and line number,
   because the two lookups are done together. */
extern Bool VG_(get_filename_linenum)
                              ( Addr a, Char* filename, Int n_filename,
                                        UInt* linenum );

/* Succeeds only if we find from debug info that 'a' is the address of the
   first instruction in a function -- as opposed to VG_(get_fnname) which
   succeeds if we find from debug info that 'a' is the address of any
   instruction in a function.  Use this to instrument the start of
   a particular function.  Nb: if an executable/shared object is stripped
   of its symbols, this function will not be able to recognise function
   entry points within it. */
extern Bool VG_(get_fnname_if_entry) ( Addr a, Char* fnname, Int n_fnname );

/* Succeeds if the address is within a shared object or the main executable.
   It doesn't matter if debug info is present or not. */
extern Bool VG_(get_objname)  ( Addr a, Char* objname,  Int n_objname  );

/* Puts into 'buf' info about the code address %eip:  the address, function
   name (if known) and filename/line number (if known), like this:

      0x4001BF05: realloc (vg_replace_malloc.c:339)

   'n_buf' gives length of 'buf'.  Returns 'buf'.
*/
extern Char* VG_(describe_eip)(Addr eip, Char* buf, Int n_buf);

/* Returns a string containing an expression for the given
   address. String is malloced with VG_(malloc)() */
Char *VG_(describe_addr)(ThreadId, Addr);

/* A way to get information about what segments are mapped */
typedef struct _SegInfo SegInfo;

/* Returns NULL if the SegInfo isn't found.  It doesn't matter if debug info
   is present or not. */
extern SegInfo* VG_(get_obj)  ( Addr a );

extern const SegInfo* VG_(next_seginfo)  ( const SegInfo *seg );
extern       Addr     VG_(seg_start)     ( const SegInfo *seg );
extern       SizeT    VG_(seg_size)      ( const SegInfo *seg );
extern const UChar*   VG_(seg_filename)  ( const SegInfo *seg );
extern       ULong    VG_(seg_sym_offset)( const SegInfo *seg );

typedef
   enum {
      Vg_SectUnknown,
      Vg_SectText,
      Vg_SectData,
      Vg_SectBSS,
      Vg_SectGOT,
      Vg_SectPLT
   }
   VgSectKind;

extern VgSectKind VG_(seg_sect_kind)(Addr);


/*====================================================================*/
/*=== Generic hash table                                           ===*/
/*====================================================================*/

/* Generic type for a separately-chained hash table.  Via a kind of dodgy
   C-as-C++ style inheritance, tools can extend the VgHashNode type, so long
   as the first two fields match the sizes of these two fields.  Requires
   a bit of casting by the tool. */
typedef
   struct _VgHashNode {
      struct _VgHashNode * next;
      UWord              key;
   }
   VgHashNode;

typedef
   VgHashNode**
   VgHashTable;

/* Make a new table. */
extern VgHashTable VG_(HT_construct) ( void );

/* Count the number of nodes in a table. */
extern Int VG_(HT_count_nodes) ( VgHashTable table );

/* Add a node to the table. */
extern void VG_(HT_add_node) ( VgHashTable t, VgHashNode* node );

/* Looks up a node in the hash table.  Also returns the address of the
   previous node's `next' pointer which allows it to be removed from the
   list later without having to look it up again.  */
extern VgHashNode* VG_(HT_get_node) ( VgHashTable t, UWord key,
                                    /*OUT*/VgHashNode*** next_ptr );

/* Allocates an array of pointers to all the shadow chunks of malloc'd
   blocks.  Must be freed with VG_(free)(). */
extern VgHashNode** VG_(HT_to_array) ( VgHashTable t, /*OUT*/ UInt* n_shadows );

/* Returns first node that matches predicate `p', or NULL if none do.
   Extra arguments can be implicitly passed to `p' using `d' which is an
   opaque pointer passed to `p' each time it is called. */
extern VgHashNode* VG_(HT_first_match) ( VgHashTable t,
                                         Bool (*p)(VgHashNode*, void*),
                                         void* d );

/* Applies a function f() once to each node.  Again, `d' can be used
   to pass extra information to the function. */
extern void VG_(HT_apply_to_all_nodes)( VgHashTable t,
                                        void (*f)(VgHashNode*, void*),
                                        void* d );

/* Destroy a table. */
extern void VG_(HT_destruct) ( VgHashTable t );


/*====================================================================*/
/*=== A generic skiplist                                           ===*/
/*====================================================================*/

/* 
   The idea here is that the skiplist puts its per-element data at the
   end of the structure.  When you initialize the skiplist, you tell
   it what structure your list elements are going to be.  Then you
   should allocate them with VG_(SkipNode_Alloc), which will allocate
   enough memory for the extra bits.
 */
#include <stddef.h>		/* for offsetof */

typedef struct _SkipList SkipList;
typedef struct _SkipNode SkipNode;

typedef Int (*SkipCmp_t)(const void *key1, const void *key2);

struct _SkipList {
   const Short		arena;		/* allocation arena                        */
   const UShort		size;		/* structure size (not including SkipNode) */
   const UShort		keyoff;		/* key offset                              */
   const SkipCmp_t	cmp;		/* compare two keys                        */
	 Char *		(*strkey)(void *); /* stringify a key (for debugging)      */
         SkipNode	*head;		/* list head                               */
};

/* Use this macro to initialize your skiplist head.  The arguments are pretty self explanitory:
   _type is the type of your element structure
   _key is the field within that type which you want to use as the key
   _cmp is the comparison function for keys - it gets two typeof(_key) pointers as args
   _strkey is a function which can return a string of your key - it's only used for debugging
   _arena is the arena to use for allocation - -1 is the default
 */
#define SKIPLIST_INIT(_type, _key, _cmp, _strkey, _arena)		\
	{								\
	   .arena       = _arena,					\
	   .size	= sizeof(_type),				\
	   .keyoff	= offsetof(_type, _key),			\
	   .cmp		= _cmp,						\
	   .strkey      = _strkey,					\
	   .head	= NULL,						\
	}

/* List operations:
   SkipList_Find_* search a list.  The 3 variants are:
      Before: returns a node which is <= key, or NULL if none
      Exact:  returns a node which is == key, or NULL if none
      After:  returns a node which is >= key, or NULL if none
   SkipList_Insert inserts a new element into the list.  Duplicates are
      forbidden.  The element must have been created with SkipList_Alloc!
   SkipList_Remove removes an element from the list and returns it.  It
      doesn't free the memory.
*/
extern void *VG_(SkipList_Find_Before)  (const SkipList *l, void *key);
extern void *VG_(SkipList_Find_Exact)   (const SkipList *l, void *key);
extern void *VG_(SkipList_Find_After)   (const SkipList *l, void *key);
extern void  VG_(SkipList_Insert)       (      SkipList *l, void *data);
extern void *VG_(SkipList_Remove)       (      SkipList *l, void *key);

/* Some useful standard comparisons */
extern Int  VG_(cmp_Addr)  (const void *a, const void *b);
extern Int  VG_(cmp_Int)   (const void *a, const void *b);
extern Int  VG_(cmp_UInt)  (const void *a, const void *b);
extern Int  VG_(cmp_string)(const void *a, const void *b);

/* Node (element) operations:
   SkipNode_Alloc: allocate memory for a new element on the list.  Must be
      used before an element can be inserted!  Returns NULL if not enough
      memory.
   SkipNode_Free: free memory allocated above
   SkipNode_First: return the first element on the list
   SkipNode_Next: return the next element after "data" on the list - 
      NULL for none

   You can iterate through a SkipList like this:

      for(x = VG_(SkipNode_First)(&list);	// or SkipList_Find
	  x != NULL;
	  x = VG_(SkipNode_Next)(&list, x)) { ... }
*/
extern void *VG_(SkipNode_Alloc) (const SkipList *l);
extern void  VG_(SkipNode_Free)  (const SkipList *l, void *p);
extern void *VG_(SkipNode_First) (const SkipList *l);
extern void *VG_(SkipNode_Next)  (const SkipList *l, void *data);

/*====================================================================*/
/*=== Functions for shadow registers                               ===*/
/*====================================================================*/

/* Nb: make sure the shadow_regs 'need' is set before using these! */

/* This one lets you override the shadow of the return value register for a
   syscall.  Call it from SK_(post_syscall)() (not SK_(pre_syscall)()!) to
   override the default shadow register value. */
extern void VG_(set_return_from_syscall_shadow) ( ThreadId tid,
                                                  UInt ret_shadow );

/* This can be called from SK_(fini)() to find the shadow of the argument
   to exit(), ie. the shadow of the program's return value. */
extern UInt VG_(get_exit_status_shadow) ( ThreadId tid );


/*====================================================================*/
/*=== Specific stuff for replacing malloc() and friends            ===*/
/*====================================================================*/

/* If a tool replaces malloc() et al, the easiest way to do so is to
   link with vg_replace_malloc.o into its vgpreload_*.so file, and
   follow the following instructions.  You can do it from scratch,
   though, if you enjoy that sort of thing. */

/* Arena size for valgrind's own malloc();  default value is 0, but can
   be overridden by tool -- but must be done so *statically*, eg:

     UInt VG_(vg_malloc_redzone_szB) = 4;

   It can't be done from a function like SK_(pre_clo_init)().  So it can't,
   for example, be controlled with a command line option, unfortunately. */
extern UInt VG_(vg_malloc_redzone_szB);

/* Can be called from SK_(malloc) et al to do the actual alloc/freeing. */
extern void* VG_(cli_malloc) ( SizeT align, SizeT nbytes );
extern void  VG_(cli_free)   ( void* p );

/* Check if an address is within a range, allowing for redzones at edges */
extern Bool VG_(addr_is_in_block)( Addr a, Addr start, SizeT size );

/* ------------------------------------------------------------------ */
/* Some options that can be used by a tool if malloc() et al are replaced.
   The tool should call the functions in the appropriate places to give
   control over these aspects of Valgrind's version of malloc(). */

/* Round malloc sizes upwards to integral number of words? default: NO */
extern Bool VG_(clo_sloppy_malloc);
/* DEBUG: print malloc details?  default: NO */
extern Bool VG_(clo_trace_malloc);
/* Minimum alignment in functions that don't specify alignment explicitly.
   default: 0, i.e. use default of the machine (== 4) */
extern UInt VG_(clo_alignment);

extern Bool VG_(replacement_malloc_process_cmd_line_option) ( Char* arg );
extern void VG_(replacement_malloc_print_usage)             ( void );
extern void VG_(replacement_malloc_print_debug_usage)       ( void );


/*====================================================================*/
/*=== Tool-specific stuff                                          ===*/
/*====================================================================*/

/* ------------------------------------------------------------------ */
/* Details */

/* Default value for avg_translations_sizeB (in bytes), indicating typical
   code expansion of about 6:1. */
#define VG_DEFAULT_TRANS_SIZEB   100

/* Information used in the startup message.  `name' also determines the
   string used for identifying suppressions in a suppression file as
   belonging to this tool.  `version' can be NULL, in which case (not
   surprisingly) no version info is printed; this mechanism is designed for
   tools distributed with Valgrind that share a version number with
   Valgrind.  Other tools not distributed as part of Valgrind should
   probably have their own version number.  */
extern void VG_(details_name)                  ( Char* name );
extern void VG_(details_version)               ( Char* version );
extern void VG_(details_description)           ( Char* description );
extern void VG_(details_copyright_author)      ( Char* copyright_author );

/* Average size of a translation, in bytes, so that the translation
   storage machinery can allocate memory appropriately.  Not critical,
   setting is optional. */
extern void VG_(details_avg_translation_sizeB) ( UInt size );

/* String printed if an `sk_assert' assertion fails or VG_(skin_panic)
   is called.  Should probably be an email address. */
extern void VG_(details_bug_reports_to)   ( Char* bug_reports_to );

/* ------------------------------------------------------------------ */
/* Needs */

/* Booleans that decide core behaviour, but don't require extra
   operations to be defined if `True' */

/* Should __libc_freeres() be run?  Bugs in it can crash the tool. */
extern void VG_(needs_libc_freeres) ( void );

/* Want to have errors detected by Valgrind's core reported?  Includes:
   - pthread API errors (many;  eg. unlocking a non-locked mutex)
   - invalid file descriptors to blocking syscalls read() and write()
   - bad signal numbers passed to sigaction()
   - attempt to install signal handler for SIGKILL or SIGSTOP */
extern void VG_(needs_core_errors) ( void );

/* Booleans that indicate extra operations are defined;  if these are True,
   the corresponding template functions (given below) must be defined.  A
   lot like being a member of a type class. */

/* Want to report errors from tool?  This implies use of suppressions, too. */
extern void VG_(needs_skin_errors) ( void );

/* Is information kept about specific individual basic blocks?  (Eg. for
   cachegrind there are cost-centres for every instruction, stored at a
   basic block level.)  If so, it sometimes has to be discarded, because
   .so mmap/munmap-ping or self-modifying code (informed by the
   DISCARD_TRANSLATIONS user request) can cause one instruction address
   to be used for more than one instruction in one program run...  */
extern void VG_(needs_basic_block_discards) ( void );

/* Tool maintains information about each register? */
extern void VG_(needs_shadow_regs) ( void );

/* Tool defines its own command line options? */
extern void VG_(needs_command_line_options) ( void );

/* Tool defines its own client requests? */
extern void VG_(needs_client_requests) ( void );

/* Tool defines its own UInstrs? */
extern void VG_(needs_extended_UCode) ( void );

/* Tool does stuff before and/or after system calls? */
extern void VG_(needs_syscall_wrapper) ( void );

/* Are tool-state sanity checks performed? */
extern void VG_(needs_sanity_checks) ( void );

/* Do we need to see data symbols? */
extern void VG_(needs_data_syms) ( void );

/* Does the tool need shadow memory allocated (if you set this, you must also statically initialize 
   float SK_(shadow_ratio) = n./m;
   to define how many shadow bits you need per client address space bit.
*/
extern void VG_(needs_shadow_memory)( void );
extern float SK_(shadow_ratio);

/* ------------------------------------------------------------------ */
/* Core events to track */

/* Part of the core from which this call was made.  Useful for determining
   what kind of error message should be emitted. */
typedef
   enum { Vg_CorePThread, Vg_CoreSignal, Vg_CoreSysCall, Vg_CoreTranslate }
   CorePart;

/* Useful to use in VG_(get_Xreg_usage)() */
#define VG_UINSTR_READS_REG(ono,regs,isWrites)  \
   { if (mycat(u->tag,ono) == tag)              \
        { regs[n]     = mycat(u->val,ono);      \
          isWrites[n] = False;                  \
          n++;                                  \
        }                                       \
   }
#define VG_UINSTR_WRITES_REG(ono,regs,isWrites) \
   { if (mycat(u->tag,ono) == tag)              \
        { regs[n]     = mycat(u->val,ono);      \
          isWrites[n] = True;                   \
          n++;                                  \
        }                                       \
   }

#endif   /* NDEF __TOOL_H */

/* gen_toolint.pl will put the VG_(init_*)() functions here: */
/* Generated by "gen_toolint.pl toolproto" */

/* These are the parameterised functions in the core.  The default definitions
   are overridden by LD_PRELOADed tool version.  At the very least, a tool
   must define the fundamental template functions.  Depending on what needs
   are set, extra template functions will be used too.  Functions are
   grouped under the needs that govern their use.

   ------------------------------------------------------------------
   Fundamental template functions

   Do initialisation that can only be done after command line processing.
 */
void SK_(post_clo_init)(void);

/* Instrument a basic block.  Must be a true function, ie. the same input
   always results in the same output, because basic blocks can be
   retranslated.  Unless you're doing something really strange...
   'orig_addr' is the address of the first instruction in the block.
 */
UCodeBlock* SK_(instrument)(UCodeBlock* cb, Addr orig_addr);

/* Finish up, print out any results, etc.  `exitcode' is program's exit
   code.  The shadow (if the `shadow_regs' need is set) can be found with
   VG_(get_exit_status_shadow)().
 */
void SK_(fini)(Int exitcode);


/* ------------------------------------------------------------------
   VG_(needs).core_errors

   (none needed)

   ------------------------------------------------------------------
   VG_(needs).skin_errors

   Identify if two errors are equal, or equal enough.  `res' indicates how
   close is "close enough".  `res' should be passed on as necessary, eg. if
   the Error's `extra' part contains an ExeContext, `res' should be
   passed to VG_(eq_ExeContext)() if the ExeContexts are considered.  Other
   than that, probably don't worry about it unless you have lots of very
   similar errors occurring.
 */
Bool SK_(eq_SkinError)(VgRes res, Error* e1, Error* e2);

/* Print error context. */
void SK_(pp_SkinError)(Error* err);

/* Should fill in any details that could be postponed until after the
   decision whether to ignore the error (ie. details not affecting the
   result of SK_(eq_SkinError)()).  This saves time when errors are ignored.
   Yuk.

   Return value: must be the size of the `extra' part in bytes -- used by
   the core to make a copy.
 */
UInt SK_(update_extra)(Error* err);

/* Return value indicates recognition.  If recognised, must set skind using
   VG_(set_supp_kind)().
 */
Bool SK_(recognised_suppression)(Char* name, Supp* su);

/* Read any extra info for this suppression kind.  Most likely for filling
   in the `extra' and `string' parts (with VG_(set_supp_{extra, string})())
   of a suppression if necessary.  Should return False if a syntax error
   occurred, True otherwise.
 */
Bool SK_(read_extra_suppression_info)(Int fd, Char* buf, Int nBuf, Supp* su);

/* This should just check the kinds match and maybe some stuff in the
   `string' and `extra' field if appropriate (using VG_(get_supp_*)() to
   get the relevant suppression parts).
 */
Bool SK_(error_matches_suppression)(Error* err, Supp* su);

/* This should return the suppression name, for --gen-suppressions, or NULL
   if that error type cannot be suppressed.  This is the inverse of
   SK_(recognised_suppression)().
 */
Char* SK_(get_error_name)(Error* err);

/* This should print any extra info for the error, for --gen-suppressions,
   including the newline.  This is the inverse of
   SK_(read_extra_suppression_info)().
 */
void SK_(print_extra_suppression_info)(Error* err);


/* ------------------------------------------------------------------
   VG_(needs).basic_block_discards

   Should discard any information that pertains to specific basic blocks
   or instructions within the address range given.
 */
void SK_(discard_basic_block_info)(Addr a, SizeT size);


/* ------------------------------------------------------------------
   VG_(needs).shadow_regs

   No functions must be defined, but the post_reg[s]_write_* events should
   be tracked.

   ------------------------------------------------------------------
   VG_(needs).command_line_options

   Return True if option was recognised.  Presumably sets some state to
   record the option as well.
 */
Bool SK_(process_cmd_line_option)(Char* argv);

/* Print out command line usage for options for normal tool operation. */
void SK_(print_usage)(void);

/* Print out command line usage for options for debugging the tool. */
void SK_(print_debug_usage)(void);

/* ------------------------------------------------------------------
   VG_(needs).client_requests

   If using client requests, the number of the first request should be equal
   to VG_USERREQ_SKIN_BASE('X', 'Y'), where 'X' and 'Y' form a suitable two
   character identification for the string.  The second and subsequent
   requests should follow.

   This function should use the VG_IS_SKIN_USERREQ macro (in
   include/valgrind.h) to first check if it's a request for this tool.  Then
   should handle it if it's recognised (and return True), or return False if
   not recognised.  arg_block[0] holds the request number, any further args
   from the request are in arg_block[1..].  'ret' is for the return value...
   it should probably be filled, if only with 0.
 */
Bool SK_(handle_client_request)(ThreadId tid, UWord* arg_block, UWord* ret);


/* ------------------------------------------------------------------
   VG_(needs).extends_UCode

   'X' prefix indicates eXtended UCode.
 */
Int SK_(get_Xreg_usage)(UInstr* u, Tag tag, Int* regs, Bool* isWrites);
void SK_(emit_XUInstr)(UInstr* u, RRegSet regs_live_before);
Bool SK_(sane_XUInstr)(Bool beforeRA, Bool beforeLiveness, UInstr* u);
Char * SK_(name_XUOpcode)(Opcode opc);
void SK_(pp_XUInstr)(UInstr* u);


/* ------------------------------------------------------------------
   VG_(needs).syscall_wrapper

   If either of the pre_ functions malloc() something to return, the
   corresponding post_ function had better free() it!

 */
void * SK_(pre_syscall)(ThreadId tid, UInt syscallno, Bool is_blocking);
void SK_(post_syscall)(ThreadId tid, UInt syscallno, void* pre_result, Int res, Bool is_blocking);


/* ---------------------------------------------------------------------
     VG_(needs).sanity_checks

   Can be useful for ensuring a tool's correctness.  SK_(cheap_sanity_check)
   is called very frequently;  SK_(expensive_sanity_check) is called less
   frequently and can be more involved.
 */
Bool SK_(cheap_sanity_check)(void);
Bool SK_(expensive_sanity_check)(void);


/* ================================================================================
   Event tracking functions

   Events happening in core to track.  To be notified, pass a callback
   function to the appropriate function.  To ignore an event, don't do
   anything (default is for events to be ignored).

   Note that most events aren't passed a ThreadId.  To find out the ThreadId
   of the affected thread, use VG_(get_current_or_recent_tid)().  For the
   ones passed a ThreadId, use that instead, since
   VG_(get_current_or_recent_tid)() might not give the right ThreadId in
   that case.

   Memory events (Nb: to track heap allocation/freeing, a tool must replace
   malloc() et al.  See above how to do this.)

   These ones occur at startup, upon some signals, and upon some syscalls
 */
void SK_(new_mem_startup)(Addr a, SizeT len, Bool rr, Bool ww, Bool xx);
void SK_(new_mem_stack_signal)(Addr a, SizeT len);
void SK_(new_mem_brk)(Addr a, SizeT len);
void SK_(new_mem_mmap)(Addr a, SizeT len, Bool rr, Bool ww, Bool xx);

void SK_(copy_mem_remap)(Addr from, Addr to, SizeT len);
void SK_(change_mem_mprotect)(Addr a, SizeT len, Bool rr, Bool ww, Bool xx);
void SK_(die_mem_stack_signal)(Addr a, SizeT len);
void SK_(die_mem_brk)(Addr a, SizeT len);
void SK_(die_mem_munmap)(Addr a, SizeT len);

/* These ones are called when %esp changes.  A tool could track these itself
   (except for ban_mem_stack) but it's much easier to use the core's help.

   The specialised ones are called in preference to the general one, if they
   are defined.  These functions are called a lot if they are used, so
   specialising can optimise things significantly.  If any of the
   specialised cases are defined, the general case must be defined too.

   Nb: they must all use the REGPARM(n) attribute.
 */
void SK_(new_mem_stack_4)(Addr new_ESP);
void SK_(new_mem_stack_8)(Addr new_ESP);
void SK_(new_mem_stack_12)(Addr new_ESP);
void SK_(new_mem_stack_16)(Addr new_ESP);
void SK_(new_mem_stack_32)(Addr new_ESP);
void SK_(new_mem_stack)(Addr a, SizeT len);

void SK_(die_mem_stack_4)(Addr die_ESP);
void SK_(die_mem_stack_8)(Addr die_ESP);
void SK_(die_mem_stack_12)(Addr die_ESP);
void SK_(die_mem_stack_16)(Addr die_ESP);
void SK_(die_mem_stack_32)(Addr die_ESP);
void SK_(die_mem_stack)(Addr a, SizeT len);

/* Used for redzone at end of thread stacks */
void SK_(ban_mem_stack)(Addr a, SizeT len);

/* These ones occur around syscalls, signal handling, etc */
void SK_(pre_mem_read)(CorePart part, ThreadId tid, Char* s, Addr a, SizeT size);
void SK_(pre_mem_read_asciiz)(CorePart part, ThreadId tid, Char* s, Addr a);
void SK_(pre_mem_write)(CorePart part, ThreadId tid, Char* s, Addr a, SizeT size);
/* Not implemented yet -- have to add in lots of places, which is a
   pain.  Won't bother unless/until there's a need.
   void (*post_mem_read)  ( ThreadState* tst, Char* s, Addr a, SizeT size );
 */
void SK_(post_mem_write)(Addr a, SizeT size);


/* Register events -- if `shadow_regs' need is set, all should probably be
   used.  Use VG_(set_thread_shadow_archreg)() to set the shadow of the
   changed register.

 */
void SK_(pre_reg_read)(CorePart part, ThreadId tid, Char* s, UInt reg, SizeT size);

/* Use VG_(set_shadow_archreg)() to set the eight general purpose regs,
   and use VG_(set_shadow_eflags)() to set eflags.
 */
void SK_(post_regs_write_init)(ThreadId tid);

/* Use VG_(set_thread_shadow_archreg)() to set the shadow regs for these
   events.
 */
void SK_(post_reg_write_syscall_return)(ThreadId tid, UInt reg);
void SK_(post_reg_write_deliver_signal)(ThreadId tid, UInt reg);
void SK_(post_reg_write_pthread_return)(ThreadId tid, UInt reg);
void SK_(post_reg_write_clientreq_return)(ThreadId tid, UInt reg);
/* This one is called for malloc() et al if they are replaced by a tool. */
void SK_(post_reg_write_clientcall_return)(ThreadId tid, UInt reg, Addr f);


/* Scheduler events (not exhaustive) */
void SK_(thread_run)(ThreadId tid);


/* Thread events (not exhaustive)

   Called during thread create, before the new thread has run any
   instructions (or touched any memory).
 */
void SK_(post_thread_create)(ThreadId tid, ThreadId child);
void SK_(post_thread_join)(ThreadId joiner, ThreadId joinee);


/* Mutex events (not exhaustive)
   "void *mutex" is really a pthread_mutex *

   Called before a thread can block while waiting for a mutex (called
   regardless of whether the thread will block or not).
 */
void SK_(pre_mutex_lock)(ThreadId tid, void* mutex);
/* Called once the thread actually holds the mutex (always paired with
   pre_mutex_lock).
 */
void SK_(post_mutex_lock)(ThreadId tid, void* mutex);
/* Called after a thread has released a mutex (no need for a corresponding
   pre_mutex_unlock, because unlocking can't block).
 */
void SK_(post_mutex_unlock)(ThreadId tid, void* mutex);

/* Signal events (not exhaustive)

   ... pre_send_signal, post_send_signal ...

   Called before a signal is delivered;  `alt_stack' indicates if it is
   delivered on an alternative stack. 
 */
void SK_(pre_deliver_signal)(ThreadId tid, Int sigNo, Bool alt_stack);
/* Called after a signal is delivered.  Nb: unfortunately, if the signal
   handler longjmps, this won't be called.
 */
void SK_(post_deliver_signal)(ThreadId tid, Int sigNo);


/* Others... condition variable...
   ...

   Shadow memory management
 */
void SK_(init_shadow_page)(Addr p);

/* ================================================================================
   malloc and friends
 */
void* SK_(malloc)(SizeT n);
void* SK_(__builtin_new)(SizeT n);
void* SK_(__builtin_vec_new)(SizeT n);
void* SK_(memalign)(SizeT align, SizeT n);
void* SK_(calloc)(SizeT nmemb, SizeT n);
void SK_(free)(void* p);
void SK_(__builtin_delete)(void* p);
void SK_(__builtin_vec_delete)(void* p);
void* SK_(realloc)(void* p, SizeT size);
/* Generated by "gen_toolint.pl initproto" */

#ifndef VG_toolint_initproto
#define VG_toolint_initproto


/* These are the parameterised functions in the core.  The default definitions
   are overridden by LD_PRELOADed tool version.  At the very least, a tool
   must define the fundamental template functions.  Depending on what needs
   are set, extra template functions will be used too.  Functions are
   grouped under the needs that govern their use.

   ------------------------------------------------------------------
   Fundamental template functions

   Do initialisation that can only be done after command line processing.
 */
void VG_(init_post_clo_init)(void (*func)(void));

/* Instrument a basic block.  Must be a true function, ie. the same input
   always results in the same output, because basic blocks can be
   retranslated.  Unless you're doing something really strange...
   'orig_addr' is the address of the first instruction in the block.
 */
void VG_(init_instrument)(UCodeBlock* (*func)(UCodeBlock* cb, Addr orig_addr));

/* Finish up, print out any results, etc.  `exitcode' is program's exit
   code.  The shadow (if the `shadow_regs' need is set) can be found with
   VG_(get_exit_status_shadow)().
 */
void VG_(init_fini)(void (*func)(Int exitcode));


/* ------------------------------------------------------------------
   VG_(needs).core_errors

   (none needed)

   ------------------------------------------------------------------
   VG_(needs).skin_errors

   Identify if two errors are equal, or equal enough.  `res' indicates how
   close is "close enough".  `res' should be passed on as necessary, eg. if
   the Error's `extra' part contains an ExeContext, `res' should be
   passed to VG_(eq_ExeContext)() if the ExeContexts are considered.  Other
   than that, probably don't worry about it unless you have lots of very
   similar errors occurring.
 */
void VG_(init_eq_SkinError)(Bool (*func)(VgRes res, Error* e1, Error* e2));

/* Print error context. */
void VG_(init_pp_SkinError)(void (*func)(Error* err));

/* Should fill in any details that could be postponed until after the
   decision whether to ignore the error (ie. details not affecting the
   result of SK_(eq_SkinError)()).  This saves time when errors are ignored.
   Yuk.

   Return value: must be the size of the `extra' part in bytes -- used by
   the core to make a copy.
 */
void VG_(init_update_extra)(UInt (*func)(Error* err));

/* Return value indicates recognition.  If recognised, must set skind using
   VG_(set_supp_kind)().
 */
void VG_(init_recognised_suppression)(Bool (*func)(Char* name, Supp* su));

/* Read any extra info for this suppression kind.  Most likely for filling
   in the `extra' and `string' parts (with VG_(set_supp_{extra, string})())
   of a suppression if necessary.  Should return False if a syntax error
   occurred, True otherwise.
 */
void VG_(init_read_extra_suppression_info)(Bool (*func)(Int fd, Char* buf, Int nBuf, Supp* su));

/* This should just check the kinds match and maybe some stuff in the
   `string' and `extra' field if appropriate (using VG_(get_supp_*)() to
   get the relevant suppression parts).
 */
void VG_(init_error_matches_suppression)(Bool (*func)(Error* err, Supp* su));

/* This should return the suppression name, for --gen-suppressions, or NULL
   if that error type cannot be suppressed.  This is the inverse of
   SK_(recognised_suppression)().
 */
void VG_(init_get_error_name)(Char* (*func)(Error* err));

/* This should print any extra info for the error, for --gen-suppressions,
   including the newline.  This is the inverse of
   SK_(read_extra_suppression_info)().
 */
void VG_(init_print_extra_suppression_info)(void (*func)(Error* err));


/* ------------------------------------------------------------------
   VG_(needs).basic_block_discards

   Should discard any information that pertains to specific basic blocks
   or instructions within the address range given.
 */
void VG_(init_discard_basic_block_info)(void (*func)(Addr a, SizeT size));


/* ------------------------------------------------------------------
   VG_(needs).shadow_regs

   No functions must be defined, but the post_reg[s]_write_* events should
   be tracked.

   ------------------------------------------------------------------
   VG_(needs).command_line_options

   Return True if option was recognised.  Presumably sets some state to
   record the option as well.
 */
void VG_(init_process_cmd_line_option)(Bool (*func)(Char* argv));

/* Print out command line usage for options for normal tool operation. */
void VG_(init_print_usage)(void (*func)(void));

/* Print out command line usage for options for debugging the tool. */
void VG_(init_print_debug_usage)(void (*func)(void));

/* ------------------------------------------------------------------
   VG_(needs).client_requests

   If using client requests, the number of the first request should be equal
   to VG_USERREQ_SKIN_BASE('X', 'Y'), where 'X' and 'Y' form a suitable two
   character identification for the string.  The second and subsequent
   requests should follow.

   This function should use the VG_IS_SKIN_USERREQ macro (in
   include/valgrind.h) to first check if it's a request for this tool.  Then
   should handle it if it's recognised (and return True), or return False if
   not recognised.  arg_block[0] holds the request number, any further args
   from the request are in arg_block[1..].  'ret' is for the return value...
   it should probably be filled, if only with 0.
 */
void VG_(init_handle_client_request)(Bool (*func)(ThreadId tid, UWord* arg_block, UWord* ret));


/* ------------------------------------------------------------------
   VG_(needs).extends_UCode

   'X' prefix indicates eXtended UCode.
 */
void VG_(init_get_Xreg_usage)(Int (*func)(UInstr* u, Tag tag, Int* regs, Bool* isWrites));
void VG_(init_emit_XUInstr)(void (*func)(UInstr* u, RRegSet regs_live_before));
void VG_(init_sane_XUInstr)(Bool (*func)(Bool beforeRA, Bool beforeLiveness, UInstr* u));
void VG_(init_name_XUOpcode)(Char * (*func)(Opcode opc));
void VG_(init_pp_XUInstr)(void (*func)(UInstr* u));


/* ------------------------------------------------------------------
   VG_(needs).syscall_wrapper

   If either of the pre_ functions malloc() something to return, the
   corresponding post_ function had better free() it!

 */
void VG_(init_pre_syscall)(void * (*func)(ThreadId tid, UInt syscallno, Bool is_blocking));
void VG_(init_post_syscall)(void (*func)(ThreadId tid, UInt syscallno, void* pre_result, Int res, Bool is_blocking));


/* ---------------------------------------------------------------------
     VG_(needs).sanity_checks

   Can be useful for ensuring a tool's correctness.  SK_(cheap_sanity_check)
   is called very frequently;  SK_(expensive_sanity_check) is called less
   frequently and can be more involved.
 */
void VG_(init_cheap_sanity_check)(Bool (*func)(void));
void VG_(init_expensive_sanity_check)(Bool (*func)(void));


/* ================================================================================
   Event tracking functions

   Events happening in core to track.  To be notified, pass a callback
   function to the appropriate function.  To ignore an event, don't do
   anything (default is for events to be ignored).

   Note that most events aren't passed a ThreadId.  To find out the ThreadId
   of the affected thread, use VG_(get_current_or_recent_tid)().  For the
   ones passed a ThreadId, use that instead, since
   VG_(get_current_or_recent_tid)() might not give the right ThreadId in
   that case.

   Memory events (Nb: to track heap allocation/freeing, a tool must replace
   malloc() et al.  See above how to do this.)

   These ones occur at startup, upon some signals, and upon some syscalls
 */
void VG_(init_new_mem_startup)(void (*func)(Addr a, SizeT len, Bool rr, Bool ww, Bool xx));
void VG_(init_new_mem_stack_signal)(void (*func)(Addr a, SizeT len));
void VG_(init_new_mem_brk)(void (*func)(Addr a, SizeT len));
void VG_(init_new_mem_mmap)(void (*func)(Addr a, SizeT len, Bool rr, Bool ww, Bool xx));

void VG_(init_copy_mem_remap)(void (*func)(Addr from, Addr to, SizeT len));
void VG_(init_change_mem_mprotect)(void (*func)(Addr a, SizeT len, Bool rr, Bool ww, Bool xx));
void VG_(init_die_mem_stack_signal)(void (*func)(Addr a, SizeT len));
void VG_(init_die_mem_brk)(void (*func)(Addr a, SizeT len));
void VG_(init_die_mem_munmap)(void (*func)(Addr a, SizeT len));

/* These ones are called when %esp changes.  A tool could track these itself
   (except for ban_mem_stack) but it's much easier to use the core's help.

   The specialised ones are called in preference to the general one, if they
   are defined.  These functions are called a lot if they are used, so
   specialising can optimise things significantly.  If any of the
   specialised cases are defined, the general case must be defined too.

   Nb: they must all use the REGPARM(n) attribute.
 */
void VG_(init_new_mem_stack_4)(void (*func)(Addr new_ESP));
void VG_(init_new_mem_stack_8)(void (*func)(Addr new_ESP));
void VG_(init_new_mem_stack_12)(void (*func)(Addr new_ESP));
void VG_(init_new_mem_stack_16)(void (*func)(Addr new_ESP));
void VG_(init_new_mem_stack_32)(void (*func)(Addr new_ESP));
void VG_(init_new_mem_stack)(void (*func)(Addr a, SizeT len));

void VG_(init_die_mem_stack_4)(void (*func)(Addr die_ESP));
void VG_(init_die_mem_stack_8)(void (*func)(Addr die_ESP));
void VG_(init_die_mem_stack_12)(void (*func)(Addr die_ESP));
void VG_(init_die_mem_stack_16)(void (*func)(Addr die_ESP));
void VG_(init_die_mem_stack_32)(void (*func)(Addr die_ESP));
void VG_(init_die_mem_stack)(void (*func)(Addr a, SizeT len));

/* Used for redzone at end of thread stacks */
void VG_(init_ban_mem_stack)(void (*func)(Addr a, SizeT len));

/* These ones occur around syscalls, signal handling, etc */
void VG_(init_pre_mem_read)(void (*func)(CorePart part, ThreadId tid, Char* s, Addr a, SizeT size));
void VG_(init_pre_mem_read_asciiz)(void (*func)(CorePart part, ThreadId tid, Char* s, Addr a));
void VG_(init_pre_mem_write)(void (*func)(CorePart part, ThreadId tid, Char* s, Addr a, SizeT size));
/* Not implemented yet -- have to add in lots of places, which is a
   pain.  Won't bother unless/until there's a need.
   void (*post_mem_read)  ( ThreadState* tst, Char* s, Addr a, SizeT size );
 */
void VG_(init_post_mem_write)(void (*func)(Addr a, SizeT size));


/* Register events -- if `shadow_regs' need is set, all should probably be
   used.  Use VG_(set_thread_shadow_archreg)() to set the shadow of the
   changed register.

 */
void VG_(init_pre_reg_read)(void (*func)(CorePart part, ThreadId tid, Char* s, UInt reg, SizeT size));

/* Use VG_(set_shadow_archreg)() to set the eight general purpose regs,
   and use VG_(set_shadow_eflags)() to set eflags.
 */
void VG_(init_post_regs_write_init)(void (*func)(ThreadId tid));

/* Use VG_(set_thread_shadow_archreg)() to set the shadow regs for these
   events.
 */
void VG_(init_post_reg_write_syscall_return)(void (*func)(ThreadId tid, UInt reg));
void VG_(init_post_reg_write_deliver_signal)(void (*func)(ThreadId tid, UInt reg));
void VG_(init_post_reg_write_pthread_return)(void (*func)(ThreadId tid, UInt reg));
void VG_(init_post_reg_write_clientreq_return)(void (*func)(ThreadId tid, UInt reg));
/* This one is called for malloc() et al if they are replaced by a tool. */
void VG_(init_post_reg_write_clientcall_return)(void (*func)(ThreadId tid, UInt reg, Addr f));


/* Scheduler events (not exhaustive) */
void VG_(init_thread_run)(void (*func)(ThreadId tid));


/* Thread events (not exhaustive)

   Called during thread create, before the new thread has run any
   instructions (or touched any memory).
 */
void VG_(init_post_thread_create)(void (*func)(ThreadId tid, ThreadId child));
void VG_(init_post_thread_join)(void (*func)(ThreadId joiner, ThreadId joinee));


/* Mutex events (not exhaustive)
   "void *mutex" is really a pthread_mutex *

   Called before a thread can block while waiting for a mutex (called
   regardless of whether the thread will block or not).
 */
void VG_(init_pre_mutex_lock)(void (*func)(ThreadId tid, void* mutex));
/* Called once the thread actually holds the mutex (always paired with
   pre_mutex_lock).
 */
void VG_(init_post_mutex_lock)(void (*func)(ThreadId tid, void* mutex));
/* Called after a thread has released a mutex (no need for a corresponding
   pre_mutex_unlock, because unlocking can't block).
 */
void VG_(init_post_mutex_unlock)(void (*func)(ThreadId tid, void* mutex));

/* Signal events (not exhaustive)

   ... pre_send_signal, post_send_signal ...

   Called before a signal is delivered;  `alt_stack' indicates if it is
   delivered on an alternative stack. 
 */
void VG_(init_pre_deliver_signal)(void (*func)(ThreadId tid, Int sigNo, Bool alt_stack));
/* Called after a signal is delivered.  Nb: unfortunately, if the signal
   handler longjmps, this won't be called.
 */
void VG_(init_post_deliver_signal)(void (*func)(ThreadId tid, Int sigNo));


/* Others... condition variable...
   ...

   Shadow memory management
 */
void VG_(init_init_shadow_page)(void (*func)(Addr p));

/* ================================================================================
   malloc and friends
 */
void VG_(init_malloc)(void* (*func)(SizeT n));
void VG_(init___builtin_new)(void* (*func)(SizeT n));
void VG_(init___builtin_vec_new)(void* (*func)(SizeT n));
void VG_(init_memalign)(void* (*func)(SizeT align, SizeT n));
void VG_(init_calloc)(void* (*func)(SizeT nmemb, SizeT n));
void VG_(init_free)(void (*func)(void* p));
void VG_(init___builtin_delete)(void (*func)(void* p));
void VG_(init___builtin_vec_delete)(void (*func)(void* p));
void VG_(init_realloc)(void* (*func)(void* p, SizeT size));

#endif /* VG_toolint_initproto */
