#write own likelihood function
binomial.likelihood<-function(p){
  choose(10,7)*p^7*(1-p)^3 #return binomial coefficients and the logs of their absolute values
}
#check the likelihood value for p=0.1
binomial.likelihood(p=0.1)

#plot likelihood function against p
p<-seq(0,1,0.01)
likelihood.values<-binomial.likelihood(p)
plot(p, likelihood.values, type='l')

#log likelihood
log.binomial.likelihood<-function(p){
  log(binomial.likelihood(p=p))
}

#plot log-likelihood
p<-seq(0,1,0.01)
log.likelihood.values<-log.binomial.likelihood(p)
plot(p, log.likelihood.values, type='l')

############################################################
optimize(binomial.likelihood, interval=c(0,1), maximum = TRUE)
