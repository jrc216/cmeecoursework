#include <stdio.h>

int main (void)
{
	
	int x = 0;
	char a = 'a';
	
	x = x + 1;
	a = a + 1;
	
	printf("x is: %i; a is: %c\n", x, a);
	
	// ++ --
	
	// ++x -> return value of x; then incrememnt x (reads x and then incrememnt (addition) x (translates to x + 1))
	
	//++x:
	// x = x + 1;
	//return x;
	
	//x++
	//return x
	//x = x + 1
	
	//printf("x is: %i; a is: %c\n", ++x, ++a); //incrememnted, then passed to printf
	printf("x is: %i; a is: %c\n", x++, ++a); // passed to printf then evaluated, be careful if put ++ before or after variable name
	
	printf("x is: %i; a is: %c\n", x, ++a);
	return 0;	
	
}
