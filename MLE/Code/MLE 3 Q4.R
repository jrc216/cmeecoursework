flowering<-read.table("../Data/flowering.txt", header=T)
names(flowering)
par(mfrow=c(1,2))
plot(flowering$Flowers, flowering$State)
plot(flowering$Root, flowering$State)

#two arguments: parm is a vector of parameters, dat is the input dataset
logistic.log.likelihood<-function(parm, dat){
  #define parameters
  a<-parm[1]
  b<-parm[2]
  c<-parm[3]
  
  #define response variable, which is the first column of dat
  State<-dat[,1]
  
  # similarly define our explanatory variables
  Flowers<-dat[,2]
  Root<-dat[,3]
  
  #model our success probability
  p<-exp(a+b*Flowers+c*Root)/(1+exp(a+b*Flowers+c*Root))
  
  #the log likelihood function
  log.like<-sum(State*log(p)+(1-State)*log(1-p))
  
  return(log.like)
}

#TRY
logistic.log.likelihood(c(0,0,0), dat=flowering)

#MAXIMISE THE LOG LIKELIHOOD
M1<-optim(par=c(0,0,0), logistic.log.likelihood, method='L-BFGS-B', lower=c(-Inf, -Inf, -Inf), upper=c(Inf, Inf, Inf), control=list(fnscale=-1), dat=flowering, hessian = T)
M1
#value= - 27.0
#####include flowers and root interaction term
logistic.log.likelihood.int<-function(parm, dat)
{
  #define parameters, one more this time
  a<-parm[1]
  b<-parm[2]
  c<-parm[3]
  d<-parm[4]
  #define response variable, which is the first column of dat
  State<-dat[,1]
  
  # similarly define our explanatory variables
  Flowers<-dat[,2]
  Root<-dat[,3]
  
  #model to our success probability
  p<-exp(a+b*Flowers+c*Root+d*Flowers*Root)/(1+exp(a+b*Flowers+c*Root+d*Flowers*Root)) #logistic regression
  
  #the log likelihood function
  log.like<-sum(State*log(p)+(1-State)*log(1-p))
  
  return(log.like)
}

logistic.log.likelihood.int(c(0,0,0,0), dat=flowering)

#MAXIMISE THE LOG LIKELIHOOD
M2<-optim(par=c(0,0,0,0), logistic.log.likelihood.int, method='L-BFGS-B', lower=c(-Inf, -Inf, -Inf -Inf), upper=c(Inf, Inf, Inf, Inf), control=list(fnscale=-1), dat=flowering, hessian = T)
M2
#value=-18.56

D<-2*(-18.56+27)
D
#=16.88

qchisq(0.95, df=1)
