#!/bin/bash
# Author: Joanna Clarke JRC216@imperial.ac.uk
# Script: run_LV.sh
# Desc: running LV1.py and LV2.py from bash
# Arguments: none
# Date: Nov 2015
#script to run the lotka volterra models scripts
python LV1.py
python LV2.py 1 0.1 1.5 0.75 30
python LV3.py 1 0.5 1.5 0.75 30

python -m cProfile LV1.py

python -m cProfile LV2.py 1 0.1 1.5 0.75 30

python -m cProfile LV3.py 1 0.5 1.5 0.75 30

python -m cProfile LV4.py

python -m cProfile LV5.py









