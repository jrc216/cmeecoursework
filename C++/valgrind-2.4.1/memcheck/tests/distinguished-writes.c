/*
  Test that a write will convert a secondary from distinguished to normal
  
  Looks like it needs to be compiled with -O to make sure the FP
  writes get compiled with the right instructions.
 */

#define SECSZ	65536		/* secondary map size */
#define ASZ	(SECSZ*4)

typedef long long llong;
typedef long double ldouble;

#define A(t)	static t t##s[ASZ / sizeof(t)]

A(char);
A(short);
A(int);
A(long);
A(llong);
A(float);
A(double);
A(ldouble);

#define NELEM(a)	(sizeof(a)/sizeof(*a))

#define T(t)	for(i = 0; i < NELEM(t##s); i++) (t##s)[i] = (t)1

int main()
{
	int i;

	T(char);
	T(short);
	T(int);
	T(long);
	T(llong);
	T(float);
	T(double);
	T(ldouble);

	return 0;
}
