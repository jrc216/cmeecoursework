"""Importing and exporting data"""
########################
# FILE INPUT
########################
# Open a file for reading 
f=open('../Sandbox/test.txt', 'r')
# use "impicit" for loop:
# if the object is a file, python will cycle over lines
for line in f:
	print line, #the "," prevents adding a new line
	
#close the file
f.close()

#same example, skip blank lines
f=open('../Sandbox/test.txt', 'r')
for line in f:                #impicit loop, loops through lines in file
	if len(line.strip()) > 0: #checks if line is empty and gets rid of any white space
		print line,
f.close()

############################
# FILE OUTPUT
############################

#Save the elements of a list to a file
list_to_save=range(100)

f=open('../Sandbox/testout.txt','w')
for i in list_to_save:
	f.write(str(i)+'\n') ##Add a new line at the end
	
f.close()

##################################
#Storing objects
##################################
#To save an object (even complex) for later use
my_dictionary = {"a key": 10, "another key":11} # pickle gets python to remember it

import pickle # pickle - dumps data in a compressed package that you can recall in python later

f=open('../Sandbox/testp.p','wb') ## note the b: accept binary files
pickle.dump(my_dictionary, f) # writes a pickled representation of the object to the open file testp.p
f.close()

##Load the data again
f=open('../Sandbox/testp.p','rb')
another_dictionary=pickle.load(f) # read apickled object representation from open file testp.p
f.close()

print another_dictionary








