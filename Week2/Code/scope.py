
"""creating a global variable"""

##Try this first

_a_global = 10 # value of variable outside function

def a_function(): #defining a function
	_a_global = 5 
	_a_local = 4 # value of variables inside function
	print "Inside the function,the value is", _a_global 
	print "Inside the function, the value is", _a_local # print values of variables inside function
	return None
	
a_function()
print "Outside the function, te value is", _a_global # outside the function
	
	
	
## Now try this

_a_global = 10

def a_function():
	global _a_global # made global a global variable
	_a_global = 5
	_a_local = 4
	print "Inside the function, the value is", _a_global
	print "Inside the function, the value is", _a_local
	return None
	
a_function()
print "Outside the function, the value is", _a_global	

#the global has been defined as '_a_global'
