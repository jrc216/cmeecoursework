
//if (/* condition */)
	//one statement;
	
//if (/* condition */)
//{
//} 
//else {
//} 


//false : 0
// true : non zero , -1, 100 etc..

//if (x + 1) (if x was -1 then would get skipped

#include <stdio.h>

int main (void)
{
	//must distinguish = (assignment) from == ('is equal to')
	
	int i = 0;
	
	if (i==1)  // is definitely equal to, return true or statement,
	{
		printf("i is 1");
	}
	else {
		printf("i is not equal 1");
	}
	if (i = 1) //if this equaled 0  this would be false so the statement wouldnt be printed //will return a warning when using "-Wall", reassign i to 1 could be replaced by almost anything like an expression or function
	{
		printf("Attempt to assign 1 to i\n");
	}
	

// a <=b; a=> b;

int a = 2;
int b = 3;

if (a <= b)
{
	printf("a is less than or equal to b \n");
}
return 0

	
}
	



// gcc -Wall control.c includes all warnings./a.out
// != is not equal
