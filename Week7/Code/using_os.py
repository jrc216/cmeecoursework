#!/usr/bin/python

"""Using os to use bash commands in a python sript """

# Use the subprocess.os module to get a list of files and  
# Hint: look in subprocess.os and/or subprocess.os.path and/or 
# subprocess.os.walk for helpful functions

import subprocess

#################################
#~Get a list of files and 
#~directories in your home/ that start with an uppercase 'C'

# Type your code here:

subprocess.os.system("find ~/ -name 'C*'")

# Get the user's home directory.
home = subprocess.os.path.expanduser("~")

# Create a list to store the results.
FilesDirsStartingWithC = []

# Use a for loop to walk through the home directory.
for (dir, subdir, files) in subprocess.os.walk(home):
	for i in subdir:
		if i.startswith ('C'):
			FilesDirsStartingWithC.append(i)
	for i in files:
		if i.startswith ('C'):
			FilesDirsStartingWithC.append(i)

print(len(FilesDirsStartingWithC)
	
	
#################################
# Get files and directories in your home/ that start with either an 
# upper or lower case 'C'

subprocess.os.system("find ~/ -iname 'c*'")

# Create a list to store the results.
FilesDirsStartingWithCorc = []

##looping
for (dir, subdir, files) in subprocess.os.walk(home):
	for i in subdir:
		if i.startswith ('C') or i.startswith('c'):
			FilesDirsStartingWithCorc.append(i)
	for i in files:
		if i.startswith ('C') or i.startswith('c'):
			FilesDirsStartingWithCorc.append(i)

print(len(FilesDirsStartingWithCorc)

#################################
# Get only directories in your home/ that start with either an upper or 
#~lower case 'C' 

# Type your code here:
subprocess.os.system("find ~/ -type d -iname 'c*'")

# Create a list to store the results.
DirsStartingWithCorc = []


##looping
for (dir, subdir,files) in subprocess.os.walk(home):
	for i in subdir:
		if i.startswith ('C') or i.startswith('c'):
			DirsStartingWithCorc.append(i)
	
print(len(DirsStartingWithCorc))		
			
			
			
			
			
			
			
			
			
			
			
