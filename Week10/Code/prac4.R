rm(list=ls())

dat<-read.table("../Data/BTdat.txt", header=T)
names(dat)
str(dat)
head(dat)
plot(dat$clutch~dat$laydate, pch=19, cex=0.7)
dat<-subset(dat, dat$clutc<40) # get rid of outlier
dat<-subset(dat, dat$laydate!="NA") # 
plot(jitter(dat$clutch)~dat$laydate, pch=19, cex=0.7)
boxplot(dat$laydate~dat$year)

dat<-subset(dat, dat$laydate!="NA")
dat<-subset(dat, dat$clutch!="NA")
dat<-subset(dat, dat$year!="NA")
dat<-subset(dat, dat$female!="NA")
library(plyr)
dat.st<-ddply(dat, c("year"), transform, ld.std = scale(laydate)) # splitting data frame by year, then standarising the laydate, so that when spring comes doesnt affect the data 
par(mfrow=c(2,1))
boxplot(dat.st$ld.std~dat.st$year, xlab="year", ylab="Annual z-scores of laying date")

plot(jitter(dat.st$clutch)~dat.st$ld.std, pch=19, cex=0.7, xlab="Annual z-scores of laying date", ylab="Clutch size (number eggs)")

dev.off()

par(mfrow =c(1,2))
plot((dat.st$laydate)~dat.st$ld.std, pch=19, cex=0.7, xlab="Annual z-scores of laying date", ylab="Laying date (April days)")

plot((dat.st$laydate)~scale(dat.st$laydate), pch=19, cex=0.7, xlab="z-scores of laying date", ylab="Laying date (April days)") # difference between annual standardising (separate ranking per year)

boxplot(dat.st$clutch~dat.st$year)

dat.st<-ddply(dat.st, c("year"), transform, clutch.std = scale(clutch)) #standardise by clutch size
head(dat.st)

boxplot(dat.st$clutch.std~dat.st$year, ylab="Annual z-scores clutch size",xlab="Year")

plot(jitter(dat.st$clutch.std)~dat.st$ld.std, pch=19, cex=0.7, xlab="Annual z-scores of laying date", ylab = "Annual z-scores clutch size")

#calculating te strength of selection

est<-rep(0,12) #12 0s in a vector
err<-rep(0,12)
selec.grad<-cbind(est,err)#combine two columns next to each other
selec.grad<-as.data.frame(selec.grad)#convert to a dataframe so can subset
head(selec.grad)

for (i in 2002:2013){
  m<-(lm(dat.st$clutch.std[dat.st$year==i]~dat.st$ld.std[dat.st$year==i])) #calculating selection gradient for each year
  j<-i-2001 #so goes up 1,2, etc...
  selec.grad$est[j]<-m$coefficients[2] # getting coefficient two from model
  selec.grad$err[j]<-summary(m)$cov.unscaled[2,2]
}
selec.grad # now a table of annual slopes and standard errors

selec.grad$Lower<-selec.grad$est-1.96*(selec.grad$err) # dont wants SEs want 95Cl so multiple by 1.96
selec.grad$Upper<-selec.grad$est+1.96*(selec.grad$err)

year<-c(2002:2013)
plot(selec.grad$est~year, pch=19, ylim=c(-0.6, -0.3))
par(mfrow=c(1,1))
segments(year, selec.grad$est-1.96*(selec.grad$err),year,selec.grad$est+1.96*(selec.grad$err))

#calculate repeatability as if there is none, there'll be no repeatabiltiy
require(MCMCglmm)
mod1<-MCMCglmm(ld.std~1, random=~female,data=dat.st)
summary(mod1) # within individual variance (not between like in notes!)

plot(mod1$VCV)
#repeatability=vi/vi+vr
repeatability<- mod1$VCV[,"female"]/(mod1$VCV[,"female"]+mod1$VCV[,"units"])
posterior.mode(repeatability)
HPDinterval(repeatability)
#have some repeatability

p<-read.table("../Data/BTped.txt", header=T)
library(MasterBayes)

po<-orderPed(p)

mod2<-MCMCglmm(ld.std~1, random=~female+animal,pedigree=po, data=dat.st,burnin=50000, nitt=100000)
#female is within individual, animal is between individuals and takes into their relatedness     


summary(mod2)
#bigger difference between individuals than within
plot(mod2$VCV)

save(mod2,file="mod2.Rdata")
#h2=Vag/vi+vr+vag
Heritability<-mod2$VCV[,"animal"]/(mod2$VCV[,"female"]+mod2$VCV[,"animal"]+mod2$VCV[,"units"])

posterior.mode(Heritability)
HPDinterval(Heritability)
dev.off()

require(pedantics)
drawPedigree(po,dots="y")
measuredPed<-po
measuredPed$Measured<-ifelse (measuredPed$animal %in% dat$animal =="TRUE",1,0)
head(measuredPed)
drawPedigree(measuredPed, dots="y", dat=measuredPed$Measured, retain="informative")

pedStatSummary(pedigreeStats(po, graphicalReport = "n")) #finding out how many individuals are related with each other










