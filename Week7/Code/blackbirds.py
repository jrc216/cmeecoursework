#!/usr/bin/python
"""extracting blackbird species information"""
import re

# Read the file
f = open('../Data/blackbirds.txt', 'r')
text = f.read()
f.close()

# remove \t\n and put a space in:
text = text.replace('\t',' ')
text = text.replace('\n',' ')

# note that there are "strange characters" (these are accents and
# non-ascii symbols) because we don't care for them, first transform
# to ASCII:
text = str(text.decode('ascii', 'ignore'))
# str gets rid of 'u' which indicates that the text is unicode
# Now write a regular expression my_reg that captures # the Kingdom, 
# Phylum and Species name for each species and prints it out neatly:

# my_reg = ??????

# Hint: you will probably want to use re.findall(my_reg, text)...
# Keep in mind that there are multiple ways to skin this cat! 
#may want to write as separate loops for kingdom then phylum then species


Kingdom = re.findall(r'Kingdom\s\w*\s',text) 
Kingdom
# find 'Kingdom' and the proceeding Kingdom name

Phylum = re.findall(r'Phylum\s\w*\s', text)
Phylum
# find 'Phylum' and the proceeding Phylum name

Species = re.findall(r'Species\s\w*\s\w*', text)
Species
# find 'Species' and the proceeding Species name

Tuple = zip(Kingdom, Phylum, Species) # put the three previous lists into tuples
Tuple


for i in xrange(len(Kingdom)): # for each element in kingdom
	num = 1 + i               # add one to each iteration as otherwise it starts from 0
	print "Blackbird species", num, "is:"
	print Kingdom[i]
	print Phylum[i]
	print Species[i], "\n" # add a space in between each set of kingdom, phylum and species







