Summary: Valgrind Memory Debugger
Name: valgrind
Version: 2.4.1
Release: 1
Epoch: 1
License: GPL
URL: http://www.valgrind.org/
Group: Development/Debuggers
Packager: Jeremy Fitzhardinge <jeremy@goop.org>
Source: valgrind-2.4.1.tar.bz2

Buildroot: %{_tmppath}/%{name}-root

%description 

Valgrind is a GPL'd system for debugging and profiling x86-Linux programs.
With the tools that come with Valgrind, you can automatically detect
many memory management and threading bugs, avoiding hours of frustrating
bug-hunting, making your programs more stable. You can also perform
detailed profiling to help speed up your programs.

The Valgrind distribution includes five tools: two memory error
detectors, a thread error detector, a cache profiler and a heap profiler.
Several other tools have been built with Valgrind.

%prep
%setup -n valgrind-2.4.1

%build
%configure
make

%install
make install DESTDIR=$RPM_BUILD_ROOT

%makeinstall
mkdir docs.installed
mv $RPM_BUILD_ROOT%{_datadir}/doc/valgrind/* docs.installed/

%files
%defattr(-,root,root)
%doc ACKNOWLEDGEMENTS AUTHORS COPYING FAQ.txt INSTALL NEWS README*
%doc docs.installed/*.html docs.installed/*.gif
%{_bindir}/*
%{_includedir}/valgrind
%{_libdir}/valgrind
%{_libdir}/pkgconfig/*

%doc
%defattr(-,root,root)
%{_mandir}/*/*

%clean
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf ${RPM_BUILD_ROOT}
