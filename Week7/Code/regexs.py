#!/usr/bin/python
""" Examples of regular expressions"""
import re
my_string = 'a given string'
#find a space in the string
match = re.search(r'\s', my_string) 
#r=tells python to read regex in its literal form, more convenient
#\s match a whitespace

print match
#this should print something like
#,_sre.SRE_Match object at 0x93ecdd30>

#now we can see what has matched
match.group()

#.group makes output faster

match = re.search(r's\w*', my_string)
#literal s and then \w* match any alpha numeric character, mtach the preceding pattern element zero or more times

#this should return  "string"
match.group()

#NOW AN EXAMPLE OF NO MATCH:
#find a digit in the string
match = re.search(r'\d', my_string) # \d match a numeric character
#this should print "None"
print match

#further example 
#
MyStr = 'an example'
match = re.search(r'\w*\s', MyStr)

if match:
	print 'found a match:', match.group()
else:
	print 'did not find a match'


# Some Basic Examples
match = re.search(r'\d' , "it takes 2 to tango")
print match.group() #print 2

match = re.search(r'\s\w*\s', 'once upon a time')
match.group() # ' upon ' * any number of letters, only gives first example

match = re.search(r'\s\w{1,3}\s', 'once upon a time')
match.group() # ' a '  # match preceeding element(alpha numeric character) between 1 and 3 times, upon is 4 so too long

match = re.search(r'\s\w*$', 'once upon a time')
match.group() # ' time' # $match the end of a line, * means any number of characters


match = re.search(r'\w*\s\d.*\d', 'take 2 grams of H2O')
match.group() # 'take 2 grams of H2' #. = math any character except line break(new line)

match = re.search(r'^\w*.*\s', 'once upon a time')
match.group() # 'once upon a '
## NOTE THAT *, +, and { } are all "greedy":
## They repeat the previous regex token as many times as possible
## As a result, they may match more text than you want
#^ = match the beginning of a line

## To make it non-greedy, use ?:
match = re.search(r'^\w*.*?\s', 'once upon a time')
match.group() # 'once ' ?=match the preceeding pattern element zero or one times


## To further illustrate greediness, let's try matching an HTML tag:
match = re.search(r'<.+>', 'This is a <EM>first</EM> test')
match.group() # '<EM>first</EM>'
## But we didn't want this: we wanted just <EM>
## It's because + is greedy!
##

## Instead, we can make + "lazy"!
match = re.search(r'<.+?>', 'This is a <EM>first</EM> test')
match.group() # '<EM>'


## OK, moving on from greed and laziness
match = re.search(r'\d*\.?\d*','1432.75+60.22i') #note "\" before "."
match.group() # '1432.75' \. search for a dot

match = re.search(r'\d*\.?\d*','1432+60.22i')
match.group() # '1432'

match = re.search(r'[AGTC]+', 'the sequence ATTCGT')
match.group() # 'ATTCGT'

re.search(r'\s+[A-Z]{1}\w+\s\w+', 'The bird-shit frog''s name is Theloderma asper').←-
group() # ' Theloderma asper'
## NOTE THAT I DIRECTLY RETURNED THE RESULT BY APPENDING .group()
# space, then capital, then alpha numeric characters then space then alpha numeric numeric characters, plus means






