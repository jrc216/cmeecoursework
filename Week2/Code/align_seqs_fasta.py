# These are the two sequences to match
#!/usr/bin/python
"""Takes two DNA sequences as an input from an external file and finds the best match between 
two DNA sequences then saves the best alignment along with its corresponding score in a single text file"""
import sys
# assign the longest sequence s1, and the shortest to s2
# l1 is the length of the longest, l2 that of the shortest
def align_seqs:
	import csv
	f=open(sys.argv[0],'rb') #open and read file
	g=open(sys.argv[1],'rb')

	csvread=csv.reader(f)  #csvread means reading f using the csv.reader module
	temp=[] # creates a temporary list
	for column in csvread: # for a column in the file
		temp.append(tuple(column)) # add a tuple to that column in the temporary file
	f.close()  #close f
	#read in a csv file
	seq1=column[0]

	csvread=csv.reader(g)  #csvread means reading f using the csv.reader module
	temp=[] # creates a temporary list
	for column in csvread: # for a column in the file
		temp.append(tuple(column)) # add a tuple to that column in the temporary file
	g.close()  #close g
	#read in a csv file
	seq2=column[0]  # sequene 2 is being assigned to column 2


	l1 = len(seq1)#l1 = length of seq1
	l2 = len(seq2)#l2 = length of seq2
	if l1 >= l2:#if l1 (length of seq1) is equal to or larger than l1(length of seq2
		s1 = seq1
		s2 = seq2
	else:
		s1 = seq2  # if l1 is shorter than l2 s1 = seq2 and s2 = seq1
		s2 = seq1
		l1, l2 = l2, l1 # swap the two lengths

	# function that computes a score
	# by returning the number of matches 
	# starting from arbitrary startpoint
	def calculate_score(s1, s2, l1, l2, startpoint):
		# startpoint is the point at which we want to start
		matched = "" # contains string for alignement
		score = 0 # score starts off as 0
		for i in range(l2):   #i can't be larger than the length of l2
			if (i + startpoint) < l1:   #if the iteration is shorter than the length of l1
				# if its matching the character 
				if s1[i + startpoint] == s2[i]: #if l1 matches l2  
					matched = matched + "*" # add  star above the match
					score = score + 1    #and add one to the score
				else:
					matched = matched + "-" # if they dont match add a - instead

		# build some formatted output
		print "." * startpoint + matched # print a "." the number of times equal to the value of matched          
		print "." * startpoint + s2 #print a "." the number of times equal to the length of seq2
		print s1 
		print score 
		print ""

		return score

	calculate_score(s1, s2, l1, l2, 0) # test the loop calculate score where the starpoint is 0
	calculate_score(s1, s2, l1, l2, 1)
	calculate_score(s1, s2, l1, l2, 5)

	# now try to find the best match (highest score)
	my_best_align = None
	my_best_score = -1

	for i in range(l1): #i in the range of the length of l1
		z = calculate_score(s1, s2, l1, l2, i) # z is the value of the score on te ith iteration
		if z > my_best_score:  # if this is higer than the best score
			my_best_align = "." * i + s2 #add a "." the number of times equal to te length of s2 + that number of the iteration
			my_best_score = z # this goes through each cycle to see which gives the next match

	print my_best_align
	print s1
	print "Best score:", my_best_score # prints te best score



	with open('../Results/align_seqs_output.txt', 'w') as f: # enables to write to a file
		print >> f, my_best_align # prints best score to the text file
		print >> f, s1   #two arrows so not over written, print s1
		print >> f, "Best score:" + str(my_best_score)  #print best score as a string as is a number

	with open('../Results/align_seqs_output.txt', 'w') as f: # enables to write to a file
		print >> f, my_best_align # prints best score to the text file
		print >> f, s1   #two arrows so not over written, print s1
		print >> f, "Best score:" + str(my_best_score)  #print best score as a string as is a number

	f.close # close file

def main(argv):
	align_seqs
	f= open('../Data/seq1.csv','rb')
	g =open('../Data/seq2.csv','rb')
	
	
if (__name__=="__main__"): #makes sure the "main" function is called from commandline
	status=main(sys.argv)
	sys.exit (status)
