Supplementary Text 1: MATLAB code. Code was written in MATLAB R2012b.
function [list]= NN_track(tracks)
 
%%Nearest neighbour tracker algorithm

% please note that execution of the majority of this code requires 
%the MATLAB Image Processing Toolbox
 
% The input variable ‘tracks’ must be a cell with length equal to the number of video frames to connect.
% Each cell element at tracks{x} is a y by 2 double, where;
% y is the number of objects in that frame.% tracks{x}(1:y,1) corresponds to x-coordinates, tracks{x} (1:y,2) corresponds to y-coordinates

% list is the output variable with all of the tracks in.
% list is an n x 2 cell.
% list {x,1} contains an m x 2 double with consecutive connected coordinates.
% m is the number of frames that object x was tracked for before the track was broken
% list {x,2} contains the 1 x 3 double that positions each track within the video
% with the structure [startframe endframe numberofframes (m)]
 
% preallocate list for memory considerations
% this assumes that there will never be more than 100000 tracks generated.

list=cell(100000,2);
 
%correspondeces are used to associate coordinates at time i and i+1 with
%tracks saved in the variable list.
% example: at time i, the coordinates in the row 3 were saved into the 7th track.
% The tracker then links these coordinates to those in row 6 at i+1,
% these coordinates also need to be saved as part of the 7th track


correspondence=zeros(1,size(tracks{1},1));
 
p=0;
 
tic   
for i=1:size(tracks,1)-1

%Display progress every 1000 iterations
 
    if mod(i,1000)==0
    display([num2str((i/(size(tracks,1)-1))*100),' percent finished in ',num2str(toc/60/60),' hours']);
    end
    
    %extract coordinates at times i and i+1

    from_coords=tracks{i};
    to_coords=tracks{i+1};
    
    %the number of coordinates to assign is the same as the number of
    %coordinates at time i

    num2assign=size(from_coords,1);
    
    %generate new correspondence1 variable

    correspondence1=zeros(1,size(to_coords,1));
    
    %generate grid for all combinations of x/y at time i and x/y at time
    %i+1

    [x_coords,x_coords1]=meshgrid(from_coords(:,1),to_coords(:,1));
    [y_coords,y_coords1]=meshgrid(from_coords(:,2),to_coords(:,2));
    
    %calculate all combinations of change in position of x and y

    X=abs(x_coords-x_coords1);
    Y=abs(y_coords-y_coords1);
    
    %convert dx,dy to a distance using polar coordinates.

    [~,RHO]=cart2pol(X,Y);
    
    %make a list of the coordinates that need to be assigned

    remaining_fromcoords=1:num2assign;
    remaining_tocoords=1:size(to_coords,1);
    
    assigned_numbers=[];
    assigned_xnumbers=[];
    
    for j=1:num2assign
        
        if size(remaining_tocoords,2)<1
            
        % All coordinates at time i have been assigned to coordinates at time i+1
        % Do nothing
            
        else
        
        %find the smallest distance within the distance matrix
        [x,y]=find(RHO==min(min(RHO(remaining_tocoords,remaining_fromcoords))));
        y=setdiff(y,assigned_numbers);
        x=setdiff(x,assigned_xnumbers);
 
        % If there are multiple with the same value, we take
        %the first in the set, and eliminate them one by one within the
        %loop.
        %We are joining coordinates in time, starting with the ones that are the closest together.
        
        %if the chosen coordinate has not yet been assigned to a track,
        %then create a new one and assign it to it.

        if correspondence(y(1))==0
            p=p+1;
            list{p,1}(1,:)=from_coords(y(1),:);
            list{p,1}(2,:)=to_coords(x(1),:);
            list{p,2}=[i,i+1,1];
            correspondence(y(1))=p;

        %otherwise, extend the track saved in the corresponding position in
        %the variable list

        else
            list{correspondence(y(1)),1}(end+1,:)=to_coords(x(1),:);
            list{correspondence(y(1)),2}(2)=list{correspondence(y(1)),2}(2)+1;
            list{correspondence(y(1)),2}(3)=list{correspondence(y(1)),2}(3)+1;
        end
        
        %update correspondences for t+1

        correspondence1(x(1))=correspondence(y(1));
        
        %remove the point that has already been assigned from the list of
        %points that remain to be assigned.

        remaining_fromcoords=setdiff(remaining_fromcoords,y(1));
        remaining_tocoords=setdiff(remaining_tocoords,x(1));
         
        assigned_numbers(end+1)=y(1);
        assigned_xnumbers(end+1)=x(1);
    
        end
    end
    
    correspondence=correspondence1;
    correspondence1=zeros(1,size(to_coords,1));
 
end
 
% when finished, resize list so that it only contains assigned tracks

list=list(1:p-1);
 
end
