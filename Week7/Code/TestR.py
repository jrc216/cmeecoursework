#!/usr/bin/python

""" Testing running an R script from python"""

import subprocess 
subprocess.Popen("Rscript --verbose TestR.R > ../Results/TestR.Rout 2>  ../Results/TestR_errFile.Rout", shell=True).wait()
#dont need relative path for Rscript as has been saved in the bin
#Rscript is instead of saying python in the terminal to run a python script, so if a python script replace Rscript with python or pdfLatex or bash
#if want to pass arguments too, concatneate a string afterwards
#errfile
#doesnt have to be .Rout can have any extension

subprocess.Popen("python"+ "LV1.py" + "1 3 5 6") # passing arguments to a python script 
