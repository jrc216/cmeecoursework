CODE
2_Basic_pop_genomics.R - using R to analyse dense genomic SNP data


DATA
H938_chr15.geno - The frequency of different genotypes in SNP data for the SLC24A5 gene
primate_aligned.fasta - Aotus lemurinus mitochondrion, complete genome
primate_raw.fasta - Aotus lemurinus mitochondrion, complete genome

RESULTS
Cverror.plot1.pdf - Cross Validation error after pruning the SNPs when looking at half-elven population structure for middle earth K is likely to be one, meaning there isnt much population structure
Cverror.plot2.pdf - Cross Validation error after pruning the SNPs once Dwarves have been added to the population structure for middle earth K is likely to be three
Cverror.plot3.pdf - Cross Validation error after pruning the SNPs once giants, orcs and hobbits have been added to the population structure for middle earth K is likely to be tfive or six
Dataset1_plot.pdf - plot showing the genetic make up of half elves
Dataset2_plot.pdf - plot showing the genetic make up of half elves, dwarves and elves
Dataset3_plot.pdf - plot showing the genetic make up of half elves, dwarves, elves, hobbits, giants and elves
defficiency - graph showing the heterozygosity deficiency per SNP found in the population, higher Fs for a particular SNP number mean that SNP is very heterzygote defficient
Fisher_test - graph showing the distribution of p values for the different SNPs
genotype_proprtion - showing how the proportion of genotypes changes with increasing the amount of the P1 allele in the population, includes the expected lines for if the population was in Hardy Weinberg equilibrium
histogram - plot of the total number of observations
ME_Dataset1_LDpruned_0.1.log - log giving details of the SNPs after theyve been pruned when looking at half-elven population structure
ME_Dataset2_LDpruned_0.1.log - log giving details of the SNPs after theyve been pruned when looking at half-elven, elven and dwarven population structures
ME_Dataset3_LDpruned_0.1.log - log giving details of the SNPs after theyve been pruned when looking at half-elven, elven, dwarven, giant, hobbit and orc population structures
observed_vs_expected_heterozygosity - graph plotting the observed vs expected heterozygosity again with a line  for if the population was in Hardy Weinberg equilibrium
p1+p2=1 - plot showing how the frequency of the dominant allele p1 changes with the recessive allele p2 and showing that p1 + p2 = 1
plot1_ME_Dataset1_LDpruned_0.1_MDS_Plot.pdf - MDS plot showing variation in half elven genetic structure
plot1_ME_Dataset2_LDpruned_0.1_MDS_Plot.pdf - MDS plot showing variation in half elven, elven and dwarven genetic structure wihin and among populations
plot1_ME_Dataset3_LDpruned_0.1_MDS_Plot.pdf - MDS plot showing variation in half elven, elven, dwarven, giant, hobbit and orc genetic structure wihin and among populations
sliding_window - take 5 values centred on a focal SNP, weighting them each by 1/5 and then take the sum, produces a local average in a sliding window of 5 SNPs
