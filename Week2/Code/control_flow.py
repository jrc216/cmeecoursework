#!/usr/bin/python
"""functions can run by themselves"""
"""Some functions exemplifying the use of control statements"""
#docstings are considered part of the running code (normal comments are #stripped). Hence, you can access your docstrings at run time.

__author__='Joanna Clarke (jrc216@ic.ac.uk)'
__version__='0.0.1'

#imports
import sys # modules to interface our program with the operating system

#constants can go here

#Functions can go here
def even_or_odd(x=0): #if not specified, x should take value 0.
	
	"""Find whether a number x is even or odd."""
	if x % 2 ==0: #The conditional if # if x/2 = 0, checks remainder
		return "%d is Even!" % x
	return "%d is Odd!" % x
	
def largest_divisor_five (x=120):
	"""Find which is the largest divisor of x among 2,3,4,5."""
	largest = 0
	if x % 5 == 0:  #if remainder after dividing by 5 is 0
		largest = 5
	elif x % 4 == 0: #else if
		largest = 4
	elif x % 3 == 0:
		largest = 3
	elif x % 2 == 0:
		largest = 2
	else: #when all other (if, elif) conditions are not met
		return "No divisor found for %d!" % x #Each function can return a value or a variable.
	return "The largest divisor of %d is %d" % (x, largest)

  
def is_prime(x=70):
	"""Find whether an integer is prime."""
	for i in range(2,x): # "range" returns a sequence of integers
		if x % i == 0:
			print "%d is not a prime: %d is a divisor" % (x,i) #Print formatted text "%d %s %f %e" % (20,"30",0.0003,0.00003)
			 
			return False
	print  "%d is a prime!" % x
	return True
	
def find_all_primes(x=22):
	"""Find all the primes up to x"""
	allprimes = []  # empty list
	for i in range(2, x + 1): # adds one to x for each iteration
		if is_prime(i):   # if the number in that iteration is prime
			allprimes.append(i) # add that number to the list
	print "There are %d primes between 2 and %d" % (len(allprimes), x) #the length of the list of primes replaces the first %d and the final value of x replaces the second %d
	return allprimes
	

def main(argv):
	# sys.exit ("dont want to do this right now!")
	print even_or_odd(22) #prints the results of each loop using the numbers given here
	print even_or_odd(33)
	print largest_divisor_five(120)
	print largest_divisor_five(121)
	print is_prime(60)
	print is_prime(59)
	print find_all_primes(100)
	return 0

if (__name__=="__main__"): #makes sure the "main" function is called from commandline
	status=main(sys.argv)
	sys.exit (status)
