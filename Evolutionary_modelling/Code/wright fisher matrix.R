WF<-function(N)
{
  result<-matrix(nc=2*N+1, nr=2*N+1)
  for (i in 1:nrow(result))
  {result[i,]<-dbinom(0:(2*N), size=2*N, prob=(i-1)/(2*N))}
  return(result)
}

WF(N=2)
WF(N=8)
dim(WF(8))

WF(2)%*%WF(2) #matrix multiplication, equivelent at looking at all paths from 2 to 3 alleles over two timesteps
#look in  (3,4) =  0.21

##############################################
#### Model genetic drift under WF model #########
##############################################

N<-15
p<-0.5
t<-4
#my version
initialise<-function(N, p)
{
  result<-matrix(nr=2, nc=N)
  
  for (cell in 1:length(result))
  {
  result[cell]<-sample(c(0,1),1, replace=TRUE, prob=c(p,p)) # no. of 1s and 0s wouldnt be exactly p
  
  }
  return(result)
  
}
initialise(N, p)

#his version
sim.genetic.drift<-function(p=0.5, N=20, t=10)
{
  #input params arguments as defined above
  #p initial allele freq
  #n = initial pop size
  # t= the no. of gens to be considered
  result<-matrix(nr=2, nc=N)
  #define output formats
  population<-list()
  length(population)<-(t+1)
  
  #give names to every element in population
  names(population)<-numeric()
  for (i in 1:(t+1))
  {
  names(population)[i] <- paste(c("generation", (i-1)), collapse="")
  }  
  #initialise population
  k<-ceiling(2*N*p) # so no decimals
  population[[1]]<-matrix(sample(c(rep(0, k), rep(1, 2*N-k))), nr=2) #shuffling 0s and 1s from allele pool
    
  #allele frequence
  allele_freq<-numeric()
  initial_freq<-sum(population[[1]])/(2*N)
  allele_freq<-c(allele_freq, initial_freq)
  #propogation
  for (i in 2:(t+1))
  {
  population[[i]]<-matrix(sample(0:1, size=2*N, replace=TRUE, prob=c(allele_freq[i-1], 1-allele_freq[i-1])), nr=2)
  freq<-sum(population[[i]])/(2*N)
  allele_freq<-c(allele_freq, freq)
  }
  #print(allele_freq)
  
  #what do you wish to return?
  return(list(population=population, allele_freq=allele_freq))
  #return(population)
  #return(allele_freq)
  #return(names)
}

#Run it

sim.genetic.drift()

#################################
####### GENETIC DRIFT 2 #####################
######################################

sim.genetic.drift2<-function(p=0.5, N=20, t=10)
{
  #initialisation
  allele_freq<-numeric()
  allele_freq[1]<-p
  
  #propogation
  for (i in 2:(t+1))
  {
    allele_freq[i]<-rbinom(1, size=2*N, prob=allele_freq[i-1])/(2*N)
    }
  print(i)
  }
  
  #return
  return(allele_freq)
}
  #run it
sim.genetic.drift2()


##SPEED TEST#####
system.time(sim.genetic.drift(N=20000, t=2000, p=0.5))
system.time(sim.genetic.drift2(N=20000, t=2000, p=0.5))



sim.drift<-function(p=0.5, N=2000)
{
  #initialisation
  #allele_freq<-numeric()
  allele_freq<-p
  t = 1
  #propogation
    while((allele_freq<1) && (allele_freq>0))
    {
      allele_freq<-rbinom(1, size=2*N, prob=allele_freq)/(2*N)
      t=t+1
    }
    if(allele_freq==0){
      #print(paste("EXTINCTION after", t, "generations"))
    }
  if (allele_freq==1){
    #print(paste("FIXATION after", t, "generations"))
  }
return(t)
}
#run it
sim.drift()

t <- numeric()
for (i in 1:1000)
{
 t<-c(t, sim.drift(p=0.5, N=2000))
}
mean_t<-mean(t, na.rm=T)
var_t<-var(t, na.rm=T)




########################
######repeat simulation 10000 times and store final allele frequencies






frequencies<-numeric()
for (i in 1:10000)
{
frequencies<-c(frequencies, sim.genetic.drift2(p=0.5, N=200, t=10)[11])
}
mean_f<-mean(frequencies, na.rm=T)
var(frequencies, na.rm=T)


frequencies<-list()
for (i in 1:10000)
{
  frequencies[[i]]<-sim.genetic.drift2(p=0.5, N=200, t=50)
}
frequencies

mean_f<-list()
sums<-numeric()



sums<-numeric()
for (vec in 1:10000)
{
  sums<-c(sums, frequencies[[vec]][1])
}

mean(sums)

