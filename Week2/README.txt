PYTHON
basic_csv.py - importing csv package to make it easier to manipulate csv files
basic_io.py - importing and exporting data
boilerplate.py - template for making python scripts
cfexercise.py - Some simple functions exemplifying the use of control statements
control_flow.py - Some simple functions exemplifying the use of control statements
loops.py - for loops in python
oaks.py - Extracting oak tree species from a list into a new list using loops and list comprehensions
scope.py - creating a global variable
sysargv.py - arg - argument variable - feed variables to python functions/holds arguments you pass to your python script when you run it
using_name.py - file is usable as a script and not just an imported module
lc1.py - script to separate latin names, common names and mean body masses for bird species
lc2.py - using list comprehensions and loops to extract data from a tuple of tuples that satisfy certain requirements
dictionary.py - create a dictionary from a tuple of tuples. grouping species in the same order together
tuple.py - printing tuples on separate lines
debugme.py  - created a bug so we can learn to search for it
regexs.py - examples of regular expressions
test_oaks.py -  Extracting oak tree species from a list into a new list using loops and list comprehensions, originally had some bugs but these were removed
align_seqs.py - Takes two DNA sequences as an input from an external file and finds the best match between two DNA sequences then saves the best alignment along with its corresponding score in a single text file
align_seqs_fasta.py - Takes two DNA sequences as an input from a fasta csv file and finds the best match between two DNA sequences then saves the best alignment along with 
its corresponding score in a single text file

