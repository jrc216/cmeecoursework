#!/usr/bin/python
""" Running fmr.R from python"""

import subprocess 
import os.path
subprocess.Popen("Rscript --verbose fmr.R", shell=True).wait()

if os.path.isfile("../Results/fmr_plot.pdf"):
	print 'graph printed successfully'
else: 
	print 'fmr.R failed'

if os.path.isfile("../Results/species.csv"):
	print 'csv produced successfully'
else:
	print 'fmr.R failed'
