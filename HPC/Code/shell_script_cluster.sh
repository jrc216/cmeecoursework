#!/bin/bash
#PBS -l walltime=12:00:00
#PBS -l select=1:ncpus=1:mem=800mb
module load R/3.3.2
module load intel-suite
echo "R is about to run"
R --vanilla < $WORK/cluster_run.R
mv Results* $WORK
echo "R has finished running”
# this is a comment at the end of the file
