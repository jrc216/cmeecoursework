#!/usr/bin/python
"""Function 1 """
def a_useless_function(x):
	y = 0
	for i in xrange(10000000):
		y = y + i
	return 0

"""2Function 2"""
def a_less_useless_function(x):
	y = 0
	for i in xrange(100000):
		y = y + i
	return 0

"""Function 3 """
def some_function(x):
	print x
	a_useless_function(x)
	a_less_useless_function(x)
	return 0

some_function(1000)

	
