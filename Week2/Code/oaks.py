"""Extracting oak tree species from a list into a new list using loops and list comprehensions"""
## Let's find just those taxa that are oak trees from a list of species

taxa = [ 'Quercus robur',
		 'Fraxinus excelsior',
		 'Pinus sylvestris',
		 'Quercus cerris',
		 'Quercus petraea',]

def is_an_oak(name):  #define is_an_oak
	return name.lower().startswith('quercus ') # if name starts with quercus print name in lower case
	
##Using for loops

oaks_loops = set () #makes oaks_loops a set so it can have functions applied to it
for species in taxa:
	if is_an_oak(species): # if the species starts with quercus as that's part of defining is_an_oak, then add to is_an_oak
		oaks_loops.add(species) 
print oaks_loops

##Using list comprehensions
oaks_lc = set([species for species in taxa if is_an_oak(species)])
print oaks_lc

##Get names in UPPER CASE using for loops
oaks_loops = set ()
for species in taxa:
	if is_an_oak(species):
		oaks_loops.add(species.upper()) #.add = add
print oaks_loops

##Get names in UPPER CASE using list comprehensions
oaks_lc = set ([species.upper() for species in taxa if is_an_oak(species)])
print oaks_lc


