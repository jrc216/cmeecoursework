"""Author = Joanna Clarke Average UK Rainfall"""
# Average UK Rainfall (mm) for 1910 by month
# http://www.metoffice.gov.uk/climate/uk/datasets

rainfall = (('JAN',111.4),
            ('FEB',126.1),
            ('MAR', 49.9),
            ('APR', 95.3),
            ('MAY', 71.8),
            ('JUN', 70.2),
            ('JUL', 97.1),
            ('AUG',140.2),
            ('SEP', 27.0),
            ('OCT', 89.4),
            ('NOV',128.4),
            ('DEC',142.2),)

#1 using list comprehension to form a list of month, rainfall tuples where the amount of rain is greater than 100mm
rainfall_lc = set([months for months in rainfall if months[1] > 100])
print rainfall_lc

#2 using list comprehension to form a list of month names where the amunt of rainfall was less than 50mm
rainfall_lc2 = set([months[0] for months in rainfall if months[1] < 50])
print rainfall_lc

#~ #3 usings conventional loops to form lists of rainfall
rainfall_list1 = [] #create an empty list
for months in rainfall: # cycles through each tuple in rainfall
	if months[1] > 100: # if the amount of rainfall is greater than 100
		rainfall_list1 += [months] # add the tuple to the list
print rainfall_list1 #print list

rainfall_list2 = []
for months in rainfall:
	if months[1] < 50:
		rainfall_list2 += [months[0]]
print rainfall_list2 

