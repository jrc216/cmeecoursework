#!/usr/bin/python

""" The typical Lotka-Volterra Model simulated using scipy """

import scipy as sc 
import scipy.integrate as integrate
import pylab as p #Contains matplotlib for plotting
import sys
import cProfile
# import matplotlip.pylab as p #Some people might need to do this

def dCR_dt(pops, t=0): # sets up a funtion object
    """ Returns the growth rate of predator and prey populations at any 
    given time step """
    
    R = pops[0]
    C = pops[1]
    dRdt = r*R*(1-(R/K)) - a*R*C 
    dCdt = -z*C + e*a*R*C
    
    return sc.array([dRdt, dCdt])

# Define parameters:
r = float(sys.argv[1]) # Resource growth rate
a = float(sys.argv[2]) # Consumer search rate (determines consumption rate) 
z = float(sys.argv[3]) # Consumer mortality rate
e = float(sys.argv[4]) # Consumer production efficiency
K = float(sys.argv[5]) # carrying capacity of resource
# Now define time -- integrate from 0 to 15, using 1000 points(set timespace as 15):
t = sc.linspace(0, 15,  1000) # how tightly youre integrating your function

x0 = 10
y0 = 5 
z0 = sc.array([x0, y0]) # initials conditions: 10 prey and 5 predators per unit area

pops, infodict = integrate.odeint(dCR_dt, z0, t, full_output=True)

infodict['message']     # >>> 'Integration successful.' # dictionary, printing in terminal if successful

prey, predators = pops.T # .T = transpose
f1 = p.figure() #Open empty figure object using pylab
p.plot(t, prey, 'g-', label='Resource density') # Plot
p.plot(t, predators  , 'b-', label='Consumer density')
p.annotate('r=%r, a=%r, z=%r, e=%r, K=%r'%(r,a,z,e, K), xy=(2,1), xytext=(7, 12))
p.grid()
p.legend(loc='best')
p.xlabel('Time')
p.ylabel('Population')
p.title('Consumer-Resource population dynamics')
#p.show()
f1.savefig('../Results/prey_and_predators_2.pdf') #Save figure


	


