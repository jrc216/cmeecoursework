"""arg - argument variable - feed variables to python functions/holds arguments you pass to your python script when you run it"""

import sys #a module that holds arguments that pass to my python script when it is run
print "This is the name of the script: ", sys.argv[0] #command line argument
print "Number of arguments: ", len(sys.argv) #length/number of arguments held in sys
print "The arguments are: " , str(sys.argv) #a string

