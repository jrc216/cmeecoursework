#!/usr/bin/python
""" The typical Lotka-Volterra Model simulated using scipy """

import scipy as sc 
import scipy.integrate as integrate
import pylab as p #Contains matplotlib for plotting
import cProfile
# import matplotlip.pylab as p #Some people might need to do this

def dCR_dt(pops, t=0): # Sets up a function object
    """ Returns the growth rate of predator and prey populations at any 
    given time step """
    
    R = pops[0]
    C = pops[1]
    dRdt = r*R - a*R*C 
    dCdt = -z*C + e*a*R*C
    
    return sc.array([dRdt, dCdt]) # Return as a scipy array, the two as vectors

# Define parameters:
r = 1. # Resource growth rate
a = 0.1 # Consumer search rate (determines consumption rate) 
z = 1.5 # Consumer mortality rate
e = 0.75 # Consumer production efficiency

# Now define time -- integrate from 0 to 15, using 1000 points:
t = sc.linspace(0, 15,  1000) # Want to know between 0 and 15 epochs, divided into 1000 - how tightly you want to follow the function.

x0 = 10
y0 = 5 
z0 = sc.array([x0, y0]) # initials conditions: 10 prey and 5 predators per unit area

pops, infodict = integrate.odeint(dCR_dt, z0, t, full_output=True)
# ?integrate - what the modifiers are
# odeint - integrate an ODE

infodict['message']     # >>> 'Integration successful.'
# dictionary
# prints whether integration successful

prey, predators = pops.T # What's this for?
# '.T' - transpose - like output to be vertical not horizontal

f1 = p.figure() #Open empty figure object
p.plot(t, prey, 'g-', label='Resource density') # Plot
p.plot(t, predators  , 'b-', label='Consumer density')
p.grid()
p.legend(loc='best')
p.xlabel('Time')
p.ylabel('Population')
p.title('Consumer-Resource population dynamics')
# p.show() - As opening the plot pauses the script
f1.savefig('../Results/prey_and_predators_1.pdf') #Save figure
# Plotting


# Need check speed
