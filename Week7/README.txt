CODE
Biotraits.db - database of traitinfo for metabolic rates
blackbirds.py - extracting blackbirds taxonomy information
DrawFW.py - Plot a snapshot of a food web graph/network
fmr.R - Plots log(field metabolic rate) against log(body mass) for the Nagy et al 1999 dataset
LV1.py - Lottka Volterra model
LV2.py - Lottka Volterra with a carrying capacity on resource limitation added and with the ability to run the script using arguments from the command line
LV3.py - appropriate parameters are inputed to LV2 so that the predator and prey values reach equilibrium, the final population numbers are then printed to the screen
LV4.py - DIscrete time version of LV3
LV%.py - random error added into Discrete time model
prey persist with prey density dependence the final (non-zero) population values should be
printed to screen.
profileme.py - using %run -p to give details of how long your script and its components took to run 
nets.R - creating a network coded by colour 
regexs.py - some examples of regular expressions
regexs2.py - more examples of regular expressions and extracting dates
run_fmr_R.py - running fmr.R from python
run_LV.sh - a bash script that runs LV1.py and LV2.py
SQLinR.R - running SQLite in R
SQLite.py - SQLite in python
subprocess_play.py - trying out subprocess in python
Test.sqlite - test sqlite database
TestR.py - Testing running an R script from python
TestR.R - Test R script to run from python
timeitme.py - finding the best way to run a script in terms of speed by timing it
using_os.py - Using os to run bash commands in a python sript

DATA
Biotraits.db - Biotraits database created in SQLite exercise
blackbirds.txt - taxonomy information for different blackbird species
Consumer.csv - Consumer taxon data used in sqlite exercise
NagyEtAl1999.csv - Mass and metabolic rate data used in fmr.R
QMEE_Net_Mat_edges.csv - edge info for nets.R
QMEE_Net_Mat_nodes.csv - node info for nets.R
Resource.csv - Resource taxon data used in sqlite exercise
TCP.csv - Temperature and mass data for consumer and resource used in SQLite exercise
test.db - test database in SQLite
test.sqlite - consumer and resource tables in SQLite
TraitInfo.csv - Trait info for TCP data in the SQLite exercise

RESULTS
errorFile.Rout - output of subprocess.Popen in TestR.py
fmr_plot.pdf -   Plot of log(field metabolic rate) against log(body mass) for the Nagy et al 1999 dataset
outputFile.Rout - output of subprocess.Popen in TestR.py
QMEENet.svg - output of Nets.R, colour coded networks plot of different organisations
prey_and_predators_1.pdf - graph of the Lottka Volterra model when there are no limitations put on resource density
prey_and_predators_2.pdf - graph of the Lottka Volterra model with limitations put on resource density, r, a, z, e and K are defined on the plot
prey_and_predators_3.pdf - prey_and_predators_2.pdf but with the population going to equilibrium
prey_and_predators_4.pdf - graph of discrete time Lottka Volterra model
prey_and_predators_5.pdf - graph of discrete time Lottka Volterra wit random error added
species.csv - sorted data from the nagy dataset
TestR.Rout - output of subprocess.Popen in TestR.py
TestR_errFile.Rout - output of subprocess.Popen in TestR.py

