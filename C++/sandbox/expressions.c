#include <stdio.h>

int main (void)
{
	int x = 4;
	int y = 7;
	int i_result = 0;
		
	float f_y = 7.0;
	float f_result = 0.0;
		
	//char alpha = 'J';
	//char beta = 'T';
	//char c_result = 0;
	
	// + -  * %
	
	i_result = y / x;   //answer is an integer so  1, (the actual number is 1.7 so chops off other numbers after 1 instead of rounding it
	//f_result = f_y/x; // = 1.750000 - only changed 1 data type to high value (float) compiler promotes answer to higher variabel type
	f_result = (float) y / (float)x; // treat x and y as float type
	//c_result = beta - 2;
	
	printf("i_result: %i\n", i_result);
	//printf("c_result: %c\n", (char)c_result); //(char) turns into a character value
	printf("f_result: %f\n", f_result); //or:
	//printf("f_result: %f\n", f_y/x);
	return 0;
}

// memory hierarchy: char, short signed int, short unsigned int, doubles and floats (related to size then complexity)

//bool can be 0 or 1
//if include stdbool.h "true" and "false" will be included in words
