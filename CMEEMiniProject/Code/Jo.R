rm(list=ls())

# Required packages...
library(ggplot2, graphics)
library(lattice)
library(minpack.lm)

#############################

# F  U  N  C  T  I  O  N  S #
#############################

# assign("Tref", 0 + 273.15, envir = .GlobalEnv) ## Set to Inf if you want to remove the normalization, so 1/Tref will be 0
assign("Tref", 15 + 273.15, envir = .GlobalEnv) # **justify value


#### Estimate STARTING VALUES for the nls

GetTpk <- function(tmp, rate){
  # Temperature at which the rate is maximised (estimate of T.peak).
  # ~~~ Parameters ~~~
  # tmp  : Temperature data (in K).
  # rate : Rate data corresponding to temperature above.
  
  return(max(tmp[which.max(rate)]))
}



GetE <- function(tmp, rate, T.p, k=8.62e-5){
  # Estimate starting value for E, taking linear regression using the rise part
  tmp.w <- which(tmp <= T.p)
  if (length(tmp.w) > 1)
  {
    m <- lm(log(rate[tmp.w]) ~ I(1 / (k * (tmp[tmp.w]))))
    tryCatch(abs(summary(m)$coefficients[2, 1]),
      error = function(e) {return(0.6)})
  }
  else{
    return(0.6)
  }
}



GetB0 <- function(tmp, rate){

  if (min(tmp,na.rm=TRUE) > Tref)
  {
    return(log(min(rate[1],na.rm=TRUE)))
  } else
  {
    return(log(max(rate[which(tmp <= Tref)],na.rm=TRUE)))
  
  }
}



###################### Boltzmann - Arrhenius model. #################
Boltzmann.Arrhenius <- function(B0, E, temp) {

    
  # Boltzmann's constant. Units imply that E is in eV.
  k <- 8.62e-5 
  
  calc <- B0 - E/k * (1/temp - 1/Tref)
  
  return(calc)
}

###################### Schoolfield model ######################



Schoolfield <- function(B0, E, E_D, T_h, temp, SchoolTpk=TRUE)
{ 
 
  k <- 8.62e-5 
  
  
  if (SchoolTpk==TRUE) # Sharpe-Schoolfield model with explicit T_pk parameter
  {
    return(B0 + log(exp((-E/k) * ((1/temp)-(1/Tref)))/(1 + (E/(E_D - E)) * exp(E_D/k * (1/T_h - 1/temp)))))
    
  } else { # Original Sharpe-Schoolfield's model with low-temp inactivation term removed, slightly modified                                                                                                                     
    return(B0 + log(exp((-E/k) * ((1/temp) - (1/Tref)))/(1 + exp((E_D/k) * (1/T_h - 1/temp))))) }
  
}

####################################################################################
############################# M  A  I  N    C  O  D  E #############################
####################################################################################

# set wd

# load some data
# original_data <- read.csv("../Modelling/Schoolfield/results/dataset.csv", sep = "\t")
thermal_data <- read.csv('../Data/CutData.csv', stringsAsFactors = F)
stats<-read.csv('../Results/poly_stats2.csv')


#dim(thermal_data) # 17519*39
thermal_data$K <- thermal_data$SynthTemp + 273.15
idsubset<-subset(thermal_data, thermal_data$FinalID=="MTD4290")

################################
##### NLS For 1 ID ############
################################
pdf("../Results/NLSoneID.pdf")
d <- idsubset
tmp <- d$K #get parameters for each id
rate <- d$NewStandardisedTraitValue
T.p <- GetTpk(tmp, rate)
E.st <- GetE(tmp, rate, T.p, k=8.62e-5) #0.6
B.st <- GetB0(tmp, rate)
k <- 8.62*10^(-5)
Schoolfield_nls <-
  tryCatch(nlsLM(
    log(NewStandardisedTraitValue) ~ Schoolfield(B0, E, E_D, T_h, temp = K),
    start = c(B0 = B.st, E = E.st, E_D = 0.1, T_h = T.p),
    lower=c(B0=-Inf, E=k, E_D = 2*k, T_h = 273.15 - 50),
    upper=c(B0=Inf,  E=3, E_D = 6, T_h = 273.15 + 50), 
    control=list(minFactor=1 / 2^16, maxiter=1024),
    data = d,
    na.action=na.omit),
    silent=TRUE, error=function(e) NA)


E_deviants <- rnorm(100, mean = E.st) # vary E est using rnorm and run nls again
for(E.dv in E_deviants){
  if(is.na(Schoolfield_nls)){
    tryCatch(
      Schoolfield_nls <- nlsLM(
        log(NewStandardisedTraitValue) ~ Schoolfield(B0, E, E_D, T_h, temp = K),
        start = c(B0 = B.st, E = E.dv, E_D = 0.1, T_h = T.p),
        lower=c(B0=-Inf, E=k, E_D = 2*k, T_h = 273.15 - 50), 
        upper=c(B0=Inf,  E=3, E_D = 6, T_h = 273.15 + 50), 
        control=list(minFactor=1 / 2^16, maxiter=1024),
        data = d,
        na.action=na.omit),
      silent=TRUE, error=function(e) NA)
  } 
}
plot(d$K, log(d$NewStandardisedTraitValue), xlab="temperature(K)", ylab="log Trait value", main="MTD2490")
data2plot = data.frame(K = seq(min(d$K), max(d$K), len = 200))
lines(data2plot$K, predict(Schoolfield_nls, newdata = data2plot), col="red")


dev.off()


#####################################
##### NLS FOR ALL IDs ##################
##############################
thermal_data<-thermal_data[-(19028), ]
set.seed(1)
pdf("../Results/NLS.pdf")
ids<-unique(thermal_data$FinalID)
AIC_df<-c()
r_sqrd<-c()
id<-c()
trait_name<-c()
habitat<-c()
E<-c()
for(i in ids){
  id<-c(id, i)
  d <- subset(thermal_data, thermal_data$FinalID == i)
  trait_name<-c(trait_name, unique(d$trait_name))
  habitat<-c(habitat, unique(d$Habitat))
  tmp <- d$K #get parameters for each id
  rate <- d$NewStandardisedTraitValue
  T.p <- GetTpk(tmp, rate)
  #GetTpk(tmp = id$K, rate = id$Trait_minus_min)
  E.st <- GetE(tmp, rate, T.p, k=8.62e-5) #0.6
  E<-c(E, E.st)
  B.st <- GetB0(tmp, rate)
  k <- 8.62*10^(-5)
  Schoolfield_nls <-
  tryCatch(nlsLM(
      log(NewStandardisedTraitValue) ~ Schoolfield(B0, E, E_D, T_h, temp = K),
      start = c(B0 = B.st, E = E.st, E_D = 0.1, T_h = T.p),
      lower=c(B0=-Inf, E=k, E_D = 2*k, T_h = 273.15 - 50), 
      upper=c(B0=Inf,  E=3, E_D = 6, T_h = 273.15 + 50), 
      control=list(minFactor=1 / 2^16, maxiter=1024),
      data = d,
      na.action=na.omit),
      silent=TRUE, error=function(e) NA)
  
  
  E_deviants <- rnorm(100, mean = E.st) 
  for(E.dv in E_deviants){
    if(is.na(Schoolfield_nls)){
      tryCatch(
        Schoolfield_nls <- nlsLM(
          log(NewStandardisedTraitValue) ~ Schoolfield(B0, E, E_D, T_h, temp = K),
          start = c(B0 = B.st, E = E.dv, E_D = 0.1, T_h = T.p),
          lower=c(B0=-Inf, E=k, E_D = 2*k, T_h = 273.15 - 50), 
          upper=c(B0=Inf,  E=3, E_D = 6, T_h = 273.15 + 50), 
          control=list(minFactor=1 / 2^16, maxiter=1024),
          data = d,
          na.action=na.omit),
        silent=TRUE, error=function(e) NA)
    }
  }

  # if(i == 767){browser()}
      AIC_df<-c(AIC_df, tryCatch(AIC(Schoolfield_nls),
                                 error = function(e) NA))
      TSS <- sum((rate- mean(rate))^2)
      if(!is.na(Schoolfield_nls)){
        predicted_y_nls <- predict(Schoolfield_nls) # predict y from x values in model
        RSS_nls <- sum((rate - exp(predicted_y_nls))^2) # **exp - linear scale
        R2_nls <- 1 - (RSS_nls / TSS)
        #r_sqrd<-c(r_sqrd,R2_nls)
        plot(d$K, log(d$NewStandardisedTraitValue), xlab="temperature(K)", ylab="log Trait value", main=paste(i)) 
        data2plot = data.frame(K = seq(min(d$K), max(d$K), len = 200))
        lines(data2plot$K, predict(Schoolfield_nls, newdata = data2plot), col="red") 
        
      }else{R2_nls=NA}
      r_sqrd<-c(r_sqrd,R2_nls)
      
      }
dev.off() 

mean(E)


nls_stats<-cbind(id, trait_name, habitat, AIC_df, r_sqrd)

write.csv(nls_stats, "../Results/nls_stats.csv")

############################################################
###########################################################
###########################################################


#cbind all the stats together
#piehart

nls_stats<-read.csv("../Results/nls_stats.csv")
poly_stats<-read.csv("../Results/poly_stats2.csv")
#nls_stats<-poly_stats[-(499), ]

merged_stats <- cbind(nls_stats, poly_stats) 


merged_stats<- subset(merged_stats, merged_stats$AIC_df!="NA") #remove NAs
merged_stats<- subset(merged_stats, merged_stats$r_sqrd!="NA")
merged_stats<- subset(merged_stats, merged_stats$AIC_df!="-Inf") #remove -Infs
merged_stats<- subset(merged_stats, merged_stats$r_sqrd!="-Inf")
merged_stats<- subset(merged_stats, merged_stats$AIC!="NA") #remove NAs
merged_stats<- subset(merged_stats, merged_stats$r.squared!="NA")
merged_stats<- subset(merged_stats, merged_stats$AIC!="-Inf") #remove -Infs
merged_stats<- subset(merged_stats, merged_stats$r.squared!="-Inf")

nrow(merged_stats)

############################
#################### AICs ###############################
########################################################
NLS<-numeric()
Poly<-numeric()
equal<-numeric()
for(row in 1:nrow(merged_stats)){ #do same for polynomial
  #print(row)
  if(merged_stats$AIC[row] < merged_stats$AIC_df[row]){
    Poly<-c(Poly, 1)}
  if (merged_stats$AIC[row] > merged_stats$AIC_df[row]){
    NLS <- c(NLS, 1)}
  if(merged_stats$AIC[row] == merged_stats$AIC_df[row]){
      equal<-c(equal, 1)}
}
#######################################################
################ AIC Pie charts ########################
#####################################################
length(NLS)  
length(Poly)
length(equal)

pdf("../Results/pie.pdf")
# Create data for the graph.
x <- c(length(NLS), length(Poly))
labels <- c("NLLS", "Polynomial")
piepercent<- round(100*x/sum(x), 1)


# Plot the chart.
#pie(x,labels)
pie(x, labels = piepercent, main = "Percentage of AICs that were lowest between two models",col = rainbow(length(x)))
legend("topright", c("NLLS", "Polynomial"), cex = 0.8,
       fill = rainbow(length(x)))


# Save the file.
dev.off()
#########################################################
################## AICs by trait type ####################
#########################################################
traits<-unique(merged_stats$trait_name)

NLS_growth<-numeric()
Poly_growth<-numeric()
NLS_net_phot_r<-numeric()
Poly_net_phot_r<-numeric()
NLS_gross_phot_r<-numeric()
Poly_gross_phot_r<-numeric()
NLS_respiration<-numeric()
Poly_respiration<-numeric()
NLS_cell<-numeric()
Poly_cell<-numeric()
NLS_oxygen<-numeric()
Poly_oxygen<-numeric()
NLS_net_phot<-numeric()
Poly_net_phot<-numeric()
NLS_ass_net_phot<-numeric()
Poly_ass_net_phot<-numeric
NLS_gross_phot<-numeric()
Poly_gross_phot<-numeric()

for(row in 1:nrow(merged_stats)){ #do same for polynomial
  if (merged_stats$trait_name[row] == "growth rate")
  {
    if(merged_stats$AIC[row] < merged_stats$AIC_df[row]){
      Poly_growth<-c(Poly_growth, 1)}
    if (merged_stats$AIC[row] > merged_stats$AIC_df[row]){
      NLS_growth <- c(NLS_growth, 1)}
  }
  if (merged_stats$trait_name[row] == "net photosynthesis rate")
  {
    if(merged_stats$AIC[row] < merged_stats$AIC_df[row]){
      Poly_net_phot_r<-c(Poly_net_phot_r, 1)}
    if (merged_stats$AIC[row] > merged_stats$AIC_df[row]){
      NLS_net_phot_r <- c(NLS_net_phot_r, 1)}
  }
  if (merged_stats$trait_name[row] == "gross photosynthesis rate")
  {
    if(merged_stats$AIC[row] < merged_stats$AIC_df[row]){
      Poly_gross_phot_r<-c(Poly_gross_phot_r, 1)}
    if (merged_stats$AIC[row] > merged_stats$AIC_df[row]){
      NLS_gross_phot_r <- c(NLS_gross_phot_r, 1)}
  }
  if (merged_stats$trait_name[row] == "respiration rate")
  {
    if(merged_stats$AIC[row] < merged_stats$AIC_df[row]){
      Poly_respiration<-c(Poly_respiration, 1)}
    if (merged_stats$AIC[row] > merged_stats$AIC_df[row]){
      NLS_respiration <- c(NLS_respiration, 1)}
  }
  if (merged_stats$trait_name[row] == "cell-specific photosynthesis rate")
  {
    if(merged_stats$AIC[row] < merged_stats$AIC_df[row]){
      Poly_cell<-c(Poly_cell, 1)}
    if (merged_stats$AIC[row] > merged_stats$AIC_df[row]){
      NLS_cell <- c(NLS_cell, 1)}
  }
  if (merged_stats$trait_name[row] == "oxygen evolution rate in thylakoid membranes")
  {
    if(merged_stats$AIC[row] < merged_stats$AIC_df[row]){
      Poly_oxygen<-c(Poly_oxygen, 1)}
    if (merged_stats$AIC[row] > merged_stats$AIC_df[row]){
      NLS_oxygen <- c(NLS_oxygen, 1)}
  }
  if (merged_stats$trait_name[row] == "net photosynthesis")
  {
    if(merged_stats$AIC[row] < merged_stats$AIC_df[row]){
      Poly_net_phot<-c(Poly_net_phot, 1)}
    if (merged_stats$AIC[row] > merged_stats$AIC_df[row]){
      NLS_net_phot <- c(NLS_net_phot, 1)}
  }
  if (merged_stats$trait_name[row] == "assumed net photosyntesis")
  {
    if(merged_stats$AIC[row] < merged_stats$AIC_df[row]){
      Poly_ass_net_phot<-c(Poly_ass_net_phot, 1)}
    if (merged_stats$AIC[row] > merged_stats$AIC_df[row]){
      NLS_ass_net_phot <- c(NLS_ass_net_phot, 1)}
  }
  if (merged_stats$trait_name[row] == "gross photosynthesis")
  {
    if(merged_stats$AIC[row] < merged_stats$AIC_df[row]){
      Poly_gross_phot<-c(Poly_gross_phot, 1)}
    if (merged_stats$AIC[row] > merged_stats$AIC_df[row]){
      NLS_gross_phot <- c(NLS_gross_phot, 1)}
  }
}

# length(NLS_growth)
# length(Poly_growth)
# length(NLS_net_phot_r)
# length(Poly_net_phot_r)
# length(NLS_gross_phot_r)
# length(Poly_gross_phot_r)
# length(NLS_respiration)
# length(Poly_respiration)
# length(NLS_cell)
# length(Poly_cell)
# length(NLS_oxygen)
# length(Poly_oxygen)
# length(NLS_net_phot)
# length(Poly_net_phot)
# length(NLS_ass_net_phot)
# length(Poly_ass_net_phot)
# length(NLS_gross_phot)
# length(Poly_gross_phot)

####################################################
#################### PIE CHARTS #######################
#################################################

pdf("../Results/pie_growth_rate.pdf")
# Create data for the graph.
x <- c(length(NLS_growth), length(Poly_growth))
labels <- c("NLLS", "Polynomial")
piepercent<- round(100*x/sum(x), 1)


# Plot the chart.
#pie(x,labels)
pie(x, labels = piepercent, main = "Percentage of AICs that were lowest between two models for the trait growth rate",col = rainbow(length(x)))
legend("topright", c("NLLS", "Polynomial"), cex = 0.8,
       fill = rainbow(length(x)))


# Save the file.
dev.off()

###############################

pdf("../Results/pie_net_phot_r.pdf")
# Create data for the graph.
x <- c(length(NLS_net_phot_r), length(Poly_net_phot_r))
labels <- c("NLLS", "Polynomial")
piepercent<- round(100*x/sum(x), 1)


# Plot the chart.
#pie(x,labels)
pie(x, labels = piepercent, main = "Percentage of AICs that were lowest between two models for the net photosynthesis rate", col = rainbow(length(x)))
legend("topright", c("NLLS", "Polynomial"), cex = 0.8,
       fill = rainbow(length(x)))


# Save the file.
dev.off()

###############################

pdf("../Results/pie_gross_phot_r.pdf")
# Create data for the graph.
x <- c(length(NLS_gross_phot_r), length(Poly_gross_phot_r))
labels <- c("NLLS", "Polynomial")
piepercent<- round(100*x/sum(x), 1)


# Plot the chart.
#pie(x,labels)
pie(x, labels = piepercent, main = "Percentage of AICs that were lowest between two models for the gross photosynthesis rate", col = rainbow(length(x)))
legend("topright", c("NLLS", "Polynomial"), cex = 0.8,
       fill = rainbow(length(x)))


# Save the file.
dev.off()


###############################

pdf("../Results/pie_respiration.pdf")
# Create data for the graph.
x <- c(length(NLS_respiration), length(Poly_respiration))
labels <- c("NLLS", "Polynomial")
piepercent<- round(100*x/sum(x), 1)


# Plot the chart.
#pie(x,labels)
pie(x, labels = piepercent, main = "Percentage of AICs that were lowest between two models for respiration", col = rainbow(length(x)))
legend("topright", c("NLLS", "Polynomial"), cex = 0.8,
       fill = rainbow(length(x)))


# Save the file.
dev.off()

###############################

pdf("../Results/pie_cell.pdf")
# Create data for the graph.
x <- c(length(NLS_cell), length(Poly_cell))
labels <- c("NLLS", "Polynomial")
piepercent<- round(100*x/sum(x), 1)


# Plot the chart.
#pie(x,labels)
pie(x, labels = piepercent, main = "Percentage of AICs that were lowest between two models for cell-specific photosynthesis rate", col = rainbow(length(x)))
legend("topright", c("NLLS", "Polynomial"), cex = 0.8,
       fill = rainbow(length(x)))


# Save the file.
dev.off()


###############################

pdf("../Results/pie_oxygen.pdf")
# Create data for the graph.
x <- c(length(NLS_oxygen), length(Poly_oxygen))
labels <- c("NLLS", "Polynomial")
piepercent<- round(100*x/sum(x), 1)


# Plot the chart.
#pie(x,labels)
pie(x, labels = piepercent, main = "Percentage of AICs that were lowest between two models for oxygen evolution rate in thylakoid membranes", col = rainbow(length(x)))
legend("topright", c("NLLS", "Polynomial"), cex = 0.8,
       fill = rainbow(length(x)))


# Save the file.
dev.off()


###############################

pdf("../Results/pie_net_phot.pdf")
# Create data for the graph.
x <- c(length(NLS_net_phot), length(Poly_net_phot))
labels <- c("NLLS", "Polynomial")
piepercent<- round(100*x/sum(x), 1)


# Plot the chart.
#pie(x,labels)
pie(x, labels = piepercent, main = "Percentage of AICs that were lowest between two models for net photosynthesis", col = rainbow(length(x)))
legend("topright", c("NLLS", "Polynomial"), cex = 0.8,
       fill = rainbow(length(x)))


# Save the file.
dev.off()

###############################

pdf("../Results/pie_ass_net_phot.pdf")
# Create data for the graph.
x <- c(length(NLS_ass_net_phot), length(Poly_ass_net_phot))
labels <- c("NLLS", "Polynomial")
piepercent<- round(100*x/sum(x), 1)


# Plot the chart.
#pie(x,labels)
pie(x, labels = piepercent, main = "Percentage of AICs that were lowest between two models for assumed net photosynthesis", col = rainbow(length(x)))
legend("topright", c("NLLS", "Polynomial"), cex = 0.8,
       fill = rainbow(length(x)))


# Save the file.
dev.off()


###############################

pdf("../Results/pie_gross_phot.pdf")
# Create data for the graph.
x <- c(length(NLS_gross_phot), length(Poly_gross_phot))
labels <- c("NLLS", "Polynomial")
piepercent<- round(100*x/sum(x), 1)


# Plot the chart.
#pie(x,labels)
pie(x, labels = piepercent, main = "Percentage of AICs that were lowest between two models for assumed gross photosynthesis", col = rainbow(length(x)))
legend("topright", c("NLLS", "Polynomial"), cex = 0.8,
       fill = rainbow(length(x)))


# Save the file.
dev.off()

##########################################################
############### AICs BY HABITAT ######################
########################################################
habitats<-unique(merged_stats$habitat)
         
NLS_marine<-numeric()
Poly_marine<-numeric()
NLS_freshwater<-numeric()
Poly_freshwater<-numeric()
NLS_estuarine<-numeric()
Poly_estuarine<-numeric()
NLS_hotspring<-numeric()
Poly_hotspring<-numeric()
NLS_saline_lake<-numeric()
Poly_saline_lake<-numeric()
NLS_unknown<-numeric()
Poly_unknown<-numeric()
NLS_terrestrial<-numeric()
Poly_terrestrial<-numeric()
NLS_fresh_terrestrial_aquatic<-numeric()
Poly_fresh_terrestrial_aquatic<-numeric()

for(row in 1:nrow(merged_stats)){ #do same for polynomial
  if (merged_stats$habitat[row] == "marine")
  {
    if(merged_stats$AIC[row] < merged_stats$AIC_df[row]){
      Poly_marine<-c(Poly_marine, 1)}
    if (merged_stats$AIC[row] > merged_stats$AIC_df[row]){
      NLS_marine <- c(NLS_marine, 1)}
  }
  if (merged_stats$habitat[row] == "freshwater")
  {
    if(merged_stats$AIC[row] < merged_stats$AIC_df[row]){
      Poly_freshwater<-c(Poly_freshwater, 1)}
    if (merged_stats$AIC[row] > merged_stats$AIC_df[row]){
      NLS_freshwater <- c(NLS_freshwater, 1)}
  }
  if (merged_stats$habitat[row] == "estuarine")
  {
    if(merged_stats$AIC[row] < merged_stats$AIC_df[row]){
      Poly_estuarine<-c(Poly_estuarine, 1)}
    if (merged_stats$AIC[row] > merged_stats$AIC_df[row]){
      NLS_estuarine <- c(NLS_estuarine, 1)}
  }
  if (merged_stats$habitat[row] == "hot spring")
  {
    if(merged_stats$AIC[row] < merged_stats$AIC_df[row]){
      Poly_hotspring<-c(Poly_hotspring, 1)}
    if (merged_stats$AIC[row] > merged_stats$AIC_df[row]){
      NLS_hotspring <- c(NLS_hotspring, 1)}
  }
  if (merged_stats$habitat[row] == "saline lake")
  {
    if(merged_stats$AIC[row] < merged_stats$AIC_df[row]){
      Poly_saline_lake<-c(Poly_saline_lake, 1)}
    if (merged_stats$AIC[row] > merged_stats$AIC_df[row]){
      NLS_saline_lake <- c(NLS_saline_lake, 1)}
  }
  if (merged_stats$habitat[row] == "unknown")
  {
    if(merged_stats$AIC[row] < merged_stats$AIC_df[row]){
      Poly_unknown<-c(Poly_unknown, 1)}
    if (merged_stats$AIC[row] > merged_stats$AIC_df[row]){
      NLS_unknown <- c(NLS_unknown, 1)}
  }
  if (merged_stats$habitat[row] == "terrestrial")
  {
    if(merged_stats$AIC[row] < merged_stats$AIC_df[row]){
      Poly_terrestrial<-c(Poly_terrestrial, 1)}
    if (merged_stats$AIC[row] > merged_stats$AIC_df[row]){
      NLS_terrestrial <- c(NLS_terrestrial, 1)}
  }
  if (merged_stats$habitat[row] == "freshwater / terrestrial aquatic")
  {
    if(merged_stats$AIC[row] < merged_stats$AIC_df[row]){
      Poly_fresh_terrestrial_aquatic<-c(Poly_fresh_terrestrial_aquatic, 1)}
    if (merged_stats$AIC[row] > merged_stats$AIC_df[row]){
      NLS_fresh_terrestrial_aquatic <- c(NLS_fresh_terrestrial_aquatic, 1)}
  }
}

length(NLS_marine)
length(Poly_marine)
length(NLS_freshwater)
length(Poly_freshwater)
length(NLS_estuarine)
length(Poly_estuarine)
length(NLS_hotspring)
length(Poly_hotspring)
length(NLS_saline_lake)
length(Poly_saline_lake)
length(NLS_unknown)
length(Poly_unknown)
length(NLS_terrestrial)
length(Poly_terrestrial)
#length(NLS_fresh_terrestrial_aquatic)
#length(Poly_fresh_terrestrial_aquatic)

NLS_aquatic<-length(NLS_marine) + length(NLS_freshwater) + length(NLS_estuarine) + length(NLS_hotspring) + length(NLS_saline_lake)
Poly_aquatic<- length(Poly_marine) + length(Poly_freshwater) + length(Poly_estuarine) + length(Poly_hotspring) + length(Poly_saline_lake)

# NLS_aquatic
# Poly_aquatic
################################################
############# HABITAT PIE CHARTS ##################
##################################################

pdf("../Results/pie_marine.pdf")
# Create data for the graph.
x <- c(length(NLS_marine), length(Poly_marine))
labels <- c("NLLS", "Polynomial")
piepercent<- round(100*x/sum(x), 1)


# Plot the chart.
#pie(x,labels)
pie(x, labels = piepercent, main = "Percentage of AICs that were lowest between two models for marine habitats", col = rainbow(length(x)))
legend("topright", c("NLLS", "Polynomial"), cex = 0.8,
       fill = rainbow(length(x)))


# Save the file.
dev.off()

##################################
pdf("../Results/pie_freshwater.pdf")
# Create data for the graph.
x <- c(length(NLS_freshwater), length(Poly_freshwater))
labels <- c("NLLS", "Polynomial")
piepercent<- round(100*x/sum(x), 1)


# Plot the chart.
#pie(x,labels)
pie(x, labels = piepercent, main = "Percentage of AICs that were lowest between two models for freshwater habitats", col = rainbow(length(x)))
legend("topright", c("NLLS", "Polynomial"), cex = 0.8,
       fill = rainbow(length(x)))


# Save the file.
dev.off()

##################################
pdf("../Results/pie_estuarine.pdf")
# Create data for the graph.
x <- c(length(NLS_estuarine), length(Poly_estuarine))
labels <- c("NLLS", "Polynomial")
piepercent<- round(100*x/sum(x), 1)


# Plot the chart.
#pie(x,labels)
pie(x, labels = piepercent, main = "Percentage of AICs that were lowest between two models for estuarine habitats", col = rainbow(length(x)))
legend("topright", c("NLLS", "Polynomial"), cex = 0.8,
       fill = rainbow(length(x)))


# Save the file.
dev.off()

##################################
pdf("../Results/pie_hotspring.pdf")
# Create data for the graph.
x <- c(length(NLS_hotspring), length(Poly_hotspring))
labels <- c("NLLS", "Polynomial")
piepercent<- round(100*x/sum(x), 1)


# Plot the chart.
#pie(x,labels)
pie(x, labels = piepercent, main = "Percentage of AICs that were lowest between two models for hot spring habitats", col = rainbow(length(x)))
legend("topright", c("NLLS", "Polynomial"), cex = 0.8,
       fill = rainbow(length(x)))


# Save the file.
dev.off()

###################################
pdf("../Results/pie_saline_lake.pdf")
# Create data for the graph.
x <- c(length(NLS_saline_lake), length(Poly_saline_lake))
labels <- c("NLLS", "Polynomial")
piepercent<- round(100*x/sum(x), 1)


# Plot the chart.
#pie(x,labels)
pie(x, labels = piepercent, main = "Percentage of AICs that were lowest between two models for saline lake habitats", col = rainbow(length(x)))
legend("topright", c("NLLS", "Polynomial"), cex = 0.8,
       fill = rainbow(length(x)))


# Save the file.
dev.off()

###################################
pdf("../Results/pie_terrestrial.pdf")
# Create data for the graph.
x <- c(length(NLS_terrestrial), length(Poly_terrestrial))
labels <- c("NLLS", "Polynomial")
piepercent<- round(100*x/sum(x), 1)


# Plot the chart.
#pie(x,labels)
pie(x, labels = piepercent, main = "Percentage of AICs that were lowest between two models for terrestrial habitats", col = rainbow(length(x)))
legend("topright", c("NLLS", "Polynomial"), cex = 0.8,
       fill = rainbow(length(x)))


# Save the file.
dev.off()

###################################

pdf("../Results/pie_aquatic.pdf")
# Create data for the graph.
x <- c(NLS_aquatic, Poly_aquatic)
labels <- c("NLLS", "Polynomial")
piepercent<- round(100*x/sum(x), 1)


# Plot the chart.
#pie(x,labels)
pie(x, labels = piepercent, main = "Percentage of AICs that were lowest between two models for all aquatic habitats", col = rainbow(length(x)))
legend("topright", c("NLLS", "Polynomial"), cex = 0.8,
       fill = rainbow(length(x)))


# Save the file.
dev.off()

###########################################
########### r squareds ####################
##########################################
NLSr<-numeric()
Polyr<-numeric()
equalr<-numeric()
for(row in 1:nrow(merged_stats)){ #do same for polynomial
  #print(row)
  if(merged_stats$r.squared[row] > merged_stats$r_sqrd[row]){
    Polyr<-c(Poly, 1)}
  if (merged_stats$r.squared[row] < merged_stats$r_sqrd[row]){
    NLSr <- c(NLS, 1)}
  if(merged_stats$AIC[row] == merged_stats$AIC_df[row]){
    equalr<-c(equal, 1)}
}

##########################################################
################# r squared pie charts ###################
######################################################
length(NLSr)  
length(Polyr)
length(equalr)

pdf("../Results/pier.pdf")
# Create data for the graph.
x <- c(length(NLSr), length(Polyr))
labels <- c("NLLS", "Polynomial")
piepercent<- round(100*x/sum(x), 1)


# Plot the chart.
#pie(x,labels)
pie(x, labels = piepercent, main = "Percentage of r squareds that were highest between two models",col = c("green", "purple"))
legend("topright", c("NLLS", "Polynomial"), cex = 0.8,
       fill = c("green", "purple"))


# Save the file.
dev.off()

##########################################################


