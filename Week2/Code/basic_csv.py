"""importing csv package to make it easier to manipulate csv files"""
import csv

# Read a file containing:
#'Species','Infraorder','Family','Distribution','Body mass male (Kg)'
f=open('../Sandbox/testcsv.csv','rb') # read in file

csvread=csv.reader(f)
temp=[]
for row in csvread:
	temp.append(tuple(row))
	print row
	print "The species is", row[0]
	
f.close()

#write a file containing only species name and Body mass
f=open('../Sandbox/testcsv.csv','rb')
g=open('../Sandbox/bodymass.csv','wb')

csvread=csv.reader(f) #enables reading a csv file
csvwrite=csv.writer(g) #enables writing to a csv file
for row in csvread:
	print row
	csvwrite.writerow([row[0], row[4]])
	
f.close()
g.close()
