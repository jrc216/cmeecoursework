Contains all handouts and some of the exercises from he stats week in R

HO1,HO2 - intro to r and species distributions
HO4,HO5 - standard errors and hypothesis testing
HO10 - linear regression
HO13 - linear regression and ANOVA
HO14 - repeatability
HO16 - Multivariate linear models
