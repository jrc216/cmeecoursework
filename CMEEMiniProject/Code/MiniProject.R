rm(list=ls())

library(ggplot2)
require(data.table)
require(plyr)
require(minpack.lm)



Data <- read.csv("../Data/GrowthRespPhotoData.csv", header=T, stringsAsFactors = F)
Data$trait<- ifelse((Data$OriginalTraitName == 'growth rate'), Data$StandardisedTraitValue, Data$OriginalTraitValue)

#no data were there are less than five data points
Data2 <- subset(Data, table(Data$FinalID)[Data$FinalID]>=5)


#making a new column that fills up with ambient temperature as the priority but if theres an NA fills with COntemp instead
Data2$SynthTemp <-Data2$AmbientTemp
my.na <- is.na(Data2$AmbientTemp)
Data2$SynthTemp[my.na] <- Data2$ConTemp[my.na]

#new column changing Temperature to Kelvin
Data2$SynthTempK<-Data2$SynthTemp +273.15

Data2 <- subset(Data2, !is.na(Data2$trait))

Data2$trait_name<- ifelse(is.na(Data2$StandardisedTraitName), Data2$OriginalTraitName, Data2$StandardisedTraitName)
###getting rid of zeros
negatives_zeros <- function(d){
  # browser()
  min_val <- min(d$trait)
  if((min_val <= 0)){
    d$trait <- d$trait- min_val
    # If there are negative trait values, substract the min from all trait values.
    # Save this as a new column, 'Trait_minus_min'.
    
    if(length(which(d$trait == 0)) > 0){
      d$trait <- d$trait + 1
      # Add 1 to 'Trait_minus_min', if there are values of 0.
    }
  } else{
    if(length(which(d$trait == 0)) > 0){
      d$trait <- d$trait + 1
      # If there values of 0 but no negative values, add 1 to 'Trait_value'.
    }
  }
  return(d)
}


growth_rate <- Data2[Data2$trait_name == 'growth rate',] # Get rows where the trait is 'growth rate'.
other_traits <- Data2[Data2$trait_name != 'growth rate',] # '!=' - not equal


growth_rate <- negatives_zeros(growth_rate)
other_traits <- negatives_zeros(other_traits)

 # Copy the data, so I can undo changes.
Data2 <- rbind(growth_rate, other_traits)


#log transforming new standardised trait value and adding one as you cant log negative or zero values
Data2$NewStandardisedTraitValue<- logb(Data2$trait, base=exp(1))

#write new dataframe to a csv
write.csv(Data2, "../Data/GrowtRespPhotoData2.csv")

#Create new dataframe with selected columns
CutData <- Data2[,c("FinalID", "OriginalTraitName", "StandardisedTraitValue","NewStandardisedTraitValue", "StandardisedTraitUnit", "SynthTemp", "SynthTempK", "Consumer", "ConSize", "ConSizeSI", "trait", "trait_name", "Habitat")] 

#Subset data so no NAs
CutData<-subset(CutData, CutData$NewStandardisedTraitValue!="NA")

#add new column of 1/KT
CutData$"-1/KT"<- -1/((8.617*10**(-5))*CutData$SynthTempK)

# creates a table where for each final ID it has the number of unique temp measurements
CountTemp<-data.table(CutData)[,length(unique(SynthTempK)), by=FinalID] 

# subset by when the number of unique temp measurements is less than 5
UniqueTemps<-subset(CountTemp, CountTemp$V1 < 5 )

for (i in UniqueTemps$FinalID) (   # iterate through the FinalID column of the subset just created
  CutData <- subset(CutData, CutData$FinalID !=i) # update data with the subset values(unique ids with less than 5) are removed
)
#write new data to a csv  
write.csv(CutData, "../Data/CutData.csv")

idsubset<-subset(CutData, FinalID=="MTD4290")
#############################################################################################
#creating a function that performs an lm and extracts stats into a dataframe and print graphs to pdf

pdf("../Results/lm_graphs.pdf")

LinearModel <- function(a) {   
  xdata<-a$SynthTemp
  ydata<-a$NewStandardisedTraitValue
  poly <- lm(ydata~poly(xdata, 3, raw = TRUE), data = a) # polynomial (cubic) lm
  DataPlt = data.frame(xdata= seq(min(xdata),max(xdata),len=200))
  plot(xdata, ydata, xlab = "Temperature Celsius", ylab = "log(B)")
  lines(DataPlt$xdata,predict(poly, newdata=DataPlt), col="red")
  data.frame(slope=summary(poly)$coefficients[2], 
             intercept=summary(poly)$coefficients[1], 
             r.squared= summary(poly)$r.squared,
             fstatistic=summary(poly)$fstatistic[1],
             p.value=summary(poly)$coefficients[8],
             AIC=AIC(poly),
             BIC=BIC(poly)
  )
}

#loops over FinalIDs in CutData and applies the above function to it
ActivationEnergy <- ddply(CutData, .(FinalID), LinearModel) #data, subset, function
dev.off()

write.csv(ActivationEnergy, "../Results/poly_stats2.csv")


######Graph for 1 ID#########
pdf("../Results/graphID.pdf")

LinearModel <- function(a) {   
  xdata<-a$SynthTemp
  ydata<-a$NewStandardisedTraitValue
  poly <- lm(ydata~poly(xdata, 3, raw = TRUE), data = a) # polynomial (cubic) lm
  DataPlt = data.frame(xdata= seq(min(xdata),max(xdata),len=200))
  plot(xdata, ydata, xlab = "Temperature Celsius", ylab = "log(B)")
  lines(DataPlt$xdata,predict(poly, newdata=DataPlt), col="red")

}

oneID<-ddply(idsubset, .(FinalID), LinearModel)
dev.off()
#create a function to get a list of temperatures at max trait value point on curve( by subsetting up to max trait values point and getting all points up to that point) then create an lm of log trait value against -1/KT so we can get the activation energy (Ea) and B0. Then output a csv to read in for NLS fitting
schoolfield_parameters<- function(d) {
  max_trait<-max(d$NewStandardisedTraitValue)
  max_temp<-d[d$NewStandardisedTraitValue==max_trait,]$SynthTempK # take temperature for max trait value
  list<-d[d$SynthTempK<=max_temp,]$SynthTempK #make a list of all values up to and including peak temperature
  y<-d[d$SynthTempK<=max_temp,]$NewStandardisedTraitValue 
  
  x<-d[d$SynthTempK<=max_temp,]$"-1/KT"
  lm<- lm(y~x) # should be doing this on the values up to max_temp!
  data.frame(Ea=summary(lm)$coefficients[2], 
             B0=summary(lm)$coefficients[1],
             Ed=2*(summary(lm)$coefficients[2]),
             Tpk=max(max_temp)
             
  )
}

schoolfield_inputs<- ddply(CutData, .(FinalID), schoolfield_parameters)  # applying the above function to each ID in CutData


write.csv(schoolfield_inputs, "../Data/schoolfield_inputs.csv") # write schoolfield parameters to a csv

merge<-merge(schoolfield_inputs, CutData, all=TRUE)
write.csv(merge, "../Data/merged_data.csv")
