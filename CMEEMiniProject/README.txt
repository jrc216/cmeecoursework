CODE
MiniProject.R = Code for cleaning data, running polynomial and  generating Schoolfield parameter estimates
Jo.R = code for running NLLS
run_MiniProject.py = script for running r code, running the bash compile latex script and wordcount script 
Compilelatex.sh = script to compile latex
wordcount.sh = code to count number of words in latex
Report = final report
mybib = bibliography

DATA
BiotraitsTemplateDescription.pdf = description of biotraits database(GrowthRespPhotoData)
GrowthRespPhotoData.csv = original data
GrowthRespPhotoData2.csv = added extra columns to dataframe
CutData.csv = just selected data columns that will be needed in the analysis
schoolfield_inputs = data generated from lm for getting scoolfield input parameters (these weren't actually used in the final NLLS as functions were used to retrieve the data instead
merged_data.csv = merged CutData and schoolfield_inputs

RESULTS
lm_graphs.pdf = polynomial fits 
NLS.pdf = Schoolfield fits
.Rout files = outputs of R scripts when run from python
_errFile.Rout files = error files from r scripts when run from python
nls_stats.csv = output data from Schoolfield NLLS
poly_stats2.csv = output data from polynomial model
pie charts are all comparing the AICs (in one case the r squared) for the polynomial and Schoolfield model. These are either compared across the whole dataset or for different habitats or traits

PAPERS
Papers read for write up

DEPENDENCIES

PYTHON
subprocess
os.path

R
data.table
plyr
minpack.lm
ggplot






