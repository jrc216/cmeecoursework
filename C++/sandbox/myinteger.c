#include <stdio.h>

int main (void)
{
	int x = 0;    //give me enough space for a normal sized signed integer, can use any letters, but dont start with a no. use an underscore instead, but cant use any other special chars
				 // can also have unsigned int, unsigned long int, unsigned long long int
	char c = 'c';
	float f = 3.141592654;
	
	printf("\nThe value of x: %i\n", x); //% an argument, i = variable type: i, so pass an integer to print
	printf("The value of c: %c\n", c);
	printf("The value of f: %.10f\n", f); //default f width is smaller than no. so add .10 before f but that just filled in arbitary numbers after!
	
	return 0;
	
}
//integers we're creating are physical spaces in memory
// ./a.out is the executable file, will contain most previous compiled code


