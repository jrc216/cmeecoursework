
/*--------------------------------------------------------------------*/
/*--- x86-specific stuff for the core.             x86/core_arch.h ---*/
/*--------------------------------------------------------------------*/

/*
   This file is part of Valgrind, an extensible x86 protected-mode
   emulator for monitoring program execution on x86-Unixes.

   Copyright (C) 2000-2005 Nicholas Nethercote
      njn25@cam.ac.uk

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307, USA.

   The GNU General Public License is contained in the file COPYING.
*/

#ifndef __X86_CORE_ARCH_H
#define __X86_CORE_ARCH_H

#include "core_arch_asm.h"    // arch-specific asm  stuff
#include "tool_arch.h"        // arch-specific tool stuff

/* ---------------------------------------------------------------------
   Interesting registers
   ------------------------------------------------------------------ */

// Accessors for the arch_thread_t
#define ARCH_INSTR_PTR(regs)           ((regs).m_eip)
#define ARCH_STACK_PTR(regs)           ((regs).m_esp)
#define ARCH_FRAME_PTR(regs)           ((regs).m_ebp)

/* Gets the return address of a function call.  Assumes the VCPU is in
   a state where a return would work (ie just before a return
   instruction, or at the start of a called function before any local
   frame has been set up). */
#define ARCH_RETADDR(regs)	       (*(Addr *)((regs).m_esp))

/* Gets argument N for a function.  Makes the same assumption as ARCH_RETADDR.

   XXX Assumes all args are simple words on the stack; it should
   really be a stdargs-like stateful interface. */
#define ARCH_FUNC_ARG(regs, N)	       ((((Word *)((regs).m_esp))[(N)+1]))

#define ARCH_RETVAL(regs)	       ((regs).m_eax)

/* va_list on x86 is just a pointer to the first arg */
#define ARCH_VA_LIST(regs)	       ((va_list)(ARCH_STACK_PTR(regs)+sizeof(Addr)))

#define ARCH_CLREQ_ARGS(regs)          ((regs).m_eax)
#define ARCH_PTHREQ_RET(regs)          ((regs).m_edx)
#define ARCH_CLREQ_RET(regs)           ((regs).m_edx)

// Accessors for the ThreadState
#define R_CLREQ_RET                    R_EDX
#define R_PTHREQ_RET                   R_EDX

// Stack frame layout and linkage
#define FIRST_STACK_FRAME(ebp)         (ebp)
#define STACK_FRAME_RET(ebp)           (((UWord*)ebp)[1])
#define STACK_FRAME_NEXT(ebp)          (((UWord*)ebp)[0])

// Offsets of interesting registers
#define VGOFF_INSTR_PTR                VGOFF_(m_eip)
#define VGOFF_STACK_PTR                VGOFF_(m_esp)
#define VGOFF_FRAME_PTR                VGOFF_(m_ebp)

// Get stack pointer and frame pointer
#define ARCH_GET_REAL_STACK_PTR(esp) do {   \
   asm("movl %%esp, %0" : "=r" (esp));       \
} while (0)

#define ARCH_GET_REAL_FRAME_PTR(ebp) do {   \
   asm("movl %%ebp, %0" : "=r" (ebp));       \
} while (0)


/* ---------------------------------------------------------------------
   Elf stuff
   ------------------------------------------------------------------ */

#define VG_ELF_ENDIANNESS     ELFDATA2LSB
#define VG_ELF_MACHINE        EM_386       
#define VG_ELF_CLASS          ELFCLASS32


/* ---------------------------------------------------------------------
   Exports of vg_helpers.S
   ------------------------------------------------------------------ */

extern const Char VG_(helper_wrapper_before)[];	/* in dispatch.S */
extern const Char VG_(helper_wrapper_return)[];	/* in dispatch.S */

extern const Char VG_(helper_undefined_instruction)[];
extern const Char VG_(helper_INT)[];
extern const Char VG_(helper_breakpoint)[];

/* Mul, div, etc, -- we don't codegen these directly. */
extern const Char VG_(helper_idiv_64_32)[];
extern const Char VG_(helper_div_64_32)[];
extern const Char VG_(helper_idiv_32_16)[];
extern const Char VG_(helper_div_32_16)[];
extern const Char VG_(helper_idiv_16_8)[];
extern const Char VG_(helper_div_16_8)[];

extern const Char VG_(helper_imul_32_64)[];
extern const Char VG_(helper_mul_32_64)[];
extern const Char VG_(helper_imul_16_32)[];
extern const Char VG_(helper_mul_16_32)[];
extern const Char VG_(helper_imul_8_16)[];
extern const Char VG_(helper_mul_8_16)[];

extern const Char VG_(helper_CLD)[];
extern const Char VG_(helper_STD)[];
extern const Char VG_(helper_get_dirflag)[];

extern const Char VG_(helper_CLC)[];
extern const Char VG_(helper_STC)[];
extern const Char VG_(helper_CMC)[];

extern const Char VG_(helper_shldl)[];
extern const Char VG_(helper_shldw)[];
extern const Char VG_(helper_shrdl)[];
extern const Char VG_(helper_shrdw)[];

extern const Char VG_(helper_IN)[];
extern const Char VG_(helper_OUT)[];

extern const Char VG_(helper_RDTSC)[];
extern const Char VG_(helper_CPUID)[];

extern const Char VG_(helper_bsfw)[];
extern const Char VG_(helper_bsfl)[];
extern const Char VG_(helper_bsrw)[];
extern const Char VG_(helper_bsrl)[];

extern const Char VG_(helper_fstsw_AX)[];
extern const Char VG_(helper_SAHF)[];
extern const Char VG_(helper_LAHF)[];
extern const Char VG_(helper_DAS)[];
extern const Char VG_(helper_DAA)[];
extern const Char VG_(helper_AAS)[];
extern const Char VG_(helper_AAA)[];
extern const Char VG_(helper_AAD)[];
extern const Char VG_(helper_AAM)[];

extern const Char VG_(helper_cmpxchg8b)[];

/* ---------------------------------------------------------------------
   LDT type             
   ------------------------------------------------------------------ */

// XXX: eventually this will be x86-private, not seen by the core(?)

/* This is the hardware-format for a segment descriptor, ie what the
   x86 actually deals with.  It is 8 bytes long.  It's ugly.  */

typedef struct _LDT_ENTRY {
    union {
       struct {
          UShort      LimitLow;
          UShort      BaseLow;
          unsigned    BaseMid         : 8;
          unsigned    Type            : 5;
          unsigned    Dpl             : 2;
          unsigned    Pres            : 1;
          unsigned    LimitHi         : 4;
          unsigned    Sys             : 1;
          unsigned    Reserved_0      : 1;
          unsigned    Default_Big     : 1;
          unsigned    Granularity     : 1;
          unsigned    BaseHi          : 8;
       } Bits;
       struct {
          UInt word1;
          UInt word2;
       } Words;
    } 
    LdtEnt;
} VgLdtEntry;

/* ---------------------------------------------------------------------
   Miscellaneous constants
   ------------------------------------------------------------------ */

// Total number of spill slots available for register allocation.
#define VG_MAX_SPILLSLOTS     24

// Valgrind's stack size, in words.
#define VG_STACK_SIZE_W    16384

// Base address of client address space.
#define CLIENT_BASE	0x00000000ul

/* ---------------------------------------------------------------------
   Constants pertaining to the simulated CPU state.
   ------------------------------------------------------------------ */

/* How big is the saved SSE/SSE2 state?  Note that this subsumes the
   FPU state.  On machines without SSE, we just save/restore the FPU
   state into the first part of this area. */
/* A general comment about SSE save/restore: It appears that the 7th
   word (which is the MXCSR) has to be &ed with 0x0000FFBF in order
   that restoring from it later does not cause a GP fault (which is
   delivered as a segfault).  I guess this will have to be done
   any time we do fxsave :-(  7th word means word offset 6 or byte
   offset 24 from the start address of the save area.
 */
#define VG_SIZE_OF_SSESTATE 512
/* ... and in words ... */
#define VG_SIZE_OF_SSESTATE_W ((VG_SIZE_OF_SSESTATE+3)/4)

#define X86_FXSR_MAGIC		0x0000

struct i387_fsave_struct {
	long	cwd;
	long	swd;
	long	twd;
	long	fip;
	long	fcs;
	long	foo;
	long	fos;
	long	st_space[20];	/* 8*10 bytes for each FP-reg = 80 bytes */
	long	status;		/* software status information */
};

struct i387_fxsave_struct {
	unsigned short	cwd;
	unsigned short	swd;
	unsigned short	twd;
	unsigned short	fop;
	long	fip;
	long	fcs;
	long	foo;
	long	fos;
	long	mxcsr;
	long	mxcsr_mask;
	long	st_space[32];	/* 8*16 bytes for each FP-reg = 128 bytes */
	long	xmm_space[32];	/* 8*16 bytes for each XMM-reg = 128 bytes */
	long	padding[56];
} __attribute__ ((aligned (16)));


// Architecture-specific part of a ThreadState
// XXX: eventually this should be made abstract, ie. the fields not visible
//      to the core...  then VgLdtEntry can be made non-visible to the core
//      also.
typedef struct {
   /* Saved machine context.  Note the FPU state, %EIP and segment
      registers are not shadowed.

      Although the segment registers are 16 bits long, storage
      management here is simplified if we pretend they are 32 bits.

      NOTE: the x86-linux/syscall.S requires this structure to start with m_eax-m_ebp
   */
   UInt m_eax;
   UInt m_ebx;
   UInt m_ecx;
   UInt m_edx;
   UInt m_esi;
   UInt m_edi;
   UInt m_ebp;
   UInt m_esp;
   UInt m_eflags;
   UInt m_dflag;
   UInt m_eip;

   UInt m_cs;
   UInt m_ss;
   UInt m_ds;
   UInt m_es;
   UInt m_fs;
   UInt m_gs;

   /* Put shadow state near real state */
   UInt sh_eax;
   UInt sh_ebx;
   UInt sh_ecx;
   UInt sh_edx;
   UInt sh_esi;
   UInt sh_edi;
   UInt sh_ebp;
   UInt sh_esp;
   UInt sh_eflags;

   /* Pointer to this thread's Local (Segment) Descriptor Table.
      Starts out as NULL, indicating there is no table, and we hope to
      keep it that way.  If the thread does __NR_modify_ldt to create
      entries, we allocate a 8192-entry table at that point.  This is
      a straight copy of the Linux kernel's scheme.  Don't forget to
      deallocate this at thread exit. */
   VgLdtEntry* ldt;

   /* TLS table. This consists of a small number (currently 3) of
      entries from the Global Descriptor Table. */
   VgLdtEntry tls[VKI_GDT_ENTRY_TLS_ENTRIES];

   /* Spill slots - don't really need to be part of per-thread state,
      since they're never live across context-switches, but it's
      convenient for now.  TODO: move onto thread stack. */
   Word spillslots[VG_MAX_SPILLSLOTS];

   /* The SSE/FPU state.  This array does not (necessarily) have the
      required 16-byte alignment required to get stuff in/out by
      fxsave/fxrestore.  So we have to do it "by hand".
   */
   union {
      UInt state[VG_SIZE_OF_SSESTATE_W];
      struct i387_fsave_struct fsave;
      struct i387_fxsave_struct fxsave;
   } m_sse;
} 
arch_thread_t;

/* ---------------------------------------------------------------------
   Signal stuff (should be plat)
   ------------------------------------------------------------------ */
void VGA_(signal_return)(ThreadId tid, Bool isRT);

#endif   // __X86_CORE_ARCH_H

/*--------------------------------------------------------------------*/
/*--- end                                                          ---*/
/*--------------------------------------------------------------------*/
