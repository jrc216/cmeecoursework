rm(list=ls())
d<-read.table("../Data/SparrowSize.txt", header=T)
d1<-subset(d, d$Wing!="NA")
hist(d1$Wing)
model1<-lm(Wing~Sex.1,data=d1)
summary(model1)
boxplot(d1$Wing~d1$Sex.1, ylab="Wing lenth (mm)")
anova(model1)
t.test(d1$Wing~d1$Sex.1, var.equal=TRUE)
d1$BirdID=as.factor(d1$BirdID) # making bird ID a categorical variable
aov1<-aov((d1$Wing~d1$BirdID))
anova(aov1)
model1<-lm(d1$Wing~d1$Sex.1) # lm automatically sees it as a factor
summary(model1)
require(dplyr)
tbl_df(d1)
glimpse(d1)

d$Mass %>% cor.test(d$Tarsus, na.rm=TRUE) # piping, takes d$mass and inserts it
                                          # into argument before d$Tarsus

d1 %>%
  group_by(BirdID) %>% # group data by bird ID
  summarise (count=length(BirdID)) # makes one row of many rows of data, takes
                                  # each group of data on the same bird ID and 
                                   #summarises it by couning how many lines are in te group
count(d1, BirdID)                 # does the above, specify the table and then the variables to group by

d1 %>%
  group_by(BirdID) %>% 
  summarise (count=length(BirdID)) %>%
  count(count)

model3<-lm(Wing~as.factor(BirdID), data=d1)
anova(model3)
boxplot(d$Mass~d$Year)
m2<- lm(d$Mass~as.factor(d$Year))
anova(m2)
summary(m2)

#Exercise
d2<-subset(d1, d1$Year!="2000")
mod1<-lm(d2$Mass ~ as.factor(d2$Year)) 
summary(mod1) # 2003 is the only year where wing length is significantly different
anova(mod1) # between year variation is larger than within yer variation between years
summary(mod1)

