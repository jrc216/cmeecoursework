
/*--------------------------------------------------------------------*/
/*--- x86 registers, etc.                              x86/state.c ---*/
/*--------------------------------------------------------------------*/

/*
   This file is part of Valgrind, an extensible x86 protected-mode
   emulator for monitoring program execution on x86-Unixes.

   Copyright (C) 2000-2005 Nicholas Nethercote
      njn25@cam.ac.uk

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307, USA.

   The GNU General Public License is contained in the file COPYING.
*/

#include "core.h"
#include "x86_private.h"
#include <sys/ptrace.h>

static UInt insertDflag(UInt eflags, Int d)
{
   vg_assert(d == 1 || d == -1);
   eflags &= ~EFlagD;
   if (d < 0) eflags |= EFlagD;
   return eflags;
}

/*------------------------------------------------------------*/
/*--- Thread stuff                                         ---*/
/*------------------------------------------------------------*/

void VGA_(clear_thread)( arch_thread_t *arch )
{
   arch->ldt = NULL;
   VG_(clear_TLS_for_thread)(arch->tls);
}

#define VG_UNUSED_SHADOW_REG_VALUE  0x27182818

void VGA_(init_thread)( ThreadId tid, Addr init_eip, Addr init_esp )
{
   ThreadState *tst = VG_(get_ThreadState)(tid);
   arch_thread_t *arch = &tst->arch;

   arch->m_eax = 0;
   arch->m_ebx = 0;
   arch->m_ecx = 0;
   arch->m_edx = 0;
   arch->m_esi = 0;
   arch->m_edi = 0;
   arch->m_esp = init_esp;
   arch->m_ebp = 0;
   arch->m_eflags = 0;
   arch->m_dflag = 1;

   arch->m_eip = init_eip;

   arch->sh_eax = 0;
   arch->sh_ebx = 0;
   arch->sh_ecx = 0;
   arch->sh_edx = 0;
   arch->sh_esi = 0;
   arch->sh_edi = 0;
   arch->sh_esp = 0;
   arch->sh_ebp = 0;
   arch->sh_eflags = 0;

   VG_TRACK( post_regs_write_init, tid );

   /* I assume that if we have SSE2 we also have SSE */
   VG_(have_ssestate) =
          VG_(cpu_has_feature)(VG_X86_FEAT_FXSR) &&
          VG_(cpu_has_feature)(VG_X86_FEAT_SSE);

   /* set up an initial FPU state (doesn't really matter what it is,
      so long as it's somewhat valid) */
   if (!VG_(have_ssestate))
      asm volatile("fwait; fnsave %0; fwait"
                   : "=m" (arch->m_sse));
   else {
      asm volatile("fwait; fxsave %0; fwait"
                   : "=m" (arch->m_sse));
      arch->m_sse.fxsave.mxcsr &= 0xffbf;
   }
}  

void VGA_(cleanup_thread) ( arch_thread_t *arch )
{  
   /* Deallocate its LDT, if it ever had one. */
   VG_(deallocate_LDT_for_thread)( arch->ldt ); 
   arch->ldt = NULL;
   
   /* Clear its TLS array. */
   VG_(clear_TLS_for_thread)( arch->tls );
}  

void VGA_(setup_child) ( arch_thread_t *regs, arch_thread_t *parent_regs )
{  
   *regs = *parent_regs;

   /* We inherit our parent's LDT. */
   if (parent_regs->ldt == NULL) {
      /* We hope this is the common case. */
      regs->ldt = NULL;
   } else {
      /* No luck .. we have to take a copy of the parent's. */
      regs->ldt = VG_(allocate_LDT_for_thread)( parent_regs->ldt );
   }

   /* Initialise the thread's TLS array */
   VG_(clear_TLS_for_thread)( regs->tls );
}  

void VGA_(set_arg_and_bogus_ret)( ThreadId tid, UWord arg, Addr ret )
{
   /* Push the arg, and mark it as readable. */
   SET_PTHREQ_ESP(tid, VG_(threads)[tid].arch.m_esp - sizeof(UWord));
   * (UInt*)(VG_(threads)[tid].arch.m_esp) = arg;
   VG_TRACK( post_mem_write, VG_(threads)[tid].arch.m_esp, sizeof(void*) );

   /* Don't mark the pushed return address as readable; any attempt to read
      this is an internal valgrind bug since thread_exit_wrapper() should not
      return. */
   SET_PTHREQ_ESP(tid, VG_(threads)[tid].arch.m_esp - sizeof(UWord));
   * (UInt*)(VG_(threads)[tid].arch.m_esp) = ret;
}

void VGA_(thread_initial_stack)(ThreadId tid, UWord arg, Addr ret)
{
   Addr esp = (Addr)ARCH_STACK_PTR(VG_(threads)[tid].arch);

   /* push two args */
   esp -= 2 * sizeof(UWord);
   SET_PTHREQ_ESP(tid, esp);
   
   VG_TRACK ( new_mem_stack, esp, 2 * sizeof(UWord) );
   VG_TRACK ( pre_mem_write, Vg_CorePThread, tid, "new thread: stack",
                             esp, 2 * sizeof(UWord) );

   /* push arg and (bogus) return address */
   *(UWord*)(esp+sizeof(UWord)) = arg;
   *(UWord*)(esp)               = ret;

   VG_TRACK ( post_mem_write, esp, 2 * sizeof(UWord) );
}

void VGA_(mark_from_registers)(ThreadId tid, void (*marker)(Addr))
{
   ThreadState *tst = VG_(get_ThreadState)(tid);
   arch_thread_t *arch = &tst->arch;

   /* XXX ask tool about validity? */
   (*marker)(arch->m_eax);
   (*marker)(arch->m_ebx);
   (*marker)(arch->m_ecx);
   (*marker)(arch->m_edx);
   (*marker)(arch->m_esi);
   (*marker)(arch->m_edi);
   (*marker)(arch->m_esp);
   (*marker)(arch->m_ebp);
}

/*------------------------------------------------------------*/
/*--- Symtab stuff                                         ---*/
/*------------------------------------------------------------*/

UInt *VGA_(reg_addr_from_tst)(Int regno, arch_thread_t *arch)
{
   switch (regno) {
   case R_EAX: return &arch->m_eax;
   case R_ECX: return &arch->m_ecx;
   case R_EDX: return &arch->m_edx;
   case R_EBX: return &arch->m_ebx;
   case R_ESP: return &arch->m_esp;
   case R_EBP: return &arch->m_ebp;
   case R_ESI: return &arch->m_esi;
   case R_EDI: return &arch->m_edi;
   default:    return NULL;
   }
}

/*------------------------------------------------------------*/
/*--- pointercheck                                         ---*/
/*------------------------------------------------------------*/

Bool VGA_(setup_pointercheck)(void)
{
   vki_modify_ldt_t ldt = { 
      VG_POINTERCHECK_SEGIDX,    // entry_number
      VG_(client_base),          // base_addr
      (VG_(client_end)-VG_(client_base)) / VKI_PAGE_SIZE, // limit
      1,                         // seg_32bit
      0,                         // contents: data, RW, non-expanding
      0,                         // ! read_exec_only
      1,                         // limit_in_pages
      0,                         // ! seg not present
      1,                         // useable
   };
   int ret = VG_(do_syscall)(__NR_modify_ldt, 1, &ldt, sizeof(ldt));
   if (ret < 0) {
      VG_(message)(Vg_UserMsg,
                   "Warning: ignoring --pointercheck=yes, "
                   "because modify_ldt failed (errno=%d)", -ret);
      return False;
   } else {
      return True;
   }
}

/*------------------------------------------------------------*/
/*--- Debugger-related operations                          ---*/
/*------------------------------------------------------------*/

Int VGA_(ptrace_setregs_from_tst)(Int pid, arch_thread_t* arch)
{
   struct vki_user_regs_struct regs;

   regs.cs     = arch->m_cs;
   regs.ss     = arch->m_ss;
   regs.ds     = arch->m_ds;
   regs.es     = arch->m_es;
   regs.fs     = arch->m_fs;
   regs.gs     = arch->m_gs;
   regs.eax    = arch->m_eax;
   regs.ebx    = arch->m_ebx;
   regs.ecx    = arch->m_ecx;
   regs.edx    = arch->m_edx;
   regs.esi    = arch->m_esi;
   regs.edi    = arch->m_edi;
   regs.ebp    = arch->m_ebp;
   regs.esp    = arch->m_esp;
   regs.eflags = insertDflag(arch->m_eflags, arch->m_dflag);
   regs.eip    = arch->m_eip;

   return ptrace(PTRACE_SETREGS, pid, NULL, &regs);
}

/*--------------------------------------------------------------------*/
/*--- end                                                          ---*/
/*--------------------------------------------------------------------*/
