"""Author = Joanna Clarke
script to separate latin names, common names and mean body masses for bird species"""
birds = ( ('Passerculus sandwichensis','Savannah sparrow',18.7),
          ('Delichon urbica','House martin',19),
          ('Junco phaeonotus','Yellow-eyed junco',19.5),
          ('Junco hyemalis','Dark-eyed junco',19.6),
          ('Tachycineata bicolor','Tree swallow',20.2),
         )

#1
latin_lc = set([species[0] for species in birds]) # print out species in first column
print latin_lc

names_lc = set([names[1] for names in birds])
print names_lc

mass_lc = set([mass[2] for mass in birds])
print mass_lc

#2
species_list = [] #create an empty list
for species in birds: # cycles through each tuple in birds
	species_list += [species[0]] # adds species to the empty list
print species_list #prints list
	
names_list = []
for names in birds:
	names_list += [names[1]]
print names_list

mass_list = []	
for mass in birds:
	mass_list += [mass[2]]
print mass_list
	
		



