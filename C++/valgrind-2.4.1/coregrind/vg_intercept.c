/*--------------------------------------------------------------------*/
/*--- Various things we want to wrap.               vg_intercept.c ---*/
/*--------------------------------------------------------------------*/

/*
   This file is part of Valgrind, an extensible x86 protected-mode
   emulator for monitoring program execution on x86-Unixes.

   Copyright (C) 2000-2004 Julian Seward 
      jseward@acm.org

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307, USA.

   The GNU General Public License is contained in the file COPYING.
*/


/* ---------------------------------------------------------------------
   ALL THE CODE IN THIS FILE RUNS ON THE SIMULATED CPU.  It is
   intended for various reasons as drop-in replacements for libc
   functions.  These functions are not called directly - they're the
   targets of code redirection.  They're named weirdly so that the
   intercept code can find them when the shared object is initially
   loaded.
   ------------------------------------------------------------------ */

#include "valgrind.h"
#include "core.h"
#include <unistd.h>
#include <pthread.h>
#include <dlfcn.h>

/* ---------------------------------------------------------------------
   Hook for running __libc_freeres once the program exits.
   ------------------------------------------------------------------ */

void VG_WRAPPER(freeres)( void )
{
   int res;
#ifndef __UCLIBC__
   extern void __libc_freeres(void);
   __libc_freeres();
#endif
   VALGRIND_MAGIC_SEQUENCE(res, 0 /* default */,
                           VG_USERREQ__LIBC_FREERES_DONE, 0, 0, 0, 0);
   /*NOTREACHED*/
   *(int *)0 = 'x';
}

/* 
   The pthread_create wrapper repoints the "start_routine" argument to
   this function.  This function is here calls pthread_self(), which
   the pthread model will notice and create the mapping from pthread_t
   to Valgrind's internal ThreadId.
 */
void *VG_WRAPPER(pthread_startfunc_wrapper)(void *v)
{
   struct vg_pthread_newthread_data *data = (struct vg_pthread_newthread_data *)v;
   void *(*func)(void *) = data->startfunc;
   void *arg = data->arg;
   int ret;
   static pthread_t (*pthread_selfp)(void);

   //VALGRIND_PRINTF("intercepted thread start: real start is %p(%p)", func, arg);

   /* Do this rather than a direct call so we don't make an explicit
      dependency on libpthread.  One presumes that libpthread has
      already been loaded if we've got to this point though. */
   if (pthread_selfp == NULL)
      pthread_selfp = dlsym(NULL, "pthread_self");

   if (pthread_selfp != NULL)
      (*pthread_selfp)();	/* just calling this is enough */
   else
      VALGRIND_PRINTF("pthread_self pointer is NULL!");

   /* Free the data the before_pthread_create wrapper left for us. */
   VALGRIND_MAGIC_SEQUENCE(ret, 0, VG_USERREQ__FREE, data, 0, 0, 0);

   return (*func)(arg);

   /* XXX should we tell core the thread returned? */
}

/*--------------------------------------------------------------------*/
/*--- end                                           vg_intercept.c ---*/
/*--------------------------------------------------------------------*/

