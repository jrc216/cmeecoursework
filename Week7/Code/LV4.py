#!/usr/bin/python


""" Lottka Volterra model simulated using discrete timesteps """

import scipy as sc 
import scipy.integrate as integrate
import pylab as p #Contains matplotlib for plotting
import sys
import cProfile
import numpy as np
# import matplotlip.pylab as p #Some people might need to do this



r=1 
a=0.1
z=1.5 
e=0.75
K=30
t = 125
R = 10
C = 5

pops = sc.array([[0, R, C]]) # create array


for i in range(t):
		Rt=(R*(1+r*(1-(R/K)) - a*C)) 
		Ct=(C*(1-z + e*a*R))
		pops = sc.append(pops, [[i +1, Rt, Ct]], axis = 0)
		R=Rt
		C=Ct

t, prey, predators = pops.T # .T = transpose
f1 = p.figure() #Open empty figure object using pylab
p.plot(t, prey, 'g-', label='Resource density') # Plot
p.plot(t, predators  , 'b-', label='Consumer density')
p.annotate('r=%r, a=%r, z=%r, e=%r, K=%r'%(r,a,z,e, K), xy=(2,1), xytext=(10, 10))
p.grid()
p.legend(loc='best')
p.xlabel('Time')
p.ylabel('Population')
p.title('Consumer-Resource population dynamics')
#p.show()
f1.savefig('../Results/prey_and_predators_4.pdf') #Save figure
