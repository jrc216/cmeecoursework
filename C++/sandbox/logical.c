#include <stdio.h>

int main (void)
{
	int a = 5;
	int b = 2;
	int c = 5;
	int d = 0;
	// && AND (binary: two operands)
	if (a && b)
	{
		printf("%i and %i are non-zero\n", a, b);
	}		
	
	if (a && c)
	{
		printf("%i and %i are non-zero\n", a, c);
	}
	else {
		printf("One of %i or %i is zero\n", a, c);
	}
		

	// || OR (binary: two operands)
	if ( a || b) 
	{
		printf("At least one of %i or %i is non-zero\n", a, b);
	}
	
	if ( a || c) // and or
	{
		printf("At least one of %i or %i is non-zero\n", a, c);
	}
	else {
	printf("both %i and %i are zero\n", a, c);
	}
		
	
	// ! NOT (unary: one operand)
	if (!a) // is "a" non zero, if a isnt 0 it evaluates to true OR if this is not true
	{
		printf(" a is non-zero\n");
	}
	else if (!d)
	{
		printf("d is zero\n");
	}		
	return 0;	
}
