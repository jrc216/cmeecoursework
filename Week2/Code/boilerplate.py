#!/usr/bin/python
"""Template for a python script"""
"""Description of this program you can use several lines."""
__author__='Joanna Clarke (jrc216@ic.ac.uk)'
__version__='0.0.1'

#imports
import sys # modules to interface our program with the operating system

#constants can go here

#functions can go here
def main(argv):
		print 'This is a boilerplate' #NOTE: indented using two tabes or 4 spaces
		return 0
if (__name__=="__main__"): #makes sure the "main" function is called from commandline
			status=main(sys.argv)
			sys.exit(status)
