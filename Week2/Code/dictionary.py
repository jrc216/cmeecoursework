"""create a dictionary grouping species in the same order together"""
taxa = [ ('Myotis lucifugus','Chiroptera'),
         ('Gerbillus henleyi','Rodentia',),
         ('Peromyscus crinitus', 'Rodentia'),
         ('Mus domesticus', 'Rodentia'),
         ('Cleithrionomys rutilus', 'Rodentia'),
         ('Microgale dobsoni', 'Afrosoricida'),
         ('Microgale talazaci', 'Afrosoricida'),
         ('Lyacon pictus', 'Carnivora'),
         ('Arctocephalus gazella', 'Carnivora'),
         ('Canis lupus', 'Carnivora'),
        ]

# Write a short python script to populate a dictionary called taxa_dic 
# derived from  taxa so that it maps order names to sets of taxa. 
# E.g. 'Chiroptera' : set(['Myotis lucifugus']) etc. 

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS

# Write your script here:


taxa_dic = {} # create an empty dictionary
for species in taxa:
	if species[1] not in taxa_dic:  #if the order name is not in the dictionary
		taxa_dic[species[1]] = set() # add the order name as a key to the dictionary and add an empty set which you can add the species to later
	taxa_dic[species[1]].add(species[0]) # for the order name in this iteration add the corresponding species to the set for that order
print taxa_dic

#cycle through lines, if order is not in loop create an empty set and add 
#species to it, if already in list, add species to the empty set


